/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QDialog>
#include <QObject>


/////////////////////// pappsomspp includes
#include <pappsomspp/widget/precisionwidget/precisionwidget.h>


/////////////////////// Local includes
#include "ui_MzIntegrationParamsDlg.h"
#include "../nongui/MzIntegrationParams.hpp"


namespace msxps
{
namespace minexpert
{


class MzIntegrationParamsDlg : public QDialog
{
  Q_OBJECT

  public:
  MzIntegrationParamsDlg(QWidget *parent = nullptr);
  MzIntegrationParamsDlg(QWidget *parent,
                         const MzIntegrationParams &mz_integration_params,
                         const QColor &color);
  ~MzIntegrationParamsDlg();

  void readSettings();
  void writeSettings();

	void	keyPressEvent(QKeyEvent *event);

  void show();
  void setBinningType(BinningType binningType);
  void setBinSizePrecisionPtr(pappso::PrecisionPtr bin_size_precsion_p);
  void setDecimalPlaces(int decimal_places);
  void setRemoveZeroValDataPoints(bool apply);

  QString toString() const;

  public slots:
  void closeEvent(QCloseEvent *event);
  void applyPushButtonClicked();

  void binningRadioButtonToggled(bool checked);

  signals:
  void mzIntegrationParamsChangedSignal(MzIntegrationParams params);
  void mzIntegrationParamsDlgShouldBeDestroyedSignal();

  protected:
  Ui::MzIntegrationParamsDlg m_ui;
  MzIntegrationParams m_mzIntegrationParams;
  pappso::PrecisionWidget *mp_precisionWidget;
  pappso::PrecisionPtr mp_precision =
    pappso::PrecisionFactory::getDaltonInstance(0.05);
  QColor m_color;

  void setupWidget();
};


} // namespace minexpert

} // namespace msxps
