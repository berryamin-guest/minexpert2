/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QSettings>
#include <QDebug>
#include <QMainWindow>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "ProgramWindow.hpp"
#include "DriftSpecTracePlotWnd.hpp"
#include "DriftSpecTracePlotCompositeWidget.hpp"
#include "ColorSelector.hpp"
#include "../nongui/MassDataIntegratorTask.hpp"
#include "TaskMonitorCompositeWidget.hpp"
#include "TaskMonitorWnd.hpp"


namespace msxps
{
namespace minexpert
{


//! Construct an DriftSpecTracePlotWnd instance.
DriftSpecTracePlotWnd::DriftSpecTracePlotWnd(QWidget *parent,
                                             const QString &title,
                                             const QString &description)
  : BaseTracePlotWnd(parent, title, description)
{
}


//! Destruct \c this DriftSpecTracePlotWnd instance.
DriftSpecTracePlotWnd::~DriftSpecTracePlotWnd()
{
}


QCPGraph *
DriftSpecTracePlotWnd::addTracePlot(const pappso::Trace &trace,
                                    MsRunDataSetCstSPtr ms_run_data_set_csp,
                                    const ProcessingFlow &processing_flow,
                                    const QColor &color,
                                    QCPAbstractPlottable *parent_plottable_p)
{
  // Allocate a drift spectrum-specific composite plot widget.

  DriftSpecTracePlotCompositeWidget *composite_widget_p =
    new DriftSpecTracePlotCompositeWidget(
      this, "drift time (ms)", "counts (a.u.)");

  return finalNewTracePlotConfiguration(
    composite_widget_p, trace, processing_flow, color, parent_plottable_p);
}


void
DriftSpecTracePlotWnd::integrateToDt(
  QCPAbstractPlottable *parent_plottable_p,
  std::shared_ptr<std::vector<pappso::QualifiedMassSpectrumCstSPtr>>
    qualified_mass_spectra_sp,
  const ProcessingFlow &processing_flow)
{
  // qDebug();

  // qDebug().noquote() << "Integrating to dt with processing flow:"
  //<< processing_flow.toString();

  // Get the ms run data set that for the graph we are going to base the
  // integration on.
  MsRunDataSetCstSPtr ms_run_data_set_csp =
    processing_flow.getMsRunDataSetCstSPtr();
  if(ms_run_data_set_csp == nullptr)
    qFatal("Cannot be that the pointer is nullptr.");

  // We will try to limit the number of mass spectra to iterate through with the
  // integration visitor.

  double start_rt = std::numeric_limits<double>::infinity();
  double end_rt   = std::numeric_limits<double>::infinity();

  bool integration_rt = processing_flow.innermostRtRange(start_rt, end_rt);

  if(!integration_rt)
    qFatal("Programming error.");

  // First prepare a vector of QualifiedMassSpectrumCstSPtr

  std::size_t qualified_mass_spectra_count = fillInQualifiedMassSpectraVector(
    ms_run_data_set_csp, qualified_mass_spectra_sp, processing_flow);

  if(!qualified_mass_spectra_count)
    return;

  // qDebug() << "The number of remaining mass spectra:"
  //<< qualified_mass_spectra_count;

  // Now start the actual integration work.

  // Allocate a mass data integrator to integrate the data.

  QualifiedMassSpectrumVectorMassDataIntegratorToDt *mass_data_integrator_p =
    new QualifiedMassSpectrumVectorMassDataIntegratorToDt(
      ms_run_data_set_csp, processing_flow, qualified_mass_spectra_sp);

  // Ensure the mass data integrator messages are used.

  connect(
    mass_data_integrator_p,
    &QualifiedMassSpectrumVectorMassDataIntegrator::logTextToConsoleSignal,
    mp_programWindow,
    &ProgramWindow::logTextToConsole);

  // qDebug() << "the integrator pointer:" << mass_data_integrator_p;

  // Allocate a mass data integrator to integrate the data.

  MassDataIntegratorTask *mass_data_integrator_task_p =
    new MassDataIntegratorTask();

  // This signal starts the computation in the MassDataIntegratorTask object.
  connect(this,

          static_cast<void (DriftSpecTracePlotWnd::*)(
            QualifiedMassSpectrumVectorMassDataIntegratorToDt *)>(
            &DriftSpecTracePlotWnd::integrateToDtSignal),

          mass_data_integrator_task_p,

          static_cast<void (MassDataIntegratorTask::*)(
            QualifiedMassSpectrumVectorMassDataIntegratorToDt *)>(
            &MassDataIntegratorTask::integrateToDt),

          // Fundamental for signals that travel across QThread instances...
          Qt::QueuedConnection);

  // Allocate the thread in which the integrator task will run.
  QThread *thread_p = new QThread;

  // Move the task to the matching thread.
  mass_data_integrator_task_p->moveToThread(thread_p);
  thread_p->start();

  // When the read task finishes, it sends a signal that we trap to go on with
  // the plot widget creation stuff.

  // Since we allocated the QThread dynamically we need to be able to destroy it
  // later, so make the connection.
  connect(mass_data_integrator_task_p,

          static_cast<void (MassDataIntegratorTask::*)(
            QualifiedMassSpectrumVectorMassDataIntegrator *)>(
            &MassDataIntegratorTask::finishedIntegratingDataSignal),

          this,

          [this,
           thread_p,
           mass_data_integrator_p,
           parent_plottable_p,
           mass_data_integrator_task_p]() {
            // Do not forget that we have to delete the MassDataIntegratorTask
            // allocated instance.
            mass_data_integrator_task_p->deleteLater();
            // Once the task has been labelled to be deleted later, we can stop
            // the thread and ask for it to also be deleted later.
            thread_p->deleteLater(), thread_p->quit();
            thread_p->wait();
            this->finishedIntegratingToDt(mass_data_integrator_p,
                                          parent_plottable_p);
          });


  // Allocate a new TaskMonitorCompositeWidget that will receive all the
  // integrator's signals and provide feedback to the user about the ongoing
  // integration.

  TaskMonitorCompositeWidget *task_monitor_composite_widget_p =
    mp_programWindow->getTaskMonitorWnd()->addTaskMonitorWidget(Qt::red);

  // Initialize the monitor composite widget's widgets and make all the
  // connections mass data integrator <--> widget.

  task_monitor_composite_widget_p->setMsRunIdText(
    ms_run_data_set_csp->getMsRunId()->getSampleName());
  task_monitor_composite_widget_p->setTaskDescriptionText(
    "Integrating to drift spectrum");
  task_monitor_composite_widget_p->setProgressBarMinValue(0);

  // Make the connections

  // When the integrator task instance has finished working, it will send a
  // signal that we trap to finally destroy (after a time lag of some seconds,
  // the monitor widget.

  connect(mass_data_integrator_task_p,

          static_cast<void (MassDataIntegratorTask::*)(
            QualifiedMassSpectrumVectorMassDataIntegrator *)>(
            &MassDataIntegratorTask::finishedIntegratingDataSignal),

          task_monitor_composite_widget_p,

          &TaskMonitorCompositeWidget::taskFinished,

          Qt::QueuedConnection);

  // If the user clicks the cancel button, relay the signal to the loader.
  connect(task_monitor_composite_widget_p,
          &TaskMonitorCompositeWidget::cancelTaskSignal,
          mass_data_integrator_p,
          &QualifiedMassSpectrumVectorMassDataIntegrator::cancelOperation);

  // We need to register the meta type for std::size_t because otherwise it
  // cannot be shipped though signals.

  qRegisterMetaType<std::size_t>("std::size_t");

  // Now make all the connections that will allow the integrator to provide
  // dynamic feedback to the user via the task monitor widget.
  task_monitor_composite_widget_p->makeMassDataIntegratorConnections(
    mass_data_integrator_p);

  // qDebug() << "going to emit integrateToDtSignal with mass "
  //"data integrator:"
  //<< mass_data_integrator_p;

  emit integrateToDtSignal(mass_data_integrator_p);

  // We do not want to make signal/slot calls more than once.
  disconnect(this,

             static_cast<void (DriftSpecTracePlotWnd::*)(
               QualifiedMassSpectrumVectorMassDataIntegratorToDt *)>(
               &DriftSpecTracePlotWnd::integrateToDtSignal),

             mass_data_integrator_task_p,

             static_cast<void (MassDataIntegratorTask::*)(
               QualifiedMassSpectrumVectorMassDataIntegratorToDt *)>(
               &MassDataIntegratorTask::integrateToDt));
}


void
DriftSpecTracePlotWnd::finishedIntegratingToDt(
  QualifiedMassSpectrumVectorMassDataIntegrator *mass_data_integrator_p,
  QCPAbstractPlottable *parent_plottable_p)
{
  // This function is actually a slot that is called when the integration to mz
  // is terminated in a QThread.

  // qDebug() << "the integrator pointer:" << mass_data_integrator_p;

  // We get back the integrator that was made to work in another thread. Now get
  // the obtained data and work with them.

  // Store the MsRunDataSetSPtr for later use before deleting the integrattor.

  MsRunDataSetCstSPtr ms_run_data_set_csp =
    mass_data_integrator_p->getMsRunDataSet();

  pappso::Trace integrated_trace =
    mass_data_integrator_p->getMapTrace().toTrace();

  if(!integrated_trace.size())
    {
      qDebug() << "There is not a single point in the integrated trace.";
      return;
    }

  // Also, do not forget to copy the processing flow from the integrator. This
  // processing flow will be set into the plot widget !

  ProcessingFlow processing_flow = mass_data_integrator_p->getProcessingFlow();

  // Sanity check
  if(ms_run_data_set_csp != processing_flow.getMsRunDataSetCstSPtr())
    qFatal("Cannot be that the ms run data set pointers be different.");

  // Finally, do not forget that we need to delete the integrator now that the
  // calculation is finished.
  delete mass_data_integrator_p;
  mass_data_integrator_p = nullptr;

  // At this point, we need to get a color for the plot. That color needs to be
  // that of the parent.

  QColor plot_color;

  // There are two situations: either the integration started from a plot widget
  // and the color is certainly obtainable via the pen of the plot.
  if(parent_plottable_p != nullptr)
    plot_color = parent_plottable_p->pen().color();
  else
    // Or the integration started at the MsRunDataSetTableView and the color can
    // only be obtained via the MsRunDataSet in the OpenMsRunDataSetsDlg.
    plot_color = mp_programWindow->getColorForMsRunDataSet(ms_run_data_set_csp);

  // The new trace checks if there are push-pinned widgets. If so, the new trace
  // will be apportioned to the widgets according to the required mechanics:
  // new plot, sum, combine...

  newTrace(integrated_trace,
           ms_run_data_set_csp,
           processing_flow,
           plot_color,
           static_cast<QCPGraph *>(parent_plottable_p));

  // At this point, if the window is really not visible, then show it, otherwise
  // I was told that the user does not think that they can ask for the window to
  // show up in the main program window.

  if(!isVisible())
    {
      showWindow();
    }
}


void
DriftSpecTracePlotWnd::integrateToRt(QCPAbstractPlottable *parent_plottable_p,
                                     const ProcessingFlow &processing_flow)
{
  // qDebug();

  if(parent_plottable_p == nullptr)
    qFatal("Cannot be that pointer is nullptr.");

  mp_programWindow->integrateToRt(parent_plottable_p, nullptr, processing_flow);
}


void
DriftSpecTracePlotWnd::integrateToMz(QCPAbstractPlottable *parent_plottable_p,
                                     const ProcessingFlow &processing_flow)
{
  // qDebug();

  if(parent_plottable_p == nullptr)
    qFatal("Cannot be that pointer is nullptr.");

  mp_programWindow->integrateToMz(parent_plottable_p, nullptr, processing_flow);
}


void
DriftSpecTracePlotWnd::integrateToDtMz(QCPAbstractPlottable *parent_plottable_p,
                                       const ProcessingFlow &processing_flow)
{
  // qDebug();

  if(parent_plottable_p == nullptr)
    qFatal("Cannot be that pointer is nullptr.");

  // We do not handle integrations to dt, because each trace plot window
  // handles their own data. So we just relay this call to the main program
  // window that will in turn relay it to the mass spec plot window.

  mp_programWindow->integrateToDtMz(
    parent_plottable_p, nullptr, processing_flow);
}


void
DriftSpecTracePlotWnd::integrateToRtMz(QCPAbstractPlottable *parent_plottable_p,
                                       const ProcessingFlow &processing_flow)
{
  // qDebug();

  if(parent_plottable_p == nullptr)
    qFatal("Cannot be that pointer is nullptr.");

  mp_programWindow->integrateToRtMz(
    parent_plottable_p, nullptr, processing_flow);
}


} // namespace minexpert

} // namespace msxps
