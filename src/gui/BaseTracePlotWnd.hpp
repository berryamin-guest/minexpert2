/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

#pragma once


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QMainWindow>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "BasePlotWnd.hpp"
#include "BaseTracePlotCompositeWidget.hpp"


namespace msxps
{
namespace minexpert
{


class MsRunDataSet;
class ProgramWindow;
class DataPlotGraphNode;

class BaseTracePlotWnd : public BasePlotWnd
{
  Q_OBJECT

  public:
  // Construction/destruction
  BaseTracePlotWnd(QWidget *parent,
                   const QString &title,
                   const QString &settingsTitle,
                   const QString &description = QString());
  virtual ~BaseTracePlotWnd();

  // This function first checks if there are receiving plots (pinned-down plot
  // widgets) or not.
  virtual void newTrace(const pappso::Trace &trace,
                        MsRunDataSetCstSPtr ms_run_data_set_csp,
                        const ProcessingFlow &processing_flow,
                        const QColor &color,
                        QCPAbstractPlottable *parent_plottable_p);

  // This one cannot be implemented here because each derived class will need to
  // instantiate the proper subclass of BaseTraceCompositeWidget.
  // This functin add a trace plot, that is, does not query if there are
  // receiving plots in which to add a new graph.
  virtual QCPGraph *addTracePlot(const pappso::Trace &trace,
                                 MsRunDataSetCstSPtr ms_run_data_set_csp,
                                 const ProcessingFlow &processing_flow,
                                 const QColor &color,
                                 QCPAbstractPlottable *parent_plottable_p) = 0;

  QCPGraph *finalNewTracePlotConfiguration(
    BaseTracePlotCompositeWidget *composite_widget_p,
    const pappso::Trace &trace,
    const ProcessingFlow &processing_flow,
    const QColor &color,
    QCPAbstractPlottable *parent_plottable_p);

  virtual QCPGraph *
  duplicateTracePlot(const BaseTracePlotCompositeWidget *composite_widget_p);


    public slots :

    signals :

    protected:
};


} // namespace minexpert

} // namespace msxps
