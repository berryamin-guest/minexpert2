/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// StdLib includes


/////////////////////// Qt includes

/////////////////////// pappsomspp includes
#include <pappsomspp/massspectrum/qualifiedmassspectrum.h>


/////////////////////// Local includes
#include "MsRunDataSetTableViewModel.hpp"



namespace msxps
{
namespace minexpert
{


class MsRunDataSetTableViewItem
{
  public:
  explicit MsRunDataSetTableViewItem(
    const QList<QVariant> &data, 
		pappso::QualifiedMassSpectrumCstSPtr qualified_mass_spectrum_csp = nullptr,
    MsRunDataSetTableViewItem *parent_item_p = nullptr);
  virtual ~MsRunDataSetTableViewItem();

  MsRunDataSetTableViewItem *parentItem();

  void appendChild(MsRunDataSetTableViewItem *child);

  MsRunDataSetTableViewItem *child(int row);

  int childCount() const;
  int columnCount() const;

  QVariant data(int column) const;

  int row() const;

	pappso::QualifiedMassSpectrumCstSPtr getQualifiedMassSpectrum() const;

  private:
  QList<MsRunDataSetTableViewItem *> m_children;
  QList<QVariant> m_itemData;
  MsRunDataSetTableViewItem *mp_parentItem;
	pappso::QualifiedMassSpectrumCstSPtr mcsp_qualifiedMassSpectrum = nullptr;
};


} // namespace minexpert
} // namespace msxps
