/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QSettings>
#include <QDebug>
#include <QMainWindow>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "ProgramWindow.hpp"
#include "TicXicChromTracePlotWnd.hpp"
#include "TicXicChromTracePlotCompositeWidget.hpp"
#include "ColorSelector.hpp"
#include "../nongui/MassDataIntegratorTask.hpp"
#include "TaskMonitorCompositeWidget.hpp"


namespace msxps
{
namespace minexpert
{


//! Construct an TicXicChromTracePlotWnd instance.
TicXicChromTracePlotWnd::TicXicChromTracePlotWnd(QWidget *parent,
                                                 const QString &title,
                                                 const QString &settingsTitle,
                                                 const QString &description)
  : BaseTracePlotWnd(parent, title, settingsTitle, description)
{
}


//! Destruct \c this TicXicChromTracePlotWnd instance.
TicXicChromTracePlotWnd::~TicXicChromTracePlotWnd()
{
}


QCPGraph *
TicXicChromTracePlotWnd::addTracePlot(const pappso::Trace &trace,
                                      MsRunDataSetCstSPtr ms_run_data_set_csp,
                                      const ProcessingFlow &processing_flow,
                                      const QColor &color,
                                      QCPAbstractPlottable *parent_plottable_p)
{
  // qDebug() << "Adding new trace plot with parent plottable:"
  //<< parent_plottable_p;

  // Allocate a TIC/XIC chromatogram-specific composite plot widget.

  TicXicChromTracePlotCompositeWidget *composite_widget_p =
    new TicXicChromTracePlotCompositeWidget(
      this, "retention time (min)", "counts (a.u.)");

  return finalNewTracePlotConfiguration(
    composite_widget_p, trace, processing_flow, color, parent_plottable_p);
}


std::pair<MsRunDataSetCstSPtr, QColor>
TicXicChromTracePlotWnd::finishedIntegratingToInitialTicChromatogram(
  MsRunDataSetTreeMassDataIntegratorToRt *mass_data_integrator_p)
{


  // qDebug() << "the integrator pointer:" << mass_data_integrator_p;

  // We get back the integrator that was made to work in another thread. Now get
  // the obtained data and work with them.

  // Store the MsRunDataSetSPtr for later use before deleting the integrattor.

  MsRunDataSetCstSPtr ms_run_data_set_csp =
    mass_data_integrator_p->getMsRunDataSet();

  // Let's see the usage count of the MsRunDataSetCstSPtr.
  // qDebug() << "Usage count:" << ms_run_data_set_csp.use_count();

  pappso::Trace integrated_trace =
    mass_data_integrator_p->getMapTrace().toTrace();

  // qDebug() << "tic trace size:" << integrated_trace.size();

  if(!integrated_trace.size())
    {
      // qDebug() << "There is not a single point in the integrated trace."
      //"It might be that the data contain only MSn data (n > 1) ?";

      QMessageBox::information(
        this,
        "The TIC chromatogram has no single point in it",
        "The loaded MS run data set has data that did not produce\n"
        "a TIC chromatogram. This might be due to several reasons:\n"
        "\n"
        "- the mass data file contains no data\n"
        "- the mass data file contains only MSn data (no MS1 data).\n"
        "Since the TIC chromatogram is computed using *only* MS1 data,\n"
        "it is emtpy.\n"
        "\n"
        "In that last case, please check the data tree table view and start\n"
        "the mining session from there.\n",
        QMessageBox::Ok);
    }

  // Note that if the mass data file that was loaded contained a single mass
  // spectrum, then there would be no TIC. So we need to craft a fake one with
  // two points: (0,TIC) and (1,TIC).

  if(integrated_trace.size() < 2)
    {

      // In this situation the single data point that is there has x = 0 ; so
      // just create a new one with x = 1.

      // qDebug() << "tic trace:" << integrated_trace.toString();

      integrated_trace.push_back(pappso::DataPoint(1, integrated_trace.sumY()));

      // qDebug() << "tic trace:" << integrated_trace.toString();
    }

  // Also, do not forget to copy the processing flow from the integrator. This
  // processing flow will be set into the plot widget !

  ProcessingFlow processing_flow = mass_data_integrator_p->getProcessingFlow();

  // qDebug() << "Got processing_flow:" << processing_flow.toString();

  // Note that we are in special situation: we are now handling a TIC
  // chromatogram that was computed as a very first integration right after
  // having read data from file. We did set MS level to 1, because, by
  // definition the TIC chromatogram is computed using MS1 data only. But, there
  // is a problem: we cannot let that MS level: 1 value there, because it will
  // prevent all the other integrations to access data with MS level > 1. We
  // thus replace that MS level to 0, that means that all the data (whatever the
  // MS level) will be handled from there.

  ProcessingStep *most_recent_step_p =
    const_cast<ProcessingStep *>(processing_flow.mostRecentStep());

  std::vector<ProcessingSpec *> processing_specs =
    most_recent_step_p->allProcessingSpecsMatching(ProcessingType("FILE_TO_RT"),
                                                   true);

  if(processing_specs.size() != 1)
    qFatal(
      "Cannot be that there are either no or more than one processing step fo "
      "type "
      "FILE_TO_RT");

  MsFragmentationSpec *fragmentation_spec_p =
    processing_specs.front()->getMsFragmentationSpecPtr();

  fragmentation_spec_p->setMsLevel(0);

  // Sanity check
  if(ms_run_data_set_csp != processing_flow.getMsRunDataSetCstSPtr())
    qFatal("Cannot be that the ms run data set pointers be different.");

  // Finally, do not forget that we need to delete the integrator now that the
  // calculation is finished.
  delete mass_data_integrator_p;
  mass_data_integrator_p = nullptr;

  // Let's see the usage count of the MsRunDataSetCstSPtr.
  // qDebug() << "Usage count:" << ms_run_data_set_csp.use_count();

  // First thing is create the data plot graph tree, because this is the entry
  // point to the whole set of data plot graphs : the tree is root into the TIC
  // chromatogram plot graph because this is where the data exploration starts.

  // At this point, we need to get a color for the plot.
  // Set true to ask that the color be selected randomly if no color is
  // available.
  QColor plot_color = ColorSelector::getColor(true);

  // Now we should document the mz integration parameters in the processing flow
  // that is mapped to the graph in such a way that the default mz integration
  // parameters that were calculated while loading the ms run from file are
  // available for the very first integration to mz that might occur.

  // Immediately set the default mz integration params to the local copy of the
  // processing flow that we'll use later to craft the plot widget.

  processing_flow.setDefaultMzIntegrationParams(
    ms_run_data_set_csp->craftInitialMzIntegrationParams());

  // qDebug().noquote()
  //<< "Default mz integration parameters:"
  //<< processing_flow.getDefaultMzIntegrationParams().toString();

  // Let's see the usage count of the MsRunDataSetCstSPtr.
  //	qDebug() << "Usage count:" << ms_run_data_set_csp.use_count();

  // And now that these params are documented, create the graph! Note the
  // nullptr parameter that shows that there is no parent graph here.

  // qDebug() << "going to call addTracePlot()";

  addTracePlot(integrated_trace,
               ms_run_data_set_csp,
               processing_flow,
               plot_color,
               nullptr);

  return std::pair<MsRunDataSetCstSPtr, QColor>(ms_run_data_set_csp,
                                                plot_color);
}

#if 0
// Old version
void
TicXicChromTracePlotWnd::integrateToRt(QCPAbstractPlottable *parent_plottable_p,
                                       const ProcessingFlow &processing_flow)

{
  qDebug();

  // qDebug().noquote() << "Integrating to rt with processing flow:"
  //<< processing_flow.toString();

  // Get the ms run data set that for the graph we are going to base the
  // integration on.
  MsRunDataSetCstSPtr ms_run_data_set_csp =
    processing_flow.getMsRunDataSetCstSPtr();
  if(ms_run_data_set_csp == nullptr)
    qFatal("Cannot be that the pointer is nullptr.");

  // Pass the integrator the flow we got as param and that describes in its most
  // recent step the integration that it should perform.
  MsRunDataSetTreeMassDataIntegratorToRt *mass_data_integrator_p =
    new MsRunDataSetTreeMassDataIntegratorToRt(ms_run_data_set_csp,
                                               processing_flow);

  // Ensure the mass data integrator messages are used.

  connect(mass_data_integrator_p,
          &MassDataIntegrator::logTextToConsoleSignal,
          mp_programWindow,
          &ProgramWindow::logTextToConsole);

  // qDebug() << "the integrator pointer:" << mass_data_integrator_p;

  // Allocate a mass data integrator to integrate the data.

  MassDataIntegratorTask *mass_data_integrator_task_p =
    new MassDataIntegratorTask();

  // This signal starts the computation in the MassDataIntegratorTask object.
  connect(
    this,
    // SIGNAL(integrateToRtSignal(MsRunDataSetTreeMassDataIntegratorToRt
    // *)),
    static_cast<void (TicXicChromTracePlotWnd::*)(
      MsRunDataSetTreeMassDataIntegratorToRt *)>(
      &TicXicChromTracePlotWnd::integrateToRtSignal),
    mass_data_integrator_task_p, // SLOT(integrateToRt(MsRunDataSetTreeMassDataIntegratorToRt
                                 // *)),
    static_cast<void (MassDataIntegratorTask::*)(
      MsRunDataSetTreeMassDataIntegratorToRt *)>(
      &MassDataIntegratorTask::integrateToRt),
    // Fundamental for signals that travel across QThread instances...
    Qt::QueuedConnection);

  // Allocate the thread in which the integrator task will run.
  QThread *thread_p = new QThread;

  // Move the task to the matching thread.
  mass_data_integrator_task_p->moveToThread(thread_p);
  thread_p->start();

  // When the read task finishes, it sends a signal that we trap to go on with
  // the plot widget creation stuff.

  // Since we allocated the QThread dynamically we need to be able to destroy it
  // later, so make the connection.
  connect(mass_data_integrator_task_p,
          static_cast<void (MassDataIntegratorTask::*)(MassDataIntegrator *)>(
            &MassDataIntegratorTask::finishedIntegratingDataSignal),
          this,
          [this,
           thread_p,
           mass_data_integrator_p,
           parent_plottable_p,
           mass_data_integrator_task_p]() {
            // Do not forget that we have to delete the MassDataIntegratorTask
            // allocated instance.
            mass_data_integrator_task_p->deleteLater();
            // Once the task has been labelled to be deleted later, we can stop
            // the thread and ask for it to also be deleted later.
            thread_p->deleteLater(), thread_p->quit();
            thread_p->wait();
            this->finishedIntegratingToRt(mass_data_integrator_p,
                                          parent_plottable_p);
          });


  // Allocate a new TaskMonitorCompositeWidget that will receive all the
  // integrator's signals and provide feedback to the user about the ongoing
  // integration.

  TaskMonitorCompositeWidget *task_monitor_composite_widget_p =
    mp_programWindow->getTaskMonitorWnd()->addTaskMonitorWidget(Qt::red);

  // Initialize the monitor composite widget's widgets and make all the
  // connections mass data integrator <--> widget.

  task_monitor_composite_widget_p->setMsRunIdText(
    ms_run_data_set_csp->getMsRunId()->getSampleName());
  task_monitor_composite_widget_p->setTaskDescriptionText(
    "Integrating to XIC chromatogram.");
  task_monitor_composite_widget_p->setProgressBarMinValue(0);

  // Make the connections

  // When the integrator task instance has finished working, it will send a
  // signal that we trap to finally destroy (after a time lag of some seconds,
  // the monitor widget.

  connect(mass_data_integrator_task_p,
          static_cast<void (MassDataIntegratorTask::*)(MassDataIntegrator *)>(
            &MassDataIntegratorTask::finishedIntegratingDataSignal),
          task_monitor_composite_widget_p,
          &TaskMonitorCompositeWidget::taskFinished,
          Qt::QueuedConnection);

  // If the user clicks the cancel button, relay the signal to the loader.
  connect(task_monitor_composite_widget_p,
          &TaskMonitorCompositeWidget::cancelTaskSignal,
          mass_data_integrator_p,
          &MassDataIntegrator::cancelOperation);

  // We need to register the meta type for std::size_t because otherwise it
  // cannot be shipped though signals.

  qRegisterMetaType<std::size_t>("std::size_t");

  // Now make all the connections that will allow the integrator to provide
  // dynamic feedback to the user via the task monitor widget.
  task_monitor_composite_widget_p->makeMassDataIntegratorConnections(
    mass_data_integrator_p);

  // qDebug() << "going to emit integrateToMzSignal with mass "
  //"data integrator:"
  //<< mass_data_integrator_p;

  emit integrateToRtSignal(mass_data_integrator_p);

  // We do not want to make signal/slot calls more than once. This is because
  // one user might well trigger more than one integration from this window to a
  // mass spectrum. Thus we do not want that *this window be still connected to
  // the specific mass_data_integrator_task_p when a new integration is
  // triggered. We want the signal/slot pairs to be contained to specific
  // objects. Each TicXicChromTracePlotWnd::integrateToMz() call must be contained
  // to a this/mass_data_integrator_task_p specific signal/slot pair.
  disconnect(
    this,
    // SIGNAL(integrateToRtSignal(MsRunDataSetTreeMassDataIntegratorToRt *)),
    static_cast<void (TicXicChromTracePlotWnd::*)(
      MsRunDataSetTreeMassDataIntegratorToRt *)>(
      &TicXicChromTracePlotWnd::integrateToRtSignal),
    mass_data_integrator_task_p,
    // SLOT(integrateToRt(MsRunDataSetTreeMassDataIntegratorToRt *)));
    static_cast<void (MassDataIntegratorTask::*)(
      MsRunDataSetTreeMassDataIntegratorToRt *)>(
      &MassDataIntegratorTask::integrateToRt));
}


// Old version
void
TicXicChromTracePlotWnd::finishedIntegratingToRt(
  MsRunDataSetTreeMassDataIntegratorToRt *mass_data_integrator_p,
  QCPAbstractPlottable *parent_plottable_p)
{
  // This function is actually a slot that is called when the integration
  // is terminated in a QThread.

  // qDebug() << "the integrator pointer:" << mass_data_integrator_p;

  // We get back the integrator that was made to work in another thread. Now get
  // the obtained data and work with them.

  // Store the MsRunDataSetSPtr for later use before deleting the integrattor.

  MsRunDataSetCstSPtr ms_run_data_set_csp =
    mass_data_integrator_p->getMsRunDataSet();

  pappso::Trace integrated_trace =
    mass_data_integrator_p->getMapTrace().toTrace();
  // qDebug() << "mass spectrum trace size:" << integrated_trace.size();

  if(!integrated_trace.size())
    {
      qDebug() << "There is not a single point in the integrated trace.";

      return;
    }

  // Also, do not forget to copy the processing flow from the integrator. This
  // processing flow will be set into the plot widget !

  ProcessingFlow processing_flow = mass_data_integrator_p->getProcessingFlow();

  // Sanity check
  if(ms_run_data_set_csp != processing_flow.getMsRunDataSetCstSPtr())
    qFatal("Cannot be that the ms run data set pointers be different.");

  // Finally, do not forget that we need to delete the integrator now that the
  // calculation is finished.
  delete mass_data_integrator_p;
  mass_data_integrator_p = nullptr;

  // At this point, we need to get a color for the plot. That color needs to be
  // that of the parent.
  QColor plot_color = parent_plottable_p->pen().color();

  // We need to establish what widget is the destination of the integration. If
  // one or more than a widget is/are pinned-down, then that/these should be the
  // destinations. If no widget is pinned-down, then a new plot widget needs to
  // be created.

  std::vector<BasePlotCompositeWidget *> pinned_down_widgets =
    pinnedDownWidgets();

  if(pinned_down_widgets.size())
    {
      for(auto &&pinned_down_widget : pinned_down_widgets)
        static_cast<BaseTracePlotCompositeWidget *>(pinned_down_widget)
          ->addTrace(integrated_trace,
                     static_cast<QCPGraph *>(parent_plottable_p),
                     ms_run_data_set_csp,
                     ms_run_data_set_csp->getMsRunId()->getSampleName(),
                     processing_flow,
                     plot_color);
    }
  else
    addTracePlot(integrated_trace,
                 ms_run_data_set_csp,
                 processing_flow,
                 plot_color,
                 static_cast<QCPGraph *>(parent_plottable_p));

  // At this point, if the window is really not visible, then show it, otherwise
  // I was told that the user does not think that they can ask for the window to
  // show up in the main program window.

  if(!isVisible())
    {
      showWindow();
    }
}
#endif

void
TicXicChromTracePlotWnd::integrateToRt(
  QCPAbstractPlottable *parent_plottable_p,
  std::shared_ptr<std::vector<pappso::QualifiedMassSpectrumCstSPtr>>
    qualified_mass_spectra_sp,
  const ProcessingFlow &processing_flow)
{
  // qDebug();

  // qDebug().noquote() << "Integrating to mz with processing flow:"
  //<< processing_flow.toString();

  // Get the ms run data set that for the graph we are going to base the
  // integration on.
  MsRunDataSetCstSPtr ms_run_data_set_csp =
    processing_flow.getMsRunDataSetCstSPtr();
  if(ms_run_data_set_csp == nullptr)
    qFatal("Cannot be that the pointer is nullptr.");

  // We will try to limit the number of mass spectra to iterate through with the
  // integration visitor.

  double start_rt = std::numeric_limits<double>::infinity();
  double end_rt   = std::numeric_limits<double>::infinity();

  bool integration_rt = processing_flow.innermostRtRange(start_rt, end_rt);

  if(!integration_rt)
    qFatal("Programming error.");

  // First prepare a vector of QualifiedMassSpectrumCstSPtr

  std::size_t qualified_mass_spectra_count = fillInQualifiedMassSpectraVector(
    ms_run_data_set_csp, qualified_mass_spectra_sp, processing_flow);

  if(!qualified_mass_spectra_count)
    return;

  // qDebug() << "The number of remaining mass spectra:"
  //<< qualified_mass_spectra_count;

  // Now start the actual integration work.

  // Allocate a mass data integrator to integrate the data.

  QualifiedMassSpectrumVectorMassDataIntegratorToRt *mass_data_integrator_p =
    new QualifiedMassSpectrumVectorMassDataIntegratorToRt(
      ms_run_data_set_csp, processing_flow, qualified_mass_spectra_sp);

  // Ensure the mass data integrator messages are used.

  connect(
    mass_data_integrator_p,
    &QualifiedMassSpectrumVectorMassDataIntegrator::logTextToConsoleSignal,
    mp_programWindow,
    &ProgramWindow::logTextToConsole);

  // qDebug() << "Receiving processing flow:" << processing_flow.toString();

  MassDataIntegratorTask *mass_data_integrator_task_p =
    new MassDataIntegratorTask();

  // This signal starts the computation in the MassDataIntegratorTask object.
  connect(this,

          static_cast<void (TicXicChromTracePlotWnd::*)(
            QualifiedMassSpectrumVectorMassDataIntegratorToRt *)>(
            &TicXicChromTracePlotWnd::integrateToRtSignal),

          mass_data_integrator_task_p,

          static_cast<void (MassDataIntegratorTask::*)(
            QualifiedMassSpectrumVectorMassDataIntegratorToRt *)>(
            &MassDataIntegratorTask::integrateToRt),

          // Fundamental for signals that travel across QThread instances...
          Qt::QueuedConnection);

  // Allocate the thread in which the integrator task will run.
  QThread *thread_p = new QThread;

  // Move the task to the matching thread.
  mass_data_integrator_task_p->moveToThread(thread_p);
  thread_p->start();

  // Since we allocated the QThread dynamically we need to be able to destroy it
  // later, so make the connection.

  connect(mass_data_integrator_task_p,

          static_cast<void (MassDataIntegratorTask::*)(
            QualifiedMassSpectrumVectorMassDataIntegrator *)>(
            &MassDataIntegratorTask::finishedIntegratingDataSignal),

          this,

          [this,
           thread_p,
           mass_data_integrator_p,
           parent_plottable_p,
           mass_data_integrator_task_p]() {
            // Do not forget that we have to delete the MassDataIntegratorTask
            // allocated instance.
            mass_data_integrator_task_p->deleteLater();
            // Once the task has been labelled to be deleted later, we can stop
            // the thread and ask for it to also be deleted later.
            thread_p->deleteLater(), thread_p->quit();
            thread_p->wait();
            this->finishedIntegratingToRt(mass_data_integrator_p,
                                          parent_plottable_p);
          });


  // qDebug() << "the integrator pointer:" << mass_data_integrator_p;

  // Allocate a new TaskMonitorCompositeWidget that will receive all the
  // integrator's signals and provide feedback to the user about the ongoing
  // integration.

  TaskMonitorCompositeWidget *task_monitor_composite_widget_p =
    mp_programWindow->mp_taskMonitorWnd->addTaskMonitorWidget(Qt::red);

  // Initialize the monitor composite widget's widgets and make all the
  // connections mass data integrator <--> widget.

  task_monitor_composite_widget_p->setMsRunIdText(
    ms_run_data_set_csp->getMsRunId()->getSampleName());
  task_monitor_composite_widget_p->setTaskDescriptionText(
    "Integrating to a TIC/XIC chromatogram");
  task_monitor_composite_widget_p->setProgressBarMinValue(0);

  // Make the connections

  // When the MsRunReadTask instance has finished working, it will send a signal
  // that we trap to finally destroy (after a time lag of some seconds, the
  // monitor widget.

  connect(mass_data_integrator_task_p,

          static_cast<void (MassDataIntegratorTask::*)(
            QualifiedMassSpectrumVectorMassDataIntegrator *)>(
            &MassDataIntegratorTask::finishedIntegratingDataSignal),

          task_monitor_composite_widget_p,

          &TaskMonitorCompositeWidget::taskFinished,

          Qt::QueuedConnection);

  // If the user clicks the cancel button, relay the signal to the loader.
  connect(task_monitor_composite_widget_p,
          &TaskMonitorCompositeWidget::cancelTaskSignal,
          mass_data_integrator_p,
          &QualifiedMassSpectrumVectorMassDataIntegrator::cancelOperation);

  // We need to register the meta type for std::size_t because otherwise it
  // cannot be shipped though signals.

  qRegisterMetaType<std::size_t>("std::size_t");

  task_monitor_composite_widget_p->makeMassDataIntegratorConnections(
    mass_data_integrator_p);

  emit integrateToRtSignal(mass_data_integrator_p);

  // We do not want to make signal/slot calls more than once. This is because
  // one user might well trigger more than one integration from this window to a
  // mass spectrum. Thus we do not want that *this window be still connected to
  // the specific mass_data_integrator_task_p when a new integration is
  // triggered. We want the signal/slot pairs to be contained to specific
  // objects. Each TicXicChromTracePlotWnd::integrateToRt() call must be
  // contained to a this/mass_data_integrator_task_p specific signal/slot pair.
  disconnect(this,

             static_cast<void (TicXicChromTracePlotWnd::*)(
               QualifiedMassSpectrumVectorMassDataIntegratorToRt *)>(
               &TicXicChromTracePlotWnd::integrateToRtSignal),

             mass_data_integrator_task_p,

             static_cast<void (MassDataIntegratorTask::*)(
               QualifiedMassSpectrumVectorMassDataIntegratorToRt *)>(
               &MassDataIntegratorTask::integrateToRt));
}


void
TicXicChromTracePlotWnd::finishedIntegratingToRt(
  QualifiedMassSpectrumVectorMassDataIntegrator *mass_data_integrator_p,
  QCPAbstractPlottable *parent_plottable_p)
{
  // qDebug();

  // This function is actually a slot that is called when the integration to mz
  // is terminated in a QThread.

  // qDebug() << "the integrator pointer:" << mass_data_integrator_p;

  // We get back the integrator that was made to work in another thread. Now get
  // the obtained data and work with them.

  // Store the MsRunDataSetSPtr for later use before deleting the integrattor.

  MsRunDataSetCstSPtr ms_run_data_set_csp =
    mass_data_integrator_p->getMsRunDataSet();

  pappso::Trace integrated_trace =
    mass_data_integrator_p->getMapTrace().toTrace();
  // qDebug() << "mass spectrum trace size:" << integrated_trace.size();

  if(!integrated_trace.size())
    {
      qDebug() << "There is not a single point in the integrated trace.";
      return;
    }

  // Also, do not forget to copy the processing flow from the integrator. This
  // processing flow will be set into the plot widget !

  ProcessingFlow processing_flow = mass_data_integrator_p->getProcessingFlow();

  // Sanity check
  if(ms_run_data_set_csp != processing_flow.getMsRunDataSetCstSPtr())
    qFatal("Cannot be that the ms run data set pointers be different.");

  // Finally, do not forget that we need to delete the integrator now that the
  // calculation is finished.
  delete mass_data_integrator_p;
  mass_data_integrator_p = nullptr;

  // At this point, we need to get a color for the plot. That color needs to be
  // that of the parent.

  QColor plot_color;

  // There are two situations: either the integration started from a plot widget
  // and the color is certainly obtainable via the pen of the plot.
  if(parent_plottable_p != nullptr)
    plot_color = parent_plottable_p->pen().color();
  else
    // Or the integration started at the MsRunDataSetTableView and the color can
    // only be obtained via the MsRunDataSet in the OpenMsRunDataSetsDlg.
    plot_color = mp_programWindow->getColorForMsRunDataSet(ms_run_data_set_csp);

  // The new trace checks if there are push-pinned widgets. If so, the new trace
  // will be apportioned to the widgets according to the required mechanics:
  // new plot, sum, combine...

  newTrace(integrated_trace,
           ms_run_data_set_csp,
           processing_flow,
           plot_color,
           static_cast<QCPGraph *>(parent_plottable_p));

  // At this point, if the window is really not visible, then show it, otherwise
  // I was told that the user does not think that they can ask for the window to
  // show up in the main program window.

  if(!isVisible())
    {
      showWindow();
    }
}


void
TicXicChromTracePlotWnd::integrateToMz(QCPAbstractPlottable *parent_plottable_p,
                                       const ProcessingFlow &processing_flow)
{
  if(parent_plottable_p == nullptr)
    qFatal("Cannot be that pointer is nullptr.");

  // We do not handle integrations to m/z, because each trace plot window
  // handles their own data. So we just relay this call to the main program
  // window that will in turn relay it to the mass spec plot window.

  // qDebug().noquote() << "Integrating to mz for graph with processing flow:"
  //<< processing_flow.toString();

  mp_programWindow->integrateToMz(parent_plottable_p, nullptr, processing_flow);
}


void
TicXicChromTracePlotWnd::integrateToDt(QCPAbstractPlottable *parent_plottable_p,
                                       const ProcessingFlow &processing_flow)
{
  if(parent_plottable_p == nullptr)
    qFatal("Cannot be that pointer is nullptr.");

  // We do not handle integrations to dt, because each trace plot window
  // handles their own data. So we just relay this call to the main program
  // window that will in turn relay it to the mass spec plot window.

  mp_programWindow->integrateToDt(parent_plottable_p, nullptr, processing_flow);
}


void
TicXicChromTracePlotWnd::integrateToDtMz(
  QCPAbstractPlottable *parent_plottable_p,
  const ProcessingFlow &processing_flow)
{
  if(parent_plottable_p == nullptr)
    qFatal("Cannot be that pointer is nullptr.");

  // We do not handle integrations to dt, because each trace plot window
  // handles their own data. So we just relay this call to the main program
  // window that will in turn relay it to the mass spec plot window.

  mp_programWindow->integrateToDtMz(
    parent_plottable_p, nullptr, processing_flow);
}


void
TicXicChromTracePlotWnd::integrateToRtMz(
  QCPAbstractPlottable *parent_plottable_p,
  const ProcessingFlow &processing_flow)
{
  if(parent_plottable_p == nullptr)
    qFatal("Cannot be that pointer is nullptr.");

  mp_programWindow->integrateToRtMz(
    parent_plottable_p, nullptr, processing_flow);
}


} // namespace minexpert

} // namespace msxps
