/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QObject>
#include <QString>
#include <QWidget>


/////////////////////// Local includes
#include "ui_ConsoleWnd.h"

namespace msxps
{
namespace minexpert
{

//! The ConsoleWnd class provides a feedback console window.
/*!

  The ConsoleWnd class is available in mineXpert to display a number of
  message for the user to have feedback on the various tasks running in the
  program. The widget is a simple multiline edit widget.

 */
class ConsoleWnd : public QMainWindow
{
  Q_OBJECT

  private:
  Ui::ConsoleWnd m_ui;

  QString m_lastLoggedText;

  protected:
  //! Name of the application.
  QString m_applicationName;

  void closeEvent(QCloseEvent *event);

  public:
  ConsoleWnd(QWidget *parent, const QString &applicationName);
  ~ConsoleWnd();

  Q_INVOKABLE void writeSettings();
  Q_INVOKABLE void readSettings();

  void logTextToConsole(QString msg);
  void logColoredTextToConsole(QString text, const QColor &color);

  Q_INVOKABLE void hide();
  Q_INVOKABLE void show();
};

} // namespace minexpert

} // namespace msxps
