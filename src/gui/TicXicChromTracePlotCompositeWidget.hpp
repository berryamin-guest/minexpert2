
/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QWidget>

/////////////////////// QCustomPlot


/////////////////////// pappsomspp includes
#include <pappsomspp/widget/plotwidget/basetraceplotwidget.h>
#include <pappsomspp/trace/trace.h>


/////////////////////// Local includes
#include "BaseTracePlotCompositeWidget.hpp"


namespace msxps
{
namespace minexpert
{

// The TicXicChromTracePlotCompositeWidget class is aimed at implementing TIC /
// XIC-specific functions. In particular the integrationRequested() function
// needs to be specialized because that function would not work if the data were
// of a MassSpectrum or a DriftSpectrum type.
class TicXicChromTracePlotCompositeWidget : public BaseTracePlotCompositeWidget
{
  Q_OBJECT

  public:
  explicit TicXicChromTracePlotCompositeWidget(
    QWidget *parent,
    const QString &x_axis_label,
    const QString &y_axis_label);
  virtual ~TicXicChromTracePlotCompositeWidget();

  public slots:

  virtual void lastCursorHoveredPoint(const QPointF &pointf) override;

  virtual void
  plotWidgetKeyPressEvent(const pappso::BasePlotContext &context) override;

  virtual void moveMouseCursorToNextGraphPoint(
    const pappso::BasePlotContext &context) override;

  void integrationRequested(const pappso::BasePlotContext &context);

	QString craftAnalysisStanza(const pappso::BasePlotContext &context);

  signals:

  private:
  void setupWidget() override;
};


} // namespace minexpert

} // namespace msxps
