/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QTimer>
#include <QIcon>
#include <QDebug>


/////////////////////// pappsomspp includes
#include <pappsomspp/msfile/msfilereader.h>
#include <pappsomspp/msfile/msfileaccessor.h>
#include <pappsomspp/msrun/msrunid.h>
#include <pappsomspp/msrun/msrunreader.h>
#include <pappsomspp/utils.h>
#include <pappsomspp/processing/combiners/tracepluscombiner.h>
#include <pappsomspp/processing/combiners/massspectrumpluscombiner.h>
#include <pappsomspp/trace/trace.h>
#include <pappsomspp/trace/maptrace.h>


/////////////////////// Local includes
#include "Application.hpp"
#include "nongui/MzIntegrationParams.hpp"
#include "gui/MsRunDataSetTableView.hpp"
#include "gui/MsRunDataSetTableViewItem.hpp"
#include "gui/MsRunDataSetTableViewModel.hpp"
#include "gui/MsRunDataSetTableViewProxyModel.hpp"


namespace msxps
{
namespace minexpert
{

void
myMessageOutputFormat(QtMsgType type,
                      const QMessageLogContext &context,
                      const QString &msg)
{
  QByteArray localMsg  = msg.toLocal8Bit();
  const char *file     = context.file ? context.file : "";
  const char *function = context.function ? context.function : "";
  switch(type)
    {
      case QtDebugMsg:
        fprintf(stderr,
                "Debug: %s@%u -- %s() %s\n",
                file,
                context.line,
                function,
                localMsg.constData());
        break;
      case QtInfoMsg:
        fprintf(stderr,
                "Info: %s (%s:%u, %s)\n",
                localMsg.constData(),
                file,
                context.line,
                function);
        break;
      case QtWarningMsg:
        fprintf(stderr,
                "Warning: %s (%s:%u, %s)\n",
                localMsg.constData(),
                file,
                context.line,
                function);
        break;
      case QtCriticalMsg:
        fprintf(stderr,
                "Critical: %s (%s:%u, %s)\n",
                localMsg.constData(),
                file,
                context.line,
                function);
        break;
      case QtFatalMsg:
        fprintf(stderr,
                "Fatal: %s (%s:%u, %s)\n",
                localMsg.constData(),
                file,
                context.line,
                function);
        break;
    }
}


Application::Application(int &argc, char **argv, const QString &moduleName)
  : QApplication{argc, argv}, m_moduleName{moduleName}
{
  QPixmap splash_pixmap(":/images/splashscreen.png");

  mpa_splash = new QSplashScreen(splash_pixmap, Qt::WindowStaysOnTopHint);
  mpa_splash->show();

  QTimer::singleShot(2000, this, SLOT(destroySplash()));

  // We now have to perform a number of initialization steps.

  QCoreApplication::setOrganizationName(m_moduleName);
  QCoreApplication::setOrganizationDomain("msxpertsuite.org");
  QCoreApplication::setApplicationName(m_moduleName);

  // The default window icon.
  QString pixmap_file_name = ":/images/icons/32x32/minexpert2.png";
  QPixmap icon_pixmap(pixmap_file_name);
  QIcon icon(icon_pixmap);
  setWindowIcon(icon);

  // Set the debugging message formatting pattern.
  // qSetMessagePattern(QString("Debug: %{file}@%{line}-%{function}():
  // %{message}"));
  qInstallMessageHandler(myMessageOutputFormat);
}


Application::~Application()
{
}


void
Application::setDescription(QString &desc)
{
  m_description = desc;
}


QString
Application::description()
{
  return m_description;
}


void
Application::destroySplash()
{
  delete mpa_splash;
  mpa_splash = 0;
}


void
Application::logToConsole(QString msg)
{
  qDebug() << "Should log message:" << msg;
}

} // namespace minexpert

} // namespace msxps
