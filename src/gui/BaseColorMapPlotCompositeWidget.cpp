/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QMessageBox>

/////////////////////// QCustomPlot


/////////////////////// pappsomspp includes
#include <pappsomspp/processing/combiners/tracepluscombiner.h>


/////////////////////// Local includes
#include "BaseColorMapPlotCompositeWidget.hpp"
#include "BaseColorMapPlotWnd.hpp"
#include "ColorSelector.hpp"
#include "MsFragmentationSpecDlg.hpp"
#include "MzIntegrationParamsDlg.hpp"
#include "ProgramWindow.hpp"

namespace msxps
{
namespace minexpert
{


BaseColorMapPlotCompositeWidget::BaseColorMapPlotCompositeWidget(
  QWidget *parent, const QString &x_axis_label, const QString &y_axis_label)
  : BasePlotCompositeWidget(parent, x_axis_label, y_axis_label)
{
  // qDebug();

  createMainMenu();

  // We hide  all the buttons that configure the destination of new
  // integrations. We do not support the stuffing more than one color map into a
  // color map plot, contrary to what we do with graphs.

  // m_ui.pushPinPushButton->hide();
  m_ui.eraseTraceCreateNewPushButton->hide();
  m_ui.keepTraceCombineNewPushButton->hide();
  m_ui.keepTraceCreateNewPushButton->hide();
}


BaseColorMapPlotCompositeWidget::~BaseColorMapPlotCompositeWidget()
{
  // qDebug();
}


void
BaseColorMapPlotCompositeWidget::createMainMenu()
{
  // qDebug();

  // Now append new ColorMap-specific menu

  // The axes transposition.
  QAction *transpose_axes_p = new QAction("Transpose axes", this);
  transpose_axes_p->setStatusTip(tr("Transpose axes"));
  connect(transpose_axes_p, &QAction::triggered, [this]() {
    static_cast<pappso::BaseColorMapPlotWidget *>(mp_plotWidget)
      ->transposeAxes();
  });

  mp_mainMenu->addAction(transpose_axes_p);
}


QCPColorMap *
BaseColorMapPlotCompositeWidget::addColorMap(
  std::shared_ptr<std::map<double, pappso::MapTrace>> &double_map_trace_map_sp,
  const pappso::ColorMapPlotConfig &color_map_plot_config,
  QCPAbstractPlottable *parent_plottable_p,
  MsRunDataSetCstSPtr ms_run_data_set_csp,
  const QString &sample_name,
  const ProcessingFlow &processing_flow,
  const QColor &color)
{
  // qDebug() << "Adding color map with config:" <<
  // color_map_plot_config.toString();

  // If color is not valid, we need to create one.

  QColor local_color(color);

  // Get color from the available colors, or if none is available, create one
  // randomly without requesting the user to select one from QColorDialog.
  if(!local_color.isValid())
    local_color = ColorSelector::getColor(true);

  // Contrary to what we do with graphs, we cannot stuff more than one color map
  // into a color map plot, so just add a new plot.

  QCPColorMap *color_map_p = nullptr;

  color_map_p = static_cast<pappso::BaseColorMapPlotWidget *>(mp_plotWidget)
                  ->addColorMap(double_map_trace_map_sp,
                                color_map_plot_config,
                                local_color);

  // Each time a new trace is added anywhere, it needs to be documented
  // in the main program window's graph nodes tree! This is what we call
  // the ms run data plot graph filiation documentation.

  mp_parentWnd->getProgramWindow()->documentMsRunDataPlottableFiliation(
    ms_run_data_set_csp, color_map_p, parent_plottable_p, this);

  // Map the graph_p to the processing flow that we got to document how
  // the trace was obtained in the first place.
  m_plottableProcessingFlowMap[color_map_p] = processing_flow;

  // Now set the name of the sample to the trace label.

  m_ui.sampleNameLabel->setText(sample_name);

  mp_plotWidget->replot();

  // The setFocus() call below will trigger the mp_plotWidget to send a signal
  // that will be caught by the parent window that will iterate in all the plot
  // widget and set their background to focused/unfocused color according to
  // their focus status.
  // mp_plotWidget->setFocus();

  // color_map_p might be nullptr.
  return color_map_p;
}


pappso::DataKind
BaseColorMapPlotCompositeWidget::xAxisKind() const
{
  return static_cast<pappso::BaseColorMapPlotWidget *>(mp_plotWidget)
    ->xAxisDataKind();
}


pappso::DataKind
BaseColorMapPlotCompositeWidget::yAxisKind() const
{
  return static_cast<pappso::BaseColorMapPlotWidget *>(mp_plotWidget)
    ->yAxisDataKind();
}


} // namespace minexpert

} // namespace msxps
