/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

#pragma once


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QMainWindow>
#include <QStringList>
#include <QMenu>


/////////////////////// pappsomspp includes
#include <pappsomspp/msrun/msrundatasettree.h>
#include <pappsomspp/massspectrum/qualifiedmassspectrum.h>


/////////////////////// Local includes
#include "ui_MsRunDataSetTableViewWnd.h"
#include "MsRunDataSetTableView.hpp"
#include "MsRunDataSetTableViewModel.hpp"
#include "MsRunDataSetTableViewProxyModel.hpp"
#include "../nongui/MsRunDataSet.hpp"
#include "../nongui/MzIntegrationParams.hpp"
#include "../nongui/ProcessingFlow.hpp"


namespace msxps
{
namespace minexpert
{


class MsRunDataSet;
class ProgramWindow;
class MsFragmentationSpecDlg;
class MzIntegrationParamsDlg;


class MsRunDataSetTableViewWnd : public QMainWindow
{
  Q_OBJECT

  public:
  // Construction/destruction
  MsRunDataSetTableViewWnd(QWidget *parent_p,
                           ProgramWindow *program_window_p,
                           MsRunDataSetCstSPtr ms_run_data_set_csp,
                           const QColor &color);

  virtual ~MsRunDataSetTableViewWnd();

  bool initialize();
  void closeEvent(QCloseEvent *event);
  void writeSettings();
  void readSettings();

  void show();

  void executeFilteringPushButtonClicked();

  void showMzIntegrationParamsDlg();
  void mzIntegrationParamsChanged(MzIntegrationParams mz_integration_params);

  public slots:

  void ticIntensityValue(double tic_intensity);

  void keyPressEvent(QKeyEvent *event);

  protected:
  ProgramWindow *mp_programWindow = nullptr;

  ProcessingFlow m_processingFlow;

  //! Graphical interface definition.
  Ui::MsRunDataSetTableViewWnd m_ui;

  QToolBar *mp_toolBar               = nullptr;
  QMenu *mp_mainMenu                 = nullptr;
  QPushButton *mp_mainMenuPushButton = nullptr;

  MsRunDataSetCstSPtr mcsp_msRunDataSet                 = nullptr;
  pappso::MsRunDataSetTreeCstSPtr mcsp_msRunDataSetTree = nullptr;

  QColor m_color;

  MsRunDataSetTableViewModel *mp_msRunDataSetTableViewModel           = nullptr;
  MsRunDataSetTableViewProxyModel *mp_msRunDataSetTableViewProxyModel = nullptr;
  MsRunDataSetTableView *mp_msRunDataSetTableView                     = nullptr;

  QItemSelectionModel *mp_msRunDataSetTableViewSelectionModel = nullptr;

  MsFragmentationSpecDlg *mp_msFragmentationSpecDlg = nullptr;
  MzIntegrationParamsDlg *mp_mzIntegrationParamsDlg = nullptr;

  std::vector<pappso::QualifiedMassSpectrumCstSPtr>
    m_qualifiedMassSpectraToIntegrate;

  void createMainMenu();

  std::vector<pappso::QualifiedMassSpectrumCstSPtr>
  recordQualifiedMassSpectraToIntegrate(double &begin_rt,
                                        double &end_rt,
                                        double &begin_dt,
                                        double &end_dt) const;
  void integrationRangesForRtAndDt(double &begin_rt,
                                   double &end_rt,
                                   double &begin_dt,
                                   double &end_dt) const;

  void integrateToMz();
  void integrateToDt();
  void integrateToRt();
  void integrateToTicIntensity();
  void integrateToDtMz();
  void integrateToRtMz();
};


} // namespace minexpert

} // namespace msxps
