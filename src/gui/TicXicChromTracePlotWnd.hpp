/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

#pragma once


/////////////////////// StdLib includes


/////////////////////// Qt includes


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "BaseTracePlotWnd.hpp"
#include "TicXicChromTracePlotCompositeWidget.hpp"
#include "../nongui/MsRunDataSet.hpp"
#include "../nongui/MsRunDataSetTreeMassDataIntegratorToRt.hpp"

#include "../nongui/QualifiedMassSpectrumVectorMassDataIntegratorToRt.hpp"

namespace msxps
{
namespace minexpert
{

class ProcessingFlow;

class TicXicChromTracePlotWnd : public BaseTracePlotWnd
{
  Q_OBJECT

  public:
  // Construction/destruction
  TicXicChromTracePlotWnd(QWidget *parent,
                          const QString &title,
                          const QString &settingsTitle,
                          const QString &description = QString());
  virtual ~TicXicChromTracePlotWnd();

  virtual QCPGraph *
  addTracePlot(const pappso::Trace &trace,
               MsRunDataSetCstSPtr ms_run_data_set_csp,
               const ProcessingFlow &processing_flow,
               const QColor &color,
               QCPAbstractPlottable *parent_plottable_p) override;

  void
  calculateInitialTicChromatogram(MsRunDataSetCstSPtr &ms_run_data_set_csp);

  void integrateToRt(QCPAbstractPlottable *plottable_p,
    std::shared_ptr<std::vector<pappso::QualifiedMassSpectrumCstSPtr>>
      qualified_mass_spectra_sp,
                     const ProcessingFlow &processing_flow);

  void integrateToMz(QCPAbstractPlottable *plottable_p,
                     const ProcessingFlow &processing_flow);
  void integrateToDt(QCPAbstractPlottable *plottable_p,
                     const ProcessingFlow &processing_flow);
  void integrateToDtMz(QCPAbstractPlottable *plottable_p,
                       const ProcessingFlow &processing_flow);
  void integrateToRtMz(QCPAbstractPlottable *plottable_p,
                       const ProcessingFlow &processing_flow);

  public slots:

  std::pair<MsRunDataSetCstSPtr, QColor>
  finishedIntegratingToInitialTicChromatogram(
    MsRunDataSetTreeMassDataIntegratorToRt *mass_data_integrator_p);

  void finishedIntegratingToRt(
    QualifiedMassSpectrumVectorMassDataIntegrator *mass_data_integrator_p,
    QCPAbstractPlottable *parent_plottable_p);

  signals:
  void integrateToInitialTicChromatogramSignal(
    MsRunDataSetTreeMassDataIntegratorToRt *mass_data_integrator_p);

  void finishedIntegratingToInitialTicChromatogramSignal(
    MsRunDataSetCstSPtr ms_run_data_set_csp);

  void integrateToRtSignal(
    MsRunDataSetTreeMassDataIntegratorToRt *mass_data_integrator_p);

  void integrateToRtSignal(
    QualifiedMassSpectrumVectorMassDataIntegratorToRt *mass_data_integrator_p);

  protected:
};


} // namespace minexpert

} // namespace msxps
