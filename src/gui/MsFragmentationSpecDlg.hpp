/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

#pragma once


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QDialog>
#include <QStringList>
#include <QMenu>


/////////////////////// pappsomspp includes
#include <pappsomspp/widget/precisionwidget/precisionwidget.h>


/////////////////////// Local includes
#include "ui_MsFragmentationSpecDlg.h"
#include "../nongui/MsRunDataSet.hpp"
#include "../nongui/MsFragmentationSpec.hpp"


namespace msxps
{
namespace minexpert
{

class MsFragmentationSpec;

class MsFragmentationSpecDlg : public QDialog
{
  Q_OBJECT

  public:
  // Construction/destruction
  MsFragmentationSpecDlg(QWidget *parent = nullptr);
  MsFragmentationSpecDlg(QWidget *parent,
                         const MsFragmentationSpec &ms_fragmentation_spec,
                         const QColor &color);
  ~MsFragmentationSpecDlg();

  void writeSettings();
  void readSettings();

  void clearAllPushButtonClicked();

	void	keyPressEvent(QKeyEvent *event);

  void show();

	bool validateInputData();

  QString toString() const;

  public slots:
  void closeEvent(QCloseEvent *event);
  bool applyPushButtonClicked();

  signals:
  void
  msFragmentationSpecChangedSignal(MsFragmentationSpec ms_fragmentation_spec);
  void msFragmentationSpecDlgShouldBeDestroyedSignal();

  protected:
  Ui::MsFragmentationSpecDlg m_ui;
  MsFragmentationSpec m_msFragmentationSpec;

  pappso::PrecisionWidget *mp_precisionWidget;
  pappso::PrecisionPtr mp_precision =
    pappso::PrecisionFactory::getDaltonInstance(0.05);
  QColor m_color = Qt::black;

  void setupWidget();
};


} // namespace minexpert

} // namespace msxps
