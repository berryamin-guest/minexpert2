/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QColor>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "DataPlottableNode.hpp"
#include "../nongui/MsRunDataSet.hpp"
#include "DataPlottableNode.hpp"


namespace msxps
{
namespace minexpert
{

class DataPlottableTree;

typedef std::shared_ptr<DataPlottableTree> DataPlottableTreeSPtr;
typedef std::shared_ptr<const DataPlottableTree> DataPlottableTreeCstSPtr;


class DataPlottableTree
{
  public:
  DataPlottableTree();
  virtual ~DataPlottableTree();

  DataPlottableNode *addPlottable(MsRunDataSetCstSPtr ms_run_data_set_csp,
                                  QCPAbstractPlottable *plottable_p,
                                  QCPAbstractPlottable *parent_plottable_p,
                                  BasePlotCompositeWidget *plot_widget_p);

  const std::vector<DataPlottableNode *> &getRootNodes() const;

  DataPlottableNode *findNode(QCPAbstractPlottable *plottable_p);
  std::vector<DataPlottableNode *>
  findNodes(MsRunDataSetCstSPtr ms_run_data_set_csp, bool recursively);
  std::vector<DataPlottableNode *>
  findNodes(BasePlotCompositeWidget *base_plot_composite_widget_p,
            bool recursively);
  std::vector<DataPlottableNode *>::iterator
  findNode(DataPlottableNode *node_p);

  const BasePlotCompositeWidget *
  getPlotWidget(QCPAbstractPlottable *plottable_p);


  bool isRootNode(DataPlottableNode *node_p);
  bool isRootPlottable(QCPAbstractPlottable *plottable_p);

  bool reparent(DataPlottableNode *node_p,
                DataPlottableNode *new_parent_node_p);

  bool removeNodeAndPlottable(DataPlottableNode *node_p,
                              bool recursively            = false);

  // Utility functions.
  std::size_t depth() const;

  std::size_t size(bool look_deep = true) const;

  private:
  std::vector<DataPlottableNode *> m_rootNodes;
};


} // namespace minexpert

} // namespace msxps
