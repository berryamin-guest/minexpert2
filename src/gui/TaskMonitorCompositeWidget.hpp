
/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes
#include <chrono>


/////////////////////// Qt includes
#include <QWidget>
#include <QColor>

/////////////////////// QCustomPlot


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "ui_TaskMonitorCompositeWidget.h"
#include "../nongui/MassDataIntegrator.hpp"
#include "../nongui/MassSpecDataFileLoader.hpp"

#include "../nongui/QualifiedMassSpectrumVectorMassDataIntegrator.hpp"


namespace msxps
{
namespace minexpert
{


class TaskMonitorCompositeWidget : public QWidget
{
  Q_OBJECT

  public:
  QColor m_color = Qt::black;

  explicit TaskMonitorCompositeWidget(QWidget *parent);
  explicit TaskMonitorCompositeWidget(QWidget *parent, const QColor &color);
  virtual ~TaskMonitorCompositeWidget();

  void setColor(const QColor &color);
  const QColor &getColor() const;

  void
  makeMassDataIntegratorConnections(MassDataIntegrator *mass_data_integrator_p);

	void
  makeMassDataIntegratorConnections(QualifiedMassSpectrumVectorMassDataIntegrator *mass_data_integrator_p);

  void makeMassSpecDataFileLoaderConnections(
    MassSpecDataFileLoader *mass_spec_data_file_loader_p);

  void setTimeLag(double time_lag);
  double getTimeLag() const;

  bool shouldUpdate();

  public slots:

  void cancelTask();
  void taskFinished();

  void setupProgressBar(int min_value, int max_value);
  void setProgressBarMinValue(int value);
  void setProgressBarMaxValue(int value);
  void incrementProgressBarMaxValue(int increment);
  void setProgressBarCurrentValue(int value);
  void incrementProgressBarCurrentValue(int increment);

  void setMsRunIdText(QString text);
  QString getMsRunIdText();

  void setTaskDescriptionText(QString text);
  void clearTaskDescriptionText();
  QString getTaskDescriptionText();

  void setStatusText(QString text);
  void appendStatusText(QString text);
  QString getStatusText();
  void clearStatusText();

  void setStatusTextAndCurrentValue(QString text, int value);
  void incrementProgressBarCurrentValueAndSetStatusText(int increment,
                                                        QString text);

  void lock();
  void unlock();
  bool isLocked() const;

  signals:

  void cancelTaskSignal();

  protected:
  Ui::TaskMonitorCompositeWidget m_ui;

  void setupWindow();

  private:
  // Start with an unlocked widget.
  bool m_isLocked = false;

  double m_timeLag = 200; /* milliseconds */
  std::chrono::system_clock::time_point m_lastTimePoint;
  double m_currentValue = 0;
  QString craftStringIfFormattedText(const QString &text) const;
};


} // namespace minexpert

} // namespace msxps
