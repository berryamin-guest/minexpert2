/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// StdLib includes
#include <vector>


/////////////////////// Qt includes
#include <QColor>


/////////////////////// pappsomspp includes


/////////////////////// Local includes


namespace msxps
{
namespace minexpert
{

struct colorCompare
{
  bool
  operator()(const QColor &a, const QColor &b) const
  {
    // The convention is to return a < b;
    // Here I decide that the comparison work is like this:
    //
    // get the HSV representation of the color, and sequentially test H, S and V
    // for inequality between colors a and b.

    int h_a;
    int s_a;
    int v_a;

    a.getHsv(&h_a, &s_a, &v_a);

    int h_b;
    int s_b;
    int v_b;

    b.getHsv(&h_b, &s_b, &v_b);

    if(h_a < h_b)
      return true;
    if(s_a < s_b)
      return true;
    if(v_a < v_b)
      return true;

    return false;
  }
};


class ColorSelector
{

  public:
  ColorSelector();
  virtual ~ColorSelector();

  void addColor(const QColor &color);
  static QColor getColor(bool make_random = true);
	static QColor chooseColor();
  static QColor getRandomColor();
  static QColor getColor(const QColor &color);
  static void releaseColor(const QColor &color);
  static bool isUsedColor(const QColor &color);

  private:
  using color_usage_pair = std::pair<QColor, bool>;

  static std::vector<color_usage_pair> m_usageOfColors;
};


} // namespace minexpert

} // namespace msxps
