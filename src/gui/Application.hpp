/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QApplication>
#include <QSplashScreen>


/////////////////////// pappsomspp includes


/////////////////////// Local includes


namespace msxps
{

namespace minexpert
{

class Application : public QApplication
{
  Q_OBJECT

  private:
  QString m_moduleName = "minexpert2";

  // The splash screen, that we'll remove automatically after
  // 2 seconds.
  QSplashScreen *mpa_splash;

  public:
  Application(int &argc, char **argv, const QString &moduleName);
  ~Application();

  Q_PROPERTY(
    QString m_description READ description WRITE setDescription USER true);
  QString m_description =
    QString("mineXpert2: a program to view and analyze mass spectrometric data");

  public slots:
  void setDescription(QString &desc);
  QString description();

  void destroySplash();

  void logToConsole(QString msg);
};

} // namespace minexpert

} // namespace msxps
