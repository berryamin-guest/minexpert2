/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

#pragma once


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QMainWindow>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "ui_BasePlotWnd.h"
#include "BasePlotCompositeWidget.hpp"
#include "../nongui/MassDataIntegrator.hpp"
#include "SaveToGraphicsFileDlg.hpp"
#include "../nongui/AnalysisPreferences.hpp"
#include "../nongui/QualifiedMassSpectrumVectorMassDataIntegratorToTicInt.hpp"


namespace msxps
{
namespace minexpert
{


class MsRunDataSet;
class ProgramWindow;
class DataPlottableNode;

class BasePlotWnd : public QMainWindow
{
  Q_OBJECT

  friend class BasePlotCompositeWidget;
  friend class TicXicChromTracePlotCompositeWidget;
  friend class MassSpecTracePlotCompositeWidget;
  friend class DriftSpecTracePlotCompositeWidget;

  public:
  // Construction/destruction
  BasePlotWnd(QWidget *parent,
              const QString &title,
              const QString &settingsTitle,
              const QString &description = QString());
  virtual ~BasePlotWnd();

  virtual void writeSettings();
  virtual void readSettings();

  virtual bool initialize();
  virtual void closeEvent(QCloseEvent *event);

  ProgramWindow *getProgramWindow();

  void plotWidgetGotFocus(BasePlotCompositeWidget *composite_widget_p);

  virtual void plotCompositeWidgetDestructionRequested(
    BasePlotCompositeWidget *composite_widget_p, bool recursively = false);

  virtual void show();

  void integrateToTicIntensity(
    QCPAbstractPlottable *parent_plottable_p,
    std::shared_ptr<std::vector<pappso::QualifiedMassSpectrumCstSPtr>>
      qualified_mass_spectra_vector_sp,
    const ProcessingFlow &processing_flow);

  std::vector<BasePlotCompositeWidget *> allWidgets();
  std::vector<BasePlotCompositeWidget *> pinnedDownWidgets();

  BasePlotCompositeWidget *
  plotCompositeWidgetForPlottable(QCPAbstractPlottable *plottable_p);

  virtual void plotRangesChanged(const pappso::BasePlotContext &context);

  void recordAnalysisStanza(QString stanza, const QColor &color = Qt::black);

  void showWindow();


  public slots:

  void finishedIntegratingToTicIntensity(
    QualifiedMassSpectrumVectorMassDataIntegrator *mass_data_integrator_p,
    QCPAbstractPlottable *parent_plottable_p);

  void savePinnedPlotsToGraphicsFile();

  void setMzIntegrationParamsForAllPlotWidgets();
  void mzIntegrationParamsChanged(MzIntegrationParams mz_integration_params);

  void setMsFragmentationSpecForAllPlotWidgets();
  void msFragmentationSpecChanged(MsFragmentationSpec ms_fragmentation_spec);

  signals:

  void integrateToTicIntensitySignal(
    QualifiedMassSpectrumVectorMassDataIntegratorToTicInt *mass_data_integrator_p);

  protected:
  // This function selects in the MS run data set all the spectra that match
  // RT/DT criteria.
  std::size_t fillInQualifiedMassSpectraVector(
    MsRunDataSetCstSPtr ms_run_data_set_csp,
    std::shared_ptr<std::vector<pappso::QualifiedMassSpectrumCstSPtr>>
      &qualified_mass_spectra_sp,
    const ProcessingFlow &processing_flow);

  ////// Setting up the window layout.

  //! Graphical interface definition.
  Ui::BasePlotWnd m_ui;

  ProgramWindow *mp_programWindow = nullptr;

  QFile *getAnalysisFilePtr();
  AnalysisPreferences *getAnalysisPreferences();

  /****************** BEGIN ******************/
  /****************** The button tool bar ******************/

  QToolBar *mp_toolBar = nullptr;

  QMenu *mp_mainMenu                 = nullptr;
  QPushButton *mp_mainMenuPushButton = nullptr;

  QAction *mp_resetPlotSizesAct                     = nullptr;
  QAction *mp_removeAllPlotWidgetsAct               = nullptr;
  QAction *mp_removeAllPlotWidgetsAndDescendantsAct = nullptr;

  QAction *mp_lockXRangeAct = nullptr;
  bool m_isLockedXRange     = false;
  QAction *mp_lockYRangeAct = nullptr;
  bool m_isLockedYRange     = false;

  void lockXRangeToggle(bool checked);
  void lockYRangeToggle(bool checked);

  void setupToolBar();
  void createMainMenu();

  /****************** The button tool bar ******************/
  /****************** END ******************/

  QWidget *mp_centralWidget               = nullptr;
  QGridLayout *mp_centralWidgetGridLayout = nullptr;
  QScrollArea *mp_scrollArea              = nullptr;

  QWidget *mp_scrollAreaWidgetContents               = nullptr;
  QGridLayout *mp_scrollAreaWidgetContentsGridLayout = nullptr;
  QVBoxLayout *mp_scrollAreaVBoxLayout               = nullptr;

  QSplitter *mp_splitter;

  QString m_title;
  QString m_settingsTitle;
  QString m_description;
};


} // namespace minexpert

} // namespace msxps
