/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// Qt includes
#include <QTableView>


/////////////////////// Local includes
#include "../nongui/IsoSpecEntity.hpp"


namespace msxps
{
namespace minexpert
{


class IsoSpecDlg;


class IsoSpecTableView : public QTableView
{
  Q_OBJECT

  private:
  IsoSpecDlg *mp_parentDlg;

  QList<IsoSpecEntity *> *m_isoSpecEntityList = nullptr;

  // For drag operations.
  QPoint m_dragStartPos;

  // We do not want to hide the parent class function here.
  using QAbstractItemView::startDrag;
  void startDrag();

  public:
  IsoSpecTableView(QWidget *parent = 0);
  ~IsoSpecTableView();

  void
  setIsoSpecEntityList(QList<IsoSpecEntity *> *isoSpecEntityList);
  QList<IsoSpecEntity *> *getIsoSpecEntityList();

  IsoSpecDlg *parentDlg();
  void setParentDlg(IsoSpecDlg *dlg);
};

} // namespace minexpert

} // namespace msxps

