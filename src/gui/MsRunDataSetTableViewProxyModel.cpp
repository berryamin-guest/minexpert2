/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes
#include <math.h>


/////////////////////// Qt includes
#include <QDebug>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "MsRunDataSetTableViewModel.hpp"
#include "MsRunDataSetTableViewProxyModel.hpp"
#include "MsRunDataSetTableViewItem.hpp"


namespace msxps
{
namespace minexpert
{

MsRunDataSetTableViewProxyModel::MsRunDataSetTableViewProxyModel(
  QObject *parent)
  : QSortFilterProxyModel(parent)
{
  // Initialize all the filtering member variables to values that show that no
  // filtering is being asked for.
  m_indicesPair = std::pair<std::size_t, std::size_t>(
    std::numeric_limits<std::size_t>::max(),
    std::numeric_limits<std::size_t>::min());

  m_msLevel = 0;

  m_rtValuesPair = std::pair<double, double>(
    std::numeric_limits<double>::max(), std::numeric_limits<double>::min());

  m_dtValuesPair = std::pair<double, double>(
    std::numeric_limits<double>::max(), std::numeric_limits<double>::min());

  m_precursorIndexPair = std::pair<std::size_t, std::size_t>(
    std::numeric_limits<std::size_t>::max(),
    std::numeric_limits<std::size_t>::min());

  m_precursorMzValuesPair = std::pair<double, double>(
    std::numeric_limits<double>::max(), std::numeric_limits<double>::min());

  m_precursorChargePair = std::pair<std::size_t, std::size_t>(
    std::numeric_limits<std::size_t>::max(),
    std::numeric_limits<std::size_t>::min());
}


MsRunDataSetTableViewProxyModel::~MsRunDataSetTableViewProxyModel()
{
}


std::pair<size_t, size_t>
MsRunDataSetTableViewProxyModel::parseSizeTString(const QString &text,
                                                  size_t accepted_lower_value)
{
  // The accepted syntax is like this:
  //
  // xxx
  // xxx - yyy
  // < yyy
  // > xxx
  // < yyy > xxx

  // We can test various QRegularExpression_s and check which one works.

  // qDebug() << "Parsing size_t string:" << text;

  QRegularExpression regexp;
  QRegularExpressionMatch match;
  std::pair<std::size_t, std::size_t> range_pair;
  bool ok = false;


  // Single value pattern (xxx)
  regexp.setPattern("^\\s*(\\d+)\\s*$");

  match = regexp.match(text);

  if(match.hasMatch())
    {
      // qDebug();

      QString value_string = match.captured(1);

      ok                = false;
      std::size_t value = value_string.toULongLong(&ok);

      if(!ok || value < accepted_lower_value)
        return std::pair<std::size_t, std::size_t>(
          std::numeric_limits<std::size_t>::max(),
          std::numeric_limits<std::size_t>::min());

      // qDebug() << "pair:" << std::pair<std::size_t, std::size_t>(value,
      // value);

      return std::pair<std::size_t, std::size_t>(value, value);
    }


  // Range of values pattern (xxx - yyy)
  regexp.setPattern("^\\s*(\\d+)\\s*-\\s*(\\d+)\\s*$");

  match = regexp.match(text);

  if(match.hasMatch())
    {
      // qDebug();

      QString lower_value_string = match.captured(1);
      QString upper_value_string = match.captured(2);

      ok               = false;
      range_pair.first = lower_value_string.toULongLong(&ok);

      if(!ok || range_pair.first < accepted_lower_value)
        return std::pair<std::size_t, std::size_t>(
          std::numeric_limits<std::size_t>::max(),
          std::numeric_limits<std::size_t>::max());

      range_pair.second = upper_value_string.toULongLong(&ok);

      if(!ok || range_pair.second < accepted_lower_value)
        return std::pair<std::size_t, std::size_t>(
          std::numeric_limits<std::size_t>::max(),
          std::numeric_limits<std::size_t>::min());

      // At this point we have the pair.

      // qDebug() << "pair:" << range_pair;

      return range_pair;
    }


  // < yyy pattern.
  regexp.setPattern("^\\s*<\\s*(\\d+)\\s*$");

  match = regexp.match(text);

  if(match.hasMatch())
    {
      // qDebug();

      // By essence the lower value is the lowest accepted value.
      range_pair.first = accepted_lower_value;

      // Get the string value.
      QString upper_value_string = match.captured(1);

      ok                      = false;
      std::size_t upper_value = upper_value_string.toULongLong(&ok);

      if(!ok || !upper_value)
        return std::pair<std::size_t, std::size_t>(
          std::numeric_limits<std::size_t>::max(),
          std::numeric_limits<std::size_t>::min());

      // But the comparison is < not <=, thus we ned to reduce the value by one
      // unit. We already tested that it --upper_value could not yield a
      // negative (and thus erroneous unsigned) value.

      --upper_value;

      // Now test if we still have an acceptable upper value, that cannot be
      // less than the accepted lower value.
      if(upper_value < accepted_lower_value)
        return std::pair<std::size_t, std::size_t>(
          std::numeric_limits<std::size_t>::max(),
          std::numeric_limits<std::size_t>::min());

      range_pair.second = upper_value;

      // At this point we have the pair.

      // qDebug() << "pair:" << range_pair;

      return range_pair;
    }


  // > xxx pattern.
  regexp.setPattern("^\\s*>\\s*(\\d+)\\s*$");

  match = regexp.match(text);

  if(match.hasMatch())
    {
      // qDebug();

      // By essence, the upper value is max:
      range_pair.second = std::numeric_limits<std::size_t>::max();

      // Get the string value.
      QString lower_value_string = match.captured(1);

      ok                      = false;
      std::size_t lower_value = lower_value_string.toULongLong(&ok);

      if(!ok)
        return std::pair<std::size_t, std::size_t>(
          std::numeric_limits<std::size_t>::max(),
          std::numeric_limits<std::size_t>::min());

      // But the comparison is > not >=, thus we ned to

      ++lower_value;

      // Now check that this value is an acceptable lower value.
      if(lower_value < accepted_lower_value)
        return std::pair<std::size_t, std::size_t>(
          std::numeric_limits<std::size_t>::max(),
          std::numeric_limits<std::size_t>::min());

      range_pair.first = lower_value;

      // At this point we have the pair.

      // qDebug() << "pair:" << range_pair;

      return range_pair;
    }

  // < yyy > xxx pattern or > xxx < yyy
  // < yyy > xxx, that is xxx < VAR < yyy
  regexp.setPattern("^\\s*<\\s*(\\d+)\\s*>\\s*(\\d+)\\s*$");

  match = regexp.match(text);

  if(match.hasMatch())
    {
      // qDebug();

      // Get the string values
      QString upper_value_string = match.captured(1);
      QString lower_value_string = match.captured(2);

      ok                      = false;
      std::size_t lower_value = lower_value_string.toULongLong(&ok);

      if(!ok)
        return std::pair<std::size_t, std::size_t>(
          std::numeric_limits<std::size_t>::max(),
          std::numeric_limits<std::size_t>::min());

      // But the comparison is > not >=, thus we ned to

      ++lower_value;

      // Now  check that this value is an acceptable lwoer value.
      if(lower_value < accepted_lower_value)
        return std::pair<std::size_t, std::size_t>(
          std::numeric_limits<std::size_t>::max(),
          std::numeric_limits<std::size_t>::min());

      range_pair.first = lower_value;

      // qDebug() << "range_pair.first:" << range_pair.first;

      std::size_t upper_value = upper_value_string.toULongLong(&ok);

      if(!ok || !upper_value)
        return std::pair<std::size_t, std::size_t>(
          std::numeric_limits<std::size_t>::max(),
          std::numeric_limits<std::size_t>::min());

      // But the comparison is < not <=, thus we ned to decrement second, which
      // is why we tested above that --upper_value would not be negative (and
      // thus an erroneous unsigned value).

      --upper_value;

      // Now test if we still have an acceptable upper value, that cannot be
      // less than the accepted lower value.
      if(upper_value < accepted_lower_value)
        return std::pair<std::size_t, std::size_t>(
          std::numeric_limits<std::size_t>::max(),
          std::numeric_limits<std::size_t>::min());

      range_pair.second = upper_value;

      // At this point we have the pair.

      // qDebug() << "pair:" << range_pair;

      return range_pair;
    }


  // < yyy > xxx pattern or > xxx < yyy
  // > xxx < yyy, that is xxx < VAR < yyy
  regexp.setPattern("^\\s*>\\s*(\\d+)\\s*<\\s*(\\d+)\\s*$");

  match = regexp.match(text);

  if(match.hasMatch())
    {
      // qDebug();

      // Get the string values
      QString lower_value_string = match.captured(1);
      QString upper_value_string = match.captured(2);

      ok = false;

      std::size_t lower_value = lower_value_string.toULongLong(&ok);

      if(!ok)
        return std::pair<std::size_t, std::size_t>(
          std::numeric_limits<std::size_t>::max(),
          std::numeric_limits<std::size_t>::min());

      // But the comparison is > not >=, thus we ned to

      ++lower_value;

      if(lower_value < accepted_lower_value)
        return std::pair<std::size_t, std::size_t>(
          std::numeric_limits<std::size_t>::max(),
          std::numeric_limits<std::size_t>::min());

      range_pair.first = lower_value;

      std::size_t upper_value = upper_value_string.toULongLong(&ok);

      if(!ok || !upper_value)
        return std::pair<std::size_t, std::size_t>(
          std::numeric_limits<std::size_t>::max(),
          std::numeric_limits<std::size_t>::min());

      // But the comparison is < not <=, thus we ned to

      --upper_value;

      if(upper_value < accepted_lower_value)
        return std::pair<std::size_t, std::size_t>(
          std::numeric_limits<std::size_t>::max(),
          std::numeric_limits<std::size_t>::min());

      range_pair.second = upper_value;

      // At this point we have the pair.

      // qDebug() << "pair:" << range_pair;

      return range_pair;
    }

  return std::pair<std::size_t, std::size_t>(
    std::numeric_limits<std::size_t>::max(),
    std::numeric_limits<std::size_t>::min());
}


std::pair<double, double>
MsRunDataSetTableViewProxyModel::parseDoubleString(const QString &text,
                                                   double accepted_lower_value)
{
  // The accepted syntax is like this:
  //
  // xxx
  // xxx - yyy
  // < yyy
  // > xxx
  // < yyy > xxx

  // We can test various QRegularExpression_s and check which one works.

  QString sci_number = "([-]?\\s*\\d*\\.?\\d*[e-]?\\d*)";

  QRegularExpression regexp;
  QRegularExpressionMatch match;
  std::pair<double, double> range_pair;
  bool ok = false;


  // Single value pattern (xxx, being positive or negative double real with
  // decimals and exponential (positive or negative)
  regexp.setPattern("^\\s*" + sci_number + "\\s*$");

  match = regexp.match(text);

  if(match.hasMatch())
    {
      QString value_string = match.captured(1);

      ok           = false;
      double value = value_string.toDouble(&ok);

      if(!ok || value < accepted_lower_value)
        return std::pair<double, double>(std::numeric_limits<double>::max(),
                                         std::numeric_limits<double>::min());

      // qDebug() << "pair:" << std::pair<double, double>(value, value);

      return std::pair<double, double>(value, value);
    }


  // Range of values pattern (xxx - yyy)
  regexp.setPattern("^\\s*" + sci_number + "\\s*-\\s*" + sci_number + "\\s*$");


  match = regexp.match(text);

  if(match.hasMatch())
    {
      QString lower_value_string = match.captured(1);
      QString upper_value_string = match.captured(2);

      ok               = false;
      range_pair.first = lower_value_string.toDouble(&ok);

      if(!ok || range_pair.first < accepted_lower_value)
        return std::pair<double, double>(std::numeric_limits<double>::max(),
                                         std::numeric_limits<double>::max());

      range_pair.second = upper_value_string.toDouble(&ok);

      if(!ok || range_pair.second < accepted_lower_value)
        return std::pair<double, double>(std::numeric_limits<double>::max(),
                                         std::numeric_limits<double>::min());

      // At this point we have the pair.

      // qDebug() << "pair:" << range_pair;

      return range_pair;
    }


  // < yyy pattern.
  regexp.setPattern("^\\s*<\\s*" + sci_number + "\\s*$");

  match = regexp.match(text);

  if(match.hasMatch())
    {
      // qDebug();

      // By essence the lower value is the lowest accepted value.
      range_pair.first = accepted_lower_value;

      // Get the string value.
      QString upper_value_string = match.captured(1);

      ok                 = false;
      double upper_value = upper_value_string.toDouble(&ok);

      if(!ok)
        return std::pair<double, double>(std::numeric_limits<double>::max(),
                                         std::numeric_limits<double>::min());

      // But the comparison is < not <=, thus we ned to reduce the value by one
      // epsilon.

      upper_value -= std::numeric_limits<double>::epsilon();

      // Now test if we still have an acceptable upper value, that cannot be
      // less than the accepted lower value.
      if(upper_value < accepted_lower_value)
        return std::pair<double, double>(std::numeric_limits<double>::max(),
                                         std::numeric_limits<double>::min());

      range_pair.second = upper_value;

      // At this point we have the pair.

      // qDebug() << "pair:" << range_pair;

      return range_pair;
    }


  // > xxx pattern.
  regexp.setPattern("^\\s*>\\s*" + sci_number + "\\s*$");

  match = regexp.match(text);

  if(match.hasMatch())
    {
      // qDebug() << "text that matched:" << text;

      // By essence the upper value is max.
      range_pair.second = std::numeric_limits<std::size_t>::max();

      // Get the string value.
      QString lower_value_string = match.captured(1);

      // qDebug() << "captured:" << lower_value_string;

      ok                 = false;
      double lower_value = lower_value_string.toDouble(&ok);

      if(!ok)
        return std::pair<double, double>(std::numeric_limits<double>::max(),
                                         std::numeric_limits<double>::min());

      // But the comparison is > not >=, thus we ned to

      lower_value += std::numeric_limits<double>::epsilon();

      // Now check that this value is an acceptable lower value.
      if(lower_value < accepted_lower_value)
        return std::pair<double, double>(std::numeric_limits<double>::max(),
                                         std::numeric_limits<double>::min());

      range_pair.first = lower_value;

      // At this point we have the pair.

      // qDebug() << "pair:" << range_pair;

      return range_pair;
    }


  // < yyy > xxx pattern or > xxx < yyy
  // > xxx < yyy, that is xxx < VAR < yyy
  regexp.setPattern("^\\s*>\\s*" + sci_number + "\\s*<\\s*" + sci_number +
                    "\\s*$");

  match = regexp.match(text);

  if(match.hasMatch())
    {
      // qDebug();

      // Get the string values
      QString lower_value_string = match.captured(1);
      QString upper_value_string = match.captured(2);

      ok = false;

      double lower_value = lower_value_string.toDouble(&ok);

      if(!ok)
        return std::pair<double, double>(std::numeric_limits<double>::max(),
                                         std::numeric_limits<double>::min());

      // But the comparison is > not >=, thus we ned to

      lower_value += std::numeric_limits<double>::epsilon();

      if(lower_value < accepted_lower_value)
        return std::pair<double, double>(std::numeric_limits<double>::max(),
                                         std::numeric_limits<double>::min());

      range_pair.first = lower_value;

      double upper_value = upper_value_string.toDouble(&ok);

      if(!ok)
        return std::pair<double, double>(std::numeric_limits<double>::max(),
                                         std::numeric_limits<double>::min());

      // But the comparison is < not <=, thus we ned to

      upper_value -= std::numeric_limits<double>::epsilon();

      if(upper_value < accepted_lower_value)
        return std::pair<double, double>(std::numeric_limits<double>::max(),
                                         std::numeric_limits<double>::min());

      range_pair.second = upper_value;

      // At this point we have the pair.

      // qDebug() << "pair:" << range_pair;

      return range_pair;
    }

  return std::pair<double, double>(std::numeric_limits<double>::max(),
                                   std::numeric_limits<double>::min());
}


bool
MsRunDataSetTableViewProxyModel::convertFilteringStrings(
  std::size_t ms_level,
  const QString &indices,
  const QString &rt_values,
  const QString &dt_values,
  const QString &precursor_index_values,
  const QString &precursor_mz_values,
  const QString &precursor_charge_values)
{
  m_msLevel = ms_level;

  // We need to parse each string and make sense of what it contains. If it is
  // empty, then we set values that we'll test later to know if the pair is to
  // be used for the filtering or not.

  using SizeTPair  = std::pair<std::size_t, std::size_t>;
  using DoublePair = std::pair<double, double>;

  // The indices (they start at value 0, which is acceptable).
  if(!indices.isEmpty())
    {
      // qDebug();

      m_indicesPair = parseSizeTString(indices, 0);
      if(m_indicesPair.first == std::numeric_limits<std::size_t>::max() ||
         m_indicesPair.second == std::numeric_limits<std::size_t>::min())
        return false;
    }
  else
    m_indicesPair = SizeTPair(std::numeric_limits<std::size_t>::max(),
                              std::numeric_limits<std::size_t>::min());

  // Now the retention time values.
  if(!rt_values.isEmpty())
    {
      m_rtValuesPair = parseDoubleString(rt_values, 0);
      if(m_rtValuesPair.first == std::numeric_limits<double>::max() ||
         m_rtValuesPair.second == std::numeric_limits<double>::min())
        return false;
    }
  else
    m_rtValuesPair = DoublePair(std::numeric_limits<double>::max(),
                                std::numeric_limits<double>::min());

  // Now the drift time values.
  if(!dt_values.isEmpty())
    {
      m_dtValuesPair = parseDoubleString(dt_values, 0);
      if(m_dtValuesPair.first == std::numeric_limits<double>::max() ||
         m_dtValuesPair.second == std::numeric_limits<double>::min())
        return false;
    }
  else
    m_dtValuesPair = DoublePair(std::numeric_limits<double>::max(),
                                std::numeric_limits<double>::min());

  // Now the precursor index values
  if(!precursor_index_values.isEmpty())
    {
      m_precursorIndexPair = parseSizeTString(precursor_index_values, 0);
      if(m_precursorIndexPair.first ==
           std::numeric_limits<std::size_t>::max() ||
         m_precursorIndexPair.second == std::numeric_limits<std::size_t>::min())
        return false;
    }
  else
    m_precursorIndexPair = SizeTPair(std::numeric_limits<std::size_t>::max(),
                                     std::numeric_limits<std::size_t>::min());

  // Now the precursor m/z values
  if(!precursor_mz_values.isEmpty())
    {
      m_precursorMzValuesPair = parseDoubleString(precursor_mz_values, 0);
      if(m_precursorMzValuesPair.first == std::numeric_limits<double>::max() ||
         m_precursorMzValuesPair.second == std::numeric_limits<double>::min())
        return false;
    }
  else
    m_precursorMzValuesPair = DoublePair(std::numeric_limits<double>::max(),
                                         std::numeric_limits<double>::min());

  // Now the precursor z values
  if(!precursor_charge_values.isEmpty())
    {
      m_precursorChargePair = parseSizeTString(precursor_charge_values, 0);
      if(m_precursorChargePair.first ==
           std::numeric_limits<std::size_t>::max() ||
         m_precursorChargePair.second ==
           std::numeric_limits<std::size_t>::min())
        return false;
    }
  else
    m_precursorChargePair = SizeTPair(std::numeric_limits<std::size_t>::max(),
                                      std::numeric_limits<std::size_t>::min());

  return true;
}


bool
MsRunDataSetTableViewProxyModel::fillInProcessingFlow(
  ProcessingFlow &processing_flow)
{
  bool was_correctly_filled_in = false;

  MsFragmentationSpec ms_fragmentation_spec;

  if(m_msLevel)
    ms_fragmentation_spec.setMsLevel(m_msLevel);

  if(ms_fragmentation_spec.isValid())
    {
      processing_flow.setDefaultMsFragmentationSpec(ms_fragmentation_spec);
      was_correctly_filled_in = true;
    }

  ProcessingStep *new_processing_step_p = new ProcessingStep;

  if(isValidFilterPair(m_rtValuesPair))
    {
      ProcessingSpec *new_processing_spec_p =
        new ProcessingSpec(m_rtValuesPair.first, m_rtValuesPair.second);

      new_processing_step_p->newSpec("RT_TO_ANY", new_processing_spec_p);
      was_correctly_filled_in = true;
    }

  if(isValidFilterPair(m_dtValuesPair))
    {
      ProcessingSpec *new_processing_spec_p =
        new ProcessingSpec(m_dtValuesPair.first, m_dtValuesPair.second);

      new_processing_step_p->newSpec("DT_TO_ANY", new_processing_spec_p);
      was_correctly_filled_in = true;
    }

  if(was_correctly_filled_in)
    processing_flow.push_back(new_processing_step_p);
  else
    delete new_processing_step_p;

  return was_correctly_filled_in;
}


bool
MsRunDataSetTableViewProxyModel::isValidFilterPair(
  const std::pair<std::size_t, std::size_t> &pair) const
{
  return (pair.first != std::numeric_limits<std::size_t>::max());
}


bool
MsRunDataSetTableViewProxyModel::isValidFilterPair(
  const std::pair<double, double> &pair) const
{
  return (pair.first != std::numeric_limits<double>::max());
}


bool
MsRunDataSetTableViewProxyModel::filterAcceptsRow(
  int source_row, const QModelIndex &source_parent) const
{
  // We check all the various filters and each time one filter is valid, then
  // we can use it to filter the data.

  bool should_accept_row = true;
  bool ok                = false;

  if(isValidFilterPair(m_indicesPair))
    {

      // The column of the spectrum indices

      QModelIndex index = sourceModel()->index(
        source_row,
        static_cast<int>(MassSpecDataViewColumns::COLUMN_SPECTRUM_INDEX),
        source_parent);

      QVariant data = sourceModel()->data(index);

      std::size_t cell_value = data.toULongLong(&ok);

      if(!ok)
        qFatal("Failed to convert variant to std::size_t.");

      if(cell_value < m_indicesPair.first || cell_value > m_indicesPair.second)
        should_accept_row = false;
    }

  if(m_msLevel)
    {

      // The column of the ms levels

      // The problem here is that the model does not return any string
      // (DisplayRole) for the ms level column, because we use an icon for
      // this bit of info. Then only place where we know the actual ms level
      // is in the tree widget item.

      QModelIndex index = sourceModel()->index(
        source_row,
        static_cast<int>(MassSpecDataViewColumns::COLUMN_MS_LEVEL),
        source_parent);

      // This is the call that returns the empty QString("").
      // QVariant data = sourceModel()->data(index)

      // So we need to get the actual widget item.
      MsRunDataSetTableViewItem *item =
        static_cast<MsRunDataSetTableViewItem *>(index.internalPointer());

      QVariant data = item->data(index.column());

      std::size_t cell_value = data.toULongLong(&ok);

      if(!ok)
        qFatal("Failed to convert variant to std::size_t.");

      if(cell_value != m_msLevel)
        should_accept_row = false;
    }

  if(isValidFilterPair(m_rtValuesPair))
    {

      // The column of the ms levels

      QModelIndex index = sourceModel()->index(
        source_row,
        static_cast<int>(MassSpecDataViewColumns::COLUMN_RT),
        source_parent);

      QVariant data = sourceModel()->data(index);

      double cell_value = data.toDouble(&ok);

      if(!ok)
        qFatal("Failed to convert variant to std::size_t.");

      if(cell_value < m_rtValuesPair.first ||
         cell_value > m_rtValuesPair.second)
        should_accept_row = false;
    }

  if(isValidFilterPair(m_dtValuesPair))
    {

      // The column of the ms levels

      QModelIndex index = sourceModel()->index(
        source_row,
        static_cast<int>(MassSpecDataViewColumns::COLUMN_DT),
        source_parent);

      QVariant data = sourceModel()->data(index);

      double cell_value = data.toDouble(&ok);

      if(!ok)
        qFatal("Failed to convert variant to std::size_t.");

      if(cell_value < m_dtValuesPair.first ||
         cell_value > m_dtValuesPair.second)
        should_accept_row = false;
    }

  if(isValidFilterPair(m_precursorIndexPair))
    {
      // qDebug();

      // This cell might contain *nothing* because it is only filled if the
      // row is about a fragmentation spectrum, that is, not a MS1 spectrum.

      QModelIndex index = sourceModel()->index(
        source_row,
        static_cast<int>(MassSpecDataViewColumns::COLUMN_PRECURSOR_INDEX),
        source_parent);

      // We know the data are stored in the tree view cells as QString
      // variables.
      QString data = sourceModel()->data(index).toString();

      // qDebug() << "initial data:" << data;

      if(!data.isEmpty())
        {
          std::size_t cell_value = data.toULongLong(&ok);

          if(!ok)
            qFatal("Failed to convert variant to std::size_t.");

          if(cell_value < m_precursorIndexPair.first ||
             cell_value > m_precursorIndexPair.second)
            should_accept_row = false;
        }
      else
        {
          // We need to check if one of the children of this cell has a
          // precursor index matching the values in the pair.

          // if(sourceModel()->hasChildren(index))
          // qDebug() << "has children";

          int row = index.row();
          int column =
            static_cast<int>(MassSpecDataViewColumns::COLUMN_PRECURSOR_INDEX);

          QModelIndex child_index =
            sourceModel()->index(row + 1, column, index);

          if(child_index.isValid())
            {
              QString data = sourceModel()->data(child_index).toString();

              // qDebug() << "child data:" << data;

              if(!data.isEmpty())
                {
                  std::size_t cell_value = data.toULongLong(&ok);

                  if(!ok)
                    qFatal("Failed to convert variant to std::size_t.");

                  if(cell_value < m_precursorIndexPair.first ||
                     cell_value > m_precursorIndexPair.second)
                    should_accept_row = false;
                }
            }
        }
    }

  if(isValidFilterPair(m_precursorMzValuesPair))
    {

      // This cell might contain *nothing* because it is only filled if the
      // row is about a fragmentation spectrum, that is, not a MS1 spectrum.

      QModelIndex index = sourceModel()->index(
        source_row,
        static_cast<int>(MassSpecDataViewColumns::COLUMN_PRECURSOR_MZ),
        source_parent);

      // We know the data are stored in the tree view cells as QString
      // variables.
      QString data = sourceModel()->data(index).toString();

      if(!data.isEmpty())
        {
          double cell_value = data.toDouble(&ok);

          if(!ok)
            qFatal("Failed to convert variant to std::size_t.");

          if(cell_value < m_precursorMzValuesPair.first ||
             cell_value > m_precursorMzValuesPair.second)
            should_accept_row = false;
        }
    }

  if(isValidFilterPair(m_precursorChargePair))
    {
      // This cell might contain *nothing* because it is only filled if the
      // row is about a fragmentation spectrum, that is, not a MS1 spectrum.

      QModelIndex index = sourceModel()->index(
        source_row,
        static_cast<int>(MassSpecDataViewColumns::COLUMN_PRECURSOR_Z),
        source_parent);

      // We know the data are stored in the tree view cells as QString
      // variables.
      QString data = sourceModel()->data(index).toString();

      if(!data.isEmpty())
        {
          std::size_t cell_value = data.toULongLong(&ok);

          if(!ok)
            qFatal("Failed to convert variant to std::size_t.");

          // qDebug() << "cell_value:" << cell_value
          //<< "m_precursorChargePair.first:" << m_precursorChargePair.first
          //<< "m_precursorChargePair.second:" << m_precursorChargePair.second;

          if(cell_value < m_precursorChargePair.first ||
             cell_value > m_precursorChargePair.second)
            should_accept_row = false;
        }
    }

  // qDebug() << "Returning should_accept_row:" << should_accept_row;

  return should_accept_row;
}


bool
MsRunDataSetTableViewProxyModel::lessThan(const QModelIndex &left,
                                          const QModelIndex &right) const
{

  // QModelIndex source_left  = mapFromSource(left);

  // Now that we have the source model indices, we can ask what is the column
  // that we need to sort data for.

  // int column = source_left.column();
  int column = left.column();

  // Get the data anyway, we'll then check what kind of data it is.

  QVariant leftData  = sourceModel()->data(left);
  QVariant rightData = sourceModel()->data(right);

  bool ok = false;

  // if(column == static_cast<int>(MassSpecDataViewColumns::COLUMN_RANK) ||
  if(column == static_cast<int>(MassSpecDataViewColumns::COLUMN_MS_LEVEL) ||
     column ==
       static_cast<int>(MassSpecDataViewColumns::COLUMN_SPECTRUM_INDEX) ||
     column ==
       static_cast<int>(MassSpecDataViewColumns::COLUMN_PRECURSOR_INDEX) ||
     column == static_cast<int>(MassSpecDataViewColumns::COLUMN_PRECURSOR_Z))
    {
      return leftData.toUInt(&ok) < rightData.toUInt(&ok);
    }
  else if(column == static_cast<int>(MassSpecDataViewColumns::COLUMN_RT) ||
          column == static_cast<int>(MassSpecDataViewColumns::COLUMN_DT) ||
          column ==
            static_cast<int>(MassSpecDataViewColumns::COLUMN_PRECURSOR_MZ))

    {
      return leftData.toDouble(&ok) < rightData.toDouble(&ok);
    }
  else
    qFatal("Should never reach this point.");

  return false;
}


} // namespace minexpert
} // namespace msxps
