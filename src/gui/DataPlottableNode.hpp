/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// StdLib includes
#include <memory>
#include <vector>


/////////////////////// Qt includes
#include <QColor>


/////////////////////// QCustomPlot
#include <qcustomplot.h>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "BasePlotCompositeWidget.hpp"


namespace msxps
{
namespace minexpert
{

class DataPlottableNode;

typedef std::shared_ptr<DataPlottableNode> DataPlottableNodeSPtr;
typedef std::shared_ptr<const DataPlottableNode> DataPlottableNodeCstSPtr;

class DataPlottableNode
{
  friend class DataPlottableTree;

  public:
  DataPlottableNode();
  DataPlottableNode(const DataPlottableNode &other);
  DataPlottableNode(MsRunDataSetCstSPtr ms_run_data_set_csp,
                    QCPAbstractPlottable *plottable_p,
                    DataPlottableNode *parent_node_pp,
                    BasePlotCompositeWidget *plot_widget_p);

  virtual ~DataPlottableNode();

  DataPlottableNode &operator=(const DataPlottableNode &other);

  QCPAbstractPlottable *getPlottable() const;
  BasePlotCompositeWidget *getPlotWidget() const;

  void setParent(DataPlottableNode *new_parent_node_p);
  bool reparent(DataPlottableNode *new_parent_node_p);
  DataPlottableNode *getParent() const;
  bool hasParent() const;

  const std::vector<DataPlottableNode *> &getChildren() const;

  DataPlottableNode *findNode(QCPAbstractPlottable *plottable_p);
  void findNodes(MsRunDataSetCstSPtr ms_run_data_set_csp,
                 std::vector<DataPlottableNode *> &found_nodes);
  void findNodes(BasePlotCompositeWidget *base_plot_composite_widget_p,
                 std::vector<DataPlottableNode *> &found_nodes,
                 bool recursively);

  void flattenedView(std::vector<DataPlottableNode *> &nodes,
                     bool with_descendants = false);

  void flattenedViewLevelNodes(std::size_t ms_level,
                               std::size_t depth,
                               std::vector<DataPlottableNode *> &nodes,
                               bool with_descendants = false);

  void setPlotColor(const QColor &color);

  // Utility functions.
  std::size_t depth(std::size_t depth) const;
  void size(std::size_t &cumulative_node_count) const;

  QString toString(int offset = 0, bool with_children = false) const;
  void toDebug(int offset = 0, bool with_children = false) const;

  private:
  // The parent of this node (might be nullptr if this node is a root node of
  // the tree.
  DataPlottableNode *mp_parent = nullptr;

  // The list of children of this node.
  std::vector<DataPlottableNode *> m_children;

  // Each node needs to know for which ms run data set it handles a plot
  // widget/plottable pair.
  MsRunDataSetCstSPtr mcsp_msRunDataSet;

  // The plot widget that contains the plottable.
  BasePlotCompositeWidget *mp_plotWidget = nullptr;

  // The plottable that is handled by this node.
  QCPAbstractPlottable *mp_plottable = nullptr;

  // The plotting color of the plottable.
  QColor m_plotColor;

  std::vector<DataPlottableNode *>::iterator
  findNode(DataPlottableNode *searched_node_p);
};


} // namespace minexpert

} // namespace msxps
