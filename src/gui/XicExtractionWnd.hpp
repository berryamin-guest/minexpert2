/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QMainWindow>
#include <QObject>


/////////////////////// pappsomspp includes
#include <pappsomspp/widget/precisionwidget/precisionwidget.h>


/////////////////////// Local includes
#include "ui_XicExtractionWnd.h"


namespace msxps
{
namespace minexpert
{


class ProgramWindow;

//! The XicExtractionWnd class provides an interface to the XIC extraction
//! settings.
/*!

  The XIC extraction settings allow the user to configure how the XIC
  extraction should be performed, on which value, with which tolerance, and if
  that extraction should be performed on the currently active plot widget or
  on all of the widgets.

*/
class XicExtractionWnd : public QMainWindow
{
  Q_OBJECT

  public:
  XicExtractionWnd(ProgramWindow *program_window_p,
                   const QString &title,
                   const QString &settings_title);
  ~XicExtractionWnd();

  void readSettings();
  void writeSettings();

	void showWindow();

	public slots:

  void runPushButtonClicked();
  void abortPushButtonClicked();
  void closePushButtonClicked();

  signals:
  void cancelOperationSignal();

  protected:
  Ui::XicExtractionWnd m_ui;

  private:
  ProgramWindow *mp_programWindow = nullptr;
  pappso::PrecisionPtr mp_precision =
    pappso::PrecisionFactory::getDaltonInstance(0.05);
  pappso::PrecisionWidget *mp_precisionWidget = nullptr;
  QString m_fileName;
  QString m_title;
  QString m_settingsTitle;

  void initialize();
};


} // namespace minexpert

} // namespace msxps

