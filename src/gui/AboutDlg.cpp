/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QFile>
#include <QDebug>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "config.h"
#include "../nongui/about_gpl_v30.hpp"
#include "AboutDlg.hpp"


namespace msxps
{
namespace minexpert
{


AboutDlg::AboutDlg(QWidget *parent, const QString &applicationName)
  : QDialog(parent), m_applicationName(applicationName)
{
  if(parent == nullptr)
    qFatal("Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);

  m_ui.setupUi(this);

  QString text = QString("msXpertSuite - module %1, version %2.")
                   .arg(m_applicationName)
                   .arg(VERSION);

  m_ui.programVersionLabel->setText(text);

  setWindowTitle(QString("%1 - About").arg(m_applicationName));

  connect(m_ui.closePushButton, SIGNAL(clicked()), this, SLOT(accept()));

  setupGraphicsView();

  m_ui.copyrightLabel->setText(
    "msXpertSuite Copyright 2016-2029 by Filippo Rusconi");
  m_ui.gplTextEdit->setText(gpl);

  setApplicationFeaturesText();

  setHistoryText();
}


AboutDlg::~AboutDlg()
{
}


void
AboutDlg::setApplicationFeaturesText()
{
  m_ui.featuresTextEdit->setHtml(
    "<html><head><meta name=\"qrichtext\" content=\"1\" /><style "
    "type=\"text/css\">\n"
    " p, li { white-space: pre-wrap; }\n"
    " </style></head><body style=\" font-family:'Sans Serif'; "
    "font-size:12pt; font-weight:400; font-style:normal; "
    "text-decoration:none;\">\n"
    "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; "
    "margin-left:0px; margin-right:0px; -qt-block-indent:0; "
    "text-indent:0px;\"></p>\n"
    "<br>\n"
    "<br>\n"
    "<em>mineXpert</em> provides the following main features:\n"
    "<br>\n"
    "<ul>\n"
    "<li>Load specific SQLite3 db files or standard mzML/mzXML files;</li>\n"
    "<li>Display the TIC chromatogram, that is dynamically computed;</li>\n"
    "<li>Open an arbitrary number of mass spectrometry files to compare "
    "them;</li>\n"
    "<li>Perform any kind of data integration, from TIC chromatogram to "
    "drift time spectum to mass spectrum in any order;</li>\n"
    "<li>Display spectra both in overlaid and stacked mode for easy "
    "comparison of the peaks;</li>\n"
    "<li>Configure an \"Analysis mode\" that sends the characteristics of "
    "pointed peaks to a given file;</li>\n"
    "<li>mineXpert is particularly well-suite for ion mobility mass "
    "spectrometry data analysis.</li>\n"
    "<li>JavaScript-based scripting of the graphical user interface "
    "and of the traces (mass/drift spectra, TIC chromatogram).</li>\n"
    "</ul>\n"
    "<br>\n"
    "</body></html>\n");
}


void
AboutDlg::setHistoryText()
{
  qDebug();

  // We load the history file now. It is part of the source code in the form
  // of doc/history.html.

  QFile historyFile(":/doc/history.html");

  if(!historyFile.open(QFile::ReadOnly | QFile::Text))
    {
      qFatal("Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);
    }

  QString history = historyFile.readAll();
  m_ui.historyTextEdit->setHtml(history);
}

void
AboutDlg::setupGraphicsView()
{
  mp_graphicsScene = new QGraphicsScene();

  QPixmap pixmap(":/images/splashscreen.png");

  QGraphicsPixmapItem *pixmapItem = mp_graphicsScene->addPixmap(pixmap);
  if(!pixmapItem)
    return;
  m_ui.graphicsView->setScene(mp_graphicsScene);
}


void
AboutDlg::showHowToCiteTab()
{
  m_ui.tabWidget->setCurrentIndex(1);
}


} // namespace minexpert

} // namespace msxps
