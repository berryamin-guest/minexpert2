/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QSettings>
#include <QMenuBar>
#include <QMenu>
#include <QDebug>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "TaskMonitorWnd.hpp"
#include "TaskMonitorCompositeWidget.hpp"


namespace msxps
{
namespace minexpert
{


//! Construct an TaskMonitorWnd instance.
TaskMonitorWnd::TaskMonitorWnd(QWidget *parent,
                               const QString &title,
                               const QString &settingsTitle,
                               const QString &description)
  : QMainWindow(parent),
    m_title(title),
    m_settingsTitle(settingsTitle),
    m_description(description)
{
  if(parent == Q_NULLPTR)
    qFatal("Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);

  m_ui.setupUi(this);

  if(!initialize())
    qFatal("Fatal error at %s@%d. Program aborted.", __FILE__, __LINE__);
}


//! Destruct \c this TaskMonitorWnd instance.
TaskMonitorWnd::~TaskMonitorWnd()
{
  // qDebug();
  writeSettings();
}


//! Write the settings to as to restore the window geometry later.
void
TaskMonitorWnd::writeSettings()
{
  // qDebug();
  QSettings settings;
  settings.setValue(m_settingsTitle + "_geometry", saveGeometry());
  settings.setValue(m_settingsTitle + "_isVisible", isVisible());
}


//! Read the settings to as to restore the window geometry.
void
TaskMonitorWnd::readSettings()
{
  // qDebug();
  QSettings settings;
  restoreGeometry(settings.value(m_settingsTitle + "_geometry").toByteArray());

  bool wasVisible = settings.value(m_settingsTitle + "_isVisible").toBool();
  setVisible(wasVisible);
}


//! Handle the close event.
void
TaskMonitorWnd::closeEvent(QCloseEvent *event)
{
  // qDebug();
  writeSettings();
  hide();
}


//! Initialize the window.
bool
TaskMonitorWnd::initialize()
{
  setWindowTitle(QString("mineXpert2 - %1").arg(m_title));

  readSettings();

  return true;
}


void
TaskMonitorWnd::show()
{
  QSettings settings;
  restoreGeometry(settings.value(m_settingsTitle + "_geometry").toByteArray());

  QMainWindow::show();
}


TaskMonitorCompositeWidget *
TaskMonitorWnd::addTaskMonitorWidget(const QColor &color)
{

  // Allocate a task monitor composite plot widget.

  TaskMonitorCompositeWidget *widget_p =
    new TaskMonitorCompositeWidget(this, color);
  widget_p->setMinimumSize(400, 150);

  // The new widget should be preprended so that the newly created ones are at
  // the top of the vertical layout box.

  m_ui.scrollAreaVerticalLayout->insertWidget(0, widget_p);

  return widget_p;
}


} // namespace minexpert

} // namespace msxps
