/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

#pragma once


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QDialog>
#include <QStringList>
#include <QMenu>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "ui_OpenMsRunDataSetsDlg.h"
#include "../nongui/MsRunDataSet.hpp"
#include "MsRunDataSetTableViewWnd.hpp"


namespace msxps
{
namespace minexpert
{


class PlotWidgetInterface;
class MsRunDataSet;
class ProgramWindow;


class OpenMsRunDataSetsDlg : public QDialog
{
  Q_OBJECT

  //! Drop-down menu item string list.
  // QStringList m_actionList{"Hide",
  //"Hide all but selected",
  //"Hide all",
  //"Show",
  //"Show all but selected",
  //"Show all",
  //"Toggle visibility",
  //"Change color",
  //"Close",
  //"Close all but selected",
  //"Close all"};

  public:
  // Construction/destruction
  OpenMsRunDataSetsDlg(QWidget *parent = nullptr);
  ~OpenMsRunDataSetsDlg();

  void closeEvent(QCloseEvent *event);
  void writeSettings();
  void readSettings();

  bool initialize();

  void show();

  int itemCount();
  int selectedItemCount();

  void changePlottingColor();

  QColor colorForMsRunDataSet(MsRunDataSetCstSPtr &ms_run_data_set_csp) const;

  void newOpenMsRunDataSet(MsRunDataSetCstSPtr &ms_run_data_set_csp,
                           QColor color);

  void removeMsRunDataSet(MsRunDataSetCstSPtr &ms_run_data_set_csp);

  void closeItems(QList<QListWidgetItem *> widget_items);
  void closeSelected(void);
  void closeAllButSelected(void);

  void showMsRunDataSetTableView();

  std::size_t msRunDataSetCount() const;
  void selectFirstMsRunDataSet();

  bool
  singleSelectedOrUniqueMsRunDataSet(MsRunDataSetCstSPtr &ms_run_data_set_csp,
                                     QColor &color) const;

  bool msRunDataSetFromSampleName(const QString &sample_name,
                                  MsRunDataSetCstSPtr &ms_run_data_set_csp,
                                  QColor &color) const;

	bool colorFromMsRunDataSet(MsRunDataSetCstSPtr ms_run_data_set_csp, QColor &color);

  std::vector<MsRunDataSetCstSPtr> allMsRunDataSets() const;

  std::vector<MsRunDataSetCstSPtr> allSelectedOrUniqueMsRunDataSets() const;

  signals:

  protected:
  ProgramWindow *mp_programWindow = nullptr;

  void showMsRunDataSetTableViewWnd(MsRunDataSetCstSPtr ms_run_data_set_csp);

  void createMenusAndActions();

  QListWidgetItem *
  listWidgetItemForMsRunDataSet(MsRunDataSetCstSPtr &ms_run_data_set_csp);
  MsRunDataSetCstSPtr msRunDataSetForListWidgetItem(int index);
  MsRunDataSetCstSPtr
  msRunDataSetForListWidgetItem(QListWidgetItem *list_widget_item_p) const;

  private:
  //! Graphical interface definition.
  Ui::OpenMsRunDataSetsDlg m_ui;
  QMenu *mp_menu = nullptr;

  std::map<MsRunDataSetCstSPtr, QListWidgetItem *>
    m_msRunDataSetListWidgetItemMap;

  QList<QListWidgetItem *> unselectedItems();
};


} // namespace minexpert

} // namespace msxps
