/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * Inspiration from XTPcpp by Olivier Langella (olivier.langella@u-psud.fr)
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes
#include <iostream>


/////////////////////// Qt includes
#include <QDebug>
#include <QSettings>
#include <QThread>
#include <QMessageBox>


/////////////////////// pappsomspp includes
#include <pappsomspp/msfile/msfileaccessor.h>


/////////////////////// Local includes
#include "MsRunReadTask.hpp"
#include "../nongui/MassSpecDataFileLoader.hpp"


namespace msxps
{
namespace minexpert
{


MsRunReadTask::MsRunReadTask(ProgramWindow *program_window_p)
{
  // qDebug();

  mp_programWindow = program_window_p;
}


MsRunReadTask::~MsRunReadTask()
{
  // qDebug() << "MsRunReadTask::MsRunReadTask destructor";
}


void
MsRunReadTask::readMsRunData(MsRunReadTask *ms_run_read_task_p,
                             pappso::MsRunReaderSPtr ms_run_reader_sp,
                             MassSpecDataFileLoaderSPtr loader_sp)
{
  // We need to ensure that we react to a signal that indeed is directed to the
  // proper MsRunReadTask, because the signal emanates from a generic emitter:
  // ProgramWindow.

  if(this != ms_run_read_task_p)
    return;

  // qDebug() << "reading ms run data from thread:" << QThread::currentThread()
  //<< "and this MsRunReadTask:" << this;

  ms_run_reader_sp->readSpectrumCollection(*(loader_sp.get()));

  // At this point, emit the signal that we did the job.
  emit finishedReadingMsRunDataSignal(loader_sp->getMsRunDataSet());
}


} // namespace minexpert

} // namespace msxps
