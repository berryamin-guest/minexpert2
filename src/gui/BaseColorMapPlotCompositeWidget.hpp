
/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QWidget>

/////////////////////// QCustomPlot


/////////////////////// pappsomspp includes
#include <pappsomspp/widget/plotwidget/basetraceplotwidget.h>
#include <pappsomspp/widget/plotwidget/colormapplotconfig.h>


/////////////////////// Local includes
#include "ui_BasePlotCompositeWidget.h"
#include "BasePlotCompositeWidget.hpp"
#include "../nongui/MsRunDataSet.hpp"
#include "../nongui/ProcessingFlow.hpp"
#include "../nongui/MzIntegrationParams.hpp"


namespace msxps
{
namespace minexpert
{

class BaseColorMapPlotWnd;
class MsFragmentationSpecDlg;
class MzIntegrationParamsDlg;


// The BaseColorMapPlotCompositeWidget class is aimed at receiving QCPGraph_s,
// not QCPColorMap_s. This class implements all the basic features related to
// the presence of QCPGraph_s, like adding a Trace plot, for example, which
// would not work if the plot were of type QCPColorMap...
class BaseColorMapPlotCompositeWidget : public BasePlotCompositeWidget
{
  Q_OBJECT

  friend class BasePlotWnd;
  friend class BaseColorMapPlotWnd;

  public:
  explicit BaseColorMapPlotCompositeWidget(
    QWidget *parent,
    const QString &x_axis_label,
    const QString &y_axis_label);

  virtual ~BaseColorMapPlotCompositeWidget();

  virtual QCPColorMap *
  addColorMap(std::shared_ptr<std::map<double, pappso::MapTrace>>
                &double_map_trace_map_sp,
              const pappso::ColorMapPlotConfig &color_map_plot_config,
              QCPAbstractPlottable *parent_plottable_p,
              MsRunDataSetCstSPtr ms_run_data_set_csp,
              const QString &sample_name,
              const ProcessingFlow &processing_flow,
              const QColor &color);

	pappso::DataKind xAxisKind() const;
	pappso::DataKind yAxisKind() const;

  public slots:

  signals:

  void plotWidgetMouseReleaseEventSignal(QMouseEvent *event);

  protected:
  virtual void createMainMenu() override;
};


} // namespace minexpert

} // namespace msxps
