/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes
#include <iostream>
#include <iomanip>

/////////////////////// OpenMP include
#include <omp.h>

/////////////////////// Qt includes
#include <QDebug>
#include <QThread>

/////////////////////// pappsomspp includes
#include <pappsomspp/processing/combiners/tracepluscombiner.h>


/////////////////////// Local includes
#include "MsRunDataSetTreeMassDataIntegratorToMz.hpp"
#include "ProcessingSpec.hpp"
#include "ProcessingStep.hpp"
#include "ProcessingType.hpp"
#include "BaseMsRunDataSetTreeNodeVisitor.hpp"
#include "TicChromTreeNodeCombinerVisitor.hpp"
#include "IntensityTreeNodeCombinerVisitor.hpp"
#include "RtDtMzColorMapsTreeNodeCombinerVisitor.hpp"
#include "TraceTreeNodeCombinerVisitor.hpp"
#include "MsRunStatisticsTreeNodeVisitor.hpp"
#include "MassSpectrumTreeNodeCombinerVisitor.hpp"
#include "DriftSpectrumTreeNodeCombinerVisitor.hpp"
#include "MultiTreeNodeCombinerVisitor.hpp"


int msRunDataSetTreeMassDataIntegratorToMzMetaTypeId =
  qRegisterMetaType<msxps::minexpert::MsRunDataSetTreeMassDataIntegratorToMz>(
    "msxps::minexpert::MsRunDataSetTreeMassDataIntegratorToMz");

int msRunDataSetTreeMassDataIntegratorToMzSPtrMetaTypeId = qRegisterMetaType<
  msxps::minexpert::MsRunDataSetTreeMassDataIntegratorToMzSPtr>(
  "msxps::minexpert::MsRunDataSetTreeMassDataIntegratorToMzSPtr");


namespace msxps
{
namespace minexpert
{


MsRunDataSetTreeMassDataIntegratorToMz::MsRunDataSetTreeMassDataIntegratorToMz()
{
  qFatal("Cannot be that the default constructor be used.");
  // qDebug() << "Allocating new integrator:" << this;
}


MsRunDataSetTreeMassDataIntegratorToMz::MsRunDataSetTreeMassDataIntegratorToMz(
  MsRunDataSetCstSPtr ms_run_data_set_csp)
  : MassDataIntegrator(ms_run_data_set_csp)
{
  // Essential that the m_processingFlow member is configured to have the right
  // pointer to the ms run data set.

  m_processingFlow.setMsRunDataSetCstSPtr(ms_run_data_set_csp);
}


MsRunDataSetTreeMassDataIntegratorToMz::MsRunDataSetTreeMassDataIntegratorToMz(
  MsRunDataSetCstSPtr ms_run_data_set_csp,
  const ProcessingFlow &processing_flow)
  : MassDataIntegrator(ms_run_data_set_csp, processing_flow)
{
  // Essential that the m_processingFlow member is configured to have the right
  // pointer to the ms run data set.
  if(ms_run_data_set_csp != m_processingFlow.getMsRunDataSetCstSPtr())
    qFatal("The pointers should be identical.");
}


MsRunDataSetTreeMassDataIntegratorToMz::MsRunDataSetTreeMassDataIntegratorToMz(
  const MsRunDataSetTreeMassDataIntegratorToMz &other)
  : MassDataIntegrator(other)
{
  // Essential that the m_processingFlow member is configured to have the right
  // pointer to the ms run data set.
  if(other.mcsp_msRunDataSet != m_processingFlow.getMsRunDataSetCstSPtr())
    qFatal("The pointers should be identical.");
}


MsRunDataSetTreeMassDataIntegratorToMz::
  ~MsRunDataSetTreeMassDataIntegratorToMz()
{
  // qDebug() << "Destroying integrator:" << this;
}


const ProcessingFlow &
MsRunDataSetTreeMassDataIntegratorToMz::getProcessingFlow() const
{
  return m_processingFlow;
}


void
MsRunDataSetTreeMassDataIntegratorToMz::appendProcessingStep(
  ProcessingStep *processing_step_p)
{
  if(processing_step_p == nullptr)
    qFatal("The pointer cannot be nullptr.");

  m_processingFlow.push_back(processing_step_p);
}


void
MsRunDataSetTreeMassDataIntegratorToMz::integrateToMz()
{
  // qDebug();

  std::chrono::system_clock::time_point chrono_start_time =
    std::chrono::system_clock::now();

  // We need to clear the map trace!
  m_mapTrace.clear();

  // We need a processing flow to work, and, in particular, a processing step
  // from which to find the specifics of the calculation. The processing flow
  // must have been set by the caller either at construction time or later
  // before using the integrator, that is, calling this function.

  if(!m_processingFlow.size())
    qFatal("The processing flow cannot be empty. Program aborted.");

  // Get the most recent step that holds all the specifics of this integration.
  const ProcessingStep *processing_step_p = m_processingFlow.mostRecentStep();

  // Now get a list of the integration types that are stored in the step's map.

  std::vector<ProcessingType> processing_types =
    processing_step_p->processingTypes();

  if(!processing_types.size())
    qFatal("The processing step cannot be empty. Program aborted.");

  if(!processing_step_p->matches("ANY_TO_MZ"))
    qFatal("There should be one ProcessingType = ANY_TO_MZ. Program aborted.");

  // Since we are asked that an integration to a mass spectrum be performed,
  // there must be a non-nullptr mz integration pointer in the step.
  const MzIntegrationParams *mz_integration_params_p =
    processing_step_p->getMzIntegrationParamsPtr();

  if(mz_integration_params_p == nullptr)
    qFatal("Cannot be that the pointer is nullptr.");

  // qDebug().noquote() << "Starting integration to mz with params:"
  //<< mz_integration_params_p->toString();

  // Reset the value of the nodes visited to 0.
  emit setProgressBarCurrentValueSignal(0);

  emit setTaskDescriptionTextSignal(
    "Computing the combination of the appropriate mass spectra");

  if(mz_integration_params_p->getBinningType() == BinningType::NONE)
    {
      integrateToMzNoBinning();
    }
  else if(mz_integration_params_p->getBinningType() == BinningType::ARBITRARY)
    {
      integrateToMzArbitraryBinning();
    }

  std::chrono::system_clock::time_point chrono_end_time =
    std::chrono::system_clock::now();

  QString chrono_string = pappso::Utils::chronoIntervalDebugString(
    "Integration to mass spectrum took:", chrono_start_time, chrono_end_time);

  emit logTextToConsoleSignal(chrono_string);
  // qDebug().noquote() << chrono_string;
}


bool
MsRunDataSetTreeMassDataIntegratorToMz::integrateToMzNoBinning()
{
  // qDebug();

  //  This function needs to be called by integrateToMz() that has setup all the
  //  required parameters.

  // There are a lot of verification of the current context that have been
  // performed in the calling function.

  // Try to limit the range of MS run data set tree nodes to be iterated through
  // by looking from what node to what other node we need to go to ensure that
  // our integration encompasses the right RT range.

  std::vector<pappso::MsRunDataSetTreeNode *> root_nodes =
    mcsp_msRunDataSet->getMsRunDataSetTreeCstSPtr()->getRootNodes();

  double start_rt = std::numeric_limits<double>::infinity();
  double end_rt   = std::numeric_limits<double>::infinity();

  bool integration_rt = m_processingFlow.innermostRtRange(start_rt, end_rt);

  using Iterator = std::vector<pappso::MsRunDataSetTreeNode *>::const_iterator;
  using Pair     = std::pair<Iterator, Iterator>;

  Pair pair;

  Iterator begin_iterator = root_nodes.begin();
  Iterator end_iterator   = root_nodes.end();

  std::size_t node_count = 0;

  if(integration_rt)
    {
      pair =
        mcsp_msRunDataSet->treeNodeIteratorRangeForRtRange(start_rt, end_rt);

      begin_iterator = pair.first;
      end_iterator   = pair.second;

      node_count = std::distance(begin_iterator, end_iterator);

      qDebug() << "node_count:" << node_count;
    }
  else
    qDebug() << "Not integration_rt";

  if(begin_iterator == root_nodes.end())
    {
      qDebug() << "There is nothing to integrate.";
      return false;
    }

  // Now that we know the non-0 count of nodes to be processed:
  emit setProgressBarMaxValueSignal(node_count);

  // At this point, allocate a visitor that is specific for the calculation of
  // the mass spectrum. Since we are not binning, we can use a visitor that
  // integrates to a pappso::Trace, not a pappso::MassSpectrum.

  // But we want to parallelize the computation. Se we will allocate a vector of
  // visitors (as many as possible for the available processor threads), each
  // visitor having a subset of the initial data to integrate.

  // In the pair below, first is the ideal number of threads and second is the
  // number of nodes per thread.
  std::pair<std::size_t, std::size_t> best_parallel_params =
    bestParallelIntegrationParams(node_count);

  // qDebug() << "nodes_per_thread:" << best_parallel_params.second;

  using Iterator = std::vector<pappso::MsRunDataSetTreeNode *>::const_iterator;

  std::vector<TraceTreeNodeCombinerVisitorSPtr> visitors;
  std::vector<std::pair<Iterator, Iterator>> iterators =
    calculateMsRunDataSetTreeNodeIteratorPairs(begin_iterator,
                                               end_iterator,
                                               best_parallel_params.first,
                                               best_parallel_params.second);

  // Since we are asked that an integration to a mass spectrum be performed,
  // there must be a non-nullptr mz integration pointer in the most recent step.

  const MzIntegrationParams *mz_integration_params_p =
    m_processingFlow.mostRecentStep()->getMzIntegrationParamsPtr();

  if(mz_integration_params_p == nullptr)
    qFatal("Cannot be that the pointer is nullptr.");

  // We use a Trace combiner because we are not going to do any binning.
  pappso::TracePlusCombinerSPtr combiner_sp =
    std::make_shared<pappso::TracePlusCombiner>(
      mz_integration_params_p->getDecimalPlaces());

  // For each available thread, allocate a new visitor. We also configure which
  // root nodes it should handle as two (begin,end) iterators to the ms run data
  // set tree. We push back the pair of iterators to the vector of iterators.

  for(std::size_t iter = 0; iter < iterators.size(); ++iter)
    {
      // qDebug() << "thread index:" << iter;

      // We are using Trace combiner visitors because we are not going to use
      // binning procedures during the visitor-mediated combination.
      TraceTreeNodeCombinerVisitorSPtr visitor_sp =
        std::make_shared<TraceTreeNodeCombinerVisitor>(
          mcsp_msRunDataSet, m_processingFlow, combiner_sp);

      // We want to be able to intercept any cancellation of the operation.

      connect(this,
              &MsRunDataSetTreeMassDataIntegratorToMz::cancelOperationSignal,
              [visitor_sp]() { visitor_sp->cancelOperation(); });
      // visitor_sp.get(),
      //&TraceTreeNodeCombinerVisitor::cancelOperation,
      // Qt::QueuedConnection);

      // The visitor gets the number of nodes to process from the data set tree
      // node. This signal tells the final user of the signal to set the max
      // value of the progress bar to number_of_nodes_to_process.

      connect(visitor_sp.get(),
              &TraceTreeNodeCombinerVisitor::setProgressBarMaxValueSignal,
              [this](std::size_t number_of_nodes_to_process) {
                emit setProgressBarMaxValueSignal(number_of_nodes_to_process);
              });

      // The visitor emits the signal to tell that the currently iterated node
      // has number_of_nodes_to_process children to process. Because the visitor
      // might operate in a thread and that there might be other threads
      // handling other nodes, we do not consider that the
      // number_of_nodes_to_process value is the total number of nodes to
      // process but only the number of nodes that the visitor is handling. We
      // thus update the max progress bar value by number_of_nodes_to_process.
      // In a typical setting, the user of the signal is the monitoring object,
      // be it a non-gui object or the TaskMonitorCompositeWidget.
      connect(visitor_sp.get(),
              &TraceTreeNodeCombinerVisitor::incrementProgressBarMaxValueSignal,
              [this](std::size_t number_of_nodes_to_process) {
                emit incrementProgressBarMaxValueSignal(
                  number_of_nodes_to_process);
              });

      connect(visitor_sp.get(),
              &TraceTreeNodeCombinerVisitor::setProgressBarCurrentValueSignal,
              [this](std::size_t number_of_processed_nodes) {
                emit setProgressBarCurrentValueSignal(
                  number_of_processed_nodes);
              });

      connect(visitor_sp.get(),
              &TraceTreeNodeCombinerVisitor::
                incrementProgressBarCurrentValueAndSetStatusTextSignal,
              [this](std::size_t increment, QString text) {
                emit incrementProgressBarCurrentValueAndSetStatusTextSignal(
                  increment, text);
              });

      connect(visitor_sp.get(),
              &TraceTreeNodeCombinerVisitor::setStatusTextSignal,
              [this](QString text) {
                // qDebug() << "Should emit text:" << text;
                emit setStatusTextSignal(text);
              });

      connect(visitor_sp.get(),
              &TraceTreeNodeCombinerVisitor::setStatusTextAndCurrentValueSignal,
              [this](QString text, std::size_t value) {
                // qDebug() << "Should emit text:" << text;
                emit setStatusTextAndCurrentValueSignal(text, value);
              });

      visitors.push_back(visitor_sp);
    }

  // Now perform a parallel iteration in the various visitors and ask them to
  // work on the matching ms run data set iterators pair.
  omp_set_num_threads(visitors.size());
#pragma omp parallel for
  for(std::size_t iter = 0; iter < visitors.size(); ++iter)
    {
      auto visitor_sp = visitors.at(iter);

      // qDebug() << "Visitor:" << &visitor << "at index:" << iter
      //<< "Executing from thread:" << QThread::currentThreadId();

      mcsp_msRunDataSet->msp_msRunDataSetTree->accept(
        *(visitor_sp.get()),
        iterators.at(iter).first,
        iterators.at(iter).second);
    }
  // End of
  // #pragma omp parallel for

  // At this point, the combination for each visitor has been performed. We
  // need to consolidate the various visitor's map traces into a single Trace
  // that will correspond to the actual final product of the combination.

  // qDebug() << "End of the iteration in the visitors";

  // If the task was cancelled, the monitor widget was locked. We need to
  // unlock it.
  emit unlockTaskMonitorCompositeWidgetSignal();
  emit setupProgressBarSignal(0, visitors.size() - 1);

  // We might be here because the user cancelled the operation in the for loop
  // above (visiting all the visitors). In this case m_isOperationCancelled is
  // true. We want to set it back to false, so that the following loop is gone
  // through. The user can ask that the operation be cancelled once more. But we
  // want that at least the performed work be used to show the trace.

  m_isOperationCancelled = false;

  for(std::size_t iter = 0; iter < visitors.size(); ++iter)
    {
      if(m_isOperationCancelled)
        break;

      auto &&visitor_sp = visitors.at(iter);

      const pappso::MapTrace &map_trace = visitor_sp->getMapTrace();

      // qDebug() << "In the consolidating loop, iter:" << iter
      //<< " with a map trace of this size : " << map_trace.size();

      emit setStatusTextSignal(
        QString("Consolidating mass spectra from thread %1").arg(iter + 1));

      emit setProgressBarCurrentValueSignal(iter + 1);

      visitor_sp->getCombiner()->combine(m_mapTrace, map_trace.toTrace());
    }

  // qDebug() << "The consolidated integrator map trace has size:"
  //<< m_mapTrace.size();

  // At this point, we have really combined all the traces into a single map
  // trace!

  return true;
}


bool
MsRunDataSetTreeMassDataIntegratorToMz::integrateToMzArbitraryBinning()
{
  // qDebug();

  //  This function needs to be called by integrateToMz() that has setup all the
  //  required parameters.

  // There are a lot of verification of the current context that have been
  // performed in the calling function.

  std::vector<pappso::MsRunDataSetTreeNode *> root_nodes =
    mcsp_msRunDataSet->getMsRunDataSetTreeCstSPtr()->getRootNodes();

  double start_rt = std::numeric_limits<double>::infinity();
  double end_rt   = std::numeric_limits<double>::infinity();

  bool integration_rt = m_processingFlow.innermostRtRange(start_rt, end_rt);

  using Iterator = std::vector<pappso::MsRunDataSetTreeNode *>::const_iterator;
  using Pair     = std::pair<Iterator, Iterator>;

  Pair pair;

  Iterator begin_iterator = root_nodes.begin();
  Iterator end_iterator   = root_nodes.end();

  std::size_t node_count = 0;

  if(integration_rt)
    {
      pair =
        mcsp_msRunDataSet->treeNodeIteratorRangeForRtRange(start_rt, end_rt);

      begin_iterator = pair.first;
      end_iterator   = pair.second;

      node_count = std::distance(begin_iterator, end_iterator);

      qDebug() << "node_count:" << node_count;
    }
  else
    qDebug() << "Not integration_rt";

  if(begin_iterator == root_nodes.end())
    {
      qDebug() << "There is nothing to integrate.";
      return false;
    }

  // Now that we know the non-0 count of nodes to be processed:
  emit setProgressBarMaxValueSignal(node_count);

  // But we want to parallelize the computation. Se we will allocate a vector of
  // visitors (as many as possible for the available processor threads), each
  // visitor having a subset of the initial data to integrate.

  // In the pair below, first is the ideal number of threads and second is the
  // number of nodes per thread.
  std::pair<std::size_t, std::size_t> best_parallel_params =
    bestParallelIntegrationParams(node_count);

  // qDebug() << "nodes_per_thread:" << best_parallel_params.second;

  using Iterator = std::vector<pappso::MsRunDataSetTreeNode *>::const_iterator;

  std::vector<MassSpectrumTreeNodeCombinerVisitorSPtr> visitors;
  std::vector<std::pair<Iterator, Iterator>> iterators =
    calculateMsRunDataSetTreeNodeIteratorPairs(begin_iterator,
                                               end_iterator,
                                               best_parallel_params.first,
                                               best_parallel_params.second);

  // Since we are asked that an integration to a mass spectrum be performed,
  // there must be a non-nullptr mz integration pointer in the most recent step.

  const MzIntegrationParams *mz_integration_params_p =
    m_processingFlow.mostRecentStep()->getMzIntegrationParamsPtr();

  if(mz_integration_params_p == nullptr)
    qFatal("Cannot be that the pointer is nullptr.");

  // When integrating to a mass spectrum with arbitrary binning, we first need
  // to establish the bins. Create a local copy that we'll be able to modify
  // before using it.

  MzIntegrationParams local_mz_integration_params = *mz_integration_params_p;

  local_mz_integration_params.setSmallestMz(
    mcsp_msRunDataSet->getMsRunDataSetStats().m_minMz);

  local_mz_integration_params.setGreatestMz(
    mcsp_msRunDataSet->getMsRunDataSetStats().m_maxMz);

  // qDebug().noquote() << "Starting integration with mz integration params:"
  //<< local_mz_integration_params.toString();

  // We use a MassSpectrum combiner because we are going to do binning.
  pappso::MassSpectrumPlusCombinerSPtr combiner_sp =
    std::make_shared<pappso::MassSpectrumPlusCombiner>(
      local_mz_integration_params.getDecimalPlaces());

  // Set the bins to the combiner by creating them according to the
  // specifications in the mz integration params instance.

  std::vector<double> bins = local_mz_integration_params.createBins();

  // qDebug() << "The number of bins is:" << bins.size();

  // Now set the bins into the combiner that will use them to perform the binned
  // combination.
  combiner_sp->setBins(bins);

  for(std::size_t iter = 0; iter < iterators.size(); ++iter)
    {
      // qDebug() << "thread index:" << iter;

      if(local_mz_integration_params.getBinningType() != BinningType::ARBITRARY)
        qFatal("Programming error.");

      // We are using MassSpectrum combiner visitors because we are going to use
      // binning procedures during the visitor-mediated combination.
      MassSpectrumTreeNodeCombinerVisitorSPtr visitor_sp =
        std::make_shared<MassSpectrumTreeNodeCombinerVisitor>(
          mcsp_msRunDataSet, m_processingFlow, combiner_sp);

      // We want to be able to intercept any cancellation of the operation.

      connect(this,
              &MsRunDataSetTreeMassDataIntegratorToMz::cancelOperationSignal,
              [visitor_sp]() { visitor_sp->cancelOperation(); });
      // visitor_sp.get(),
      //&MassSpectrumTreeNodeCombinerVisitor::cancelOperation,
      // Qt::QueuedConnection);

      // The visitor gets the number of nodes to process from the data set tree
      // node. This signal tells the final user of the signal to set the max
      // value of the progress bar to number_of_nodes_to_process.

      connect(
        visitor_sp.get(),
        &MassSpectrumTreeNodeCombinerVisitor::setProgressBarMaxValueSignal,
        [this](std::size_t number_of_nodes_to_process) {
          emit setProgressBarMaxValueSignal(number_of_nodes_to_process);
        });

      // The visitor emits the signal to tell that the currently iterated node
      // has number_of_nodes_to_process children to process. Because the visitor
      // might operate in a thread and that there might be other threads
      // handling other nodes, we do not consider that the
      // number_of_nodes_to_process value is the total number of nodes to
      // process but only the number of nodes that the visitor is handling. We
      // thus update the max progress bar value by number_of_nodes_to_process.
      // In a typical setting, the user of the signal is the monitoring object,
      // be it a non-gui object or the TaskMonitorCompositeWidget.
      connect(visitor_sp.get(),
              &MassSpectrumTreeNodeCombinerVisitor::
                incrementProgressBarMaxValueSignal,
              [this](std::size_t number_of_nodes_to_process) {
                emit incrementProgressBarMaxValueSignal(
                  number_of_nodes_to_process);
              });

      connect(
        visitor_sp.get(),
        &MassSpectrumTreeNodeCombinerVisitor::setProgressBarCurrentValueSignal,
        [this](std::size_t number_of_processed_nodes) {
          emit setProgressBarCurrentValueSignal(number_of_processed_nodes);
        });

      connect(visitor_sp.get(),
              &MassSpectrumTreeNodeCombinerVisitor::
                incrementProgressBarCurrentValueAndSetStatusTextSignal,
              [this](std::size_t increment, QString text) {
                emit incrementProgressBarCurrentValueAndSetStatusTextSignal(
                  increment, text);
              });

      connect(visitor_sp.get(),
              &MassSpectrumTreeNodeCombinerVisitor::setStatusTextSignal,
              [this](QString text) {
                // qDebug() << "Should emit text:" << text;
                emit setStatusTextSignal(text);
              });

      connect(visitor_sp.get(),
              &MassSpectrumTreeNodeCombinerVisitor::
                setStatusTextAndCurrentValueSignal,
              [this](QString text, std::size_t value) {
                // qDebug() << "Should emit text:" << text;
                emit setStatusTextAndCurrentValueSignal(text, value);
              });

      visitors.push_back(visitor_sp);
    }

  // Now perform a parallel iteration in the various visitors and ask them to
  // work on the matching ms run data set iterators pair.
  omp_set_num_threads(visitors.size());
#pragma omp parallel for
  for(std::size_t iter = 0; iter < visitors.size(); ++iter)
    {
      auto visitor_sp = visitors.at(iter);

      // qDebug() << "Visitor:" << visitor_sp.get() << "at index:" << iter
      //<< "Executing from thread:" << QThread::currentThreadId();

      mcsp_msRunDataSet->msp_msRunDataSetTree->accept(
        *(visitor_sp.get()),
        iterators.at(iter).first,
        iterators.at(iter).second);
    }
  // End of
  // #pragma omp parallel for

  // At this point, the combination for each visitor has been performed. We
  // need to consolidate the various visitor's map traces into a single
  // Trace that will correspond to the actual final product of the
  // combination.

  // qDebug() << "End of the iteration in the visitors";

  // If the task was cancelled, the monitor widget was locked. We need to
  // unlock it.
  emit unlockTaskMonitorCompositeWidgetSignal();
  emit setupProgressBarSignal(0, visitors.size() - 1);

  // We might be here because the user cancelled the operation in the for loop
  // above (visiting all the visitors). In this case m_isOperationCancelled is
  // true. We want to set it back to false, so that the following loop is gone
  // through. The user can ask that the operation be cancelled once more. But we
  // want that at least the performed work be used to show the trace.

  m_isOperationCancelled = false;

  for(std::size_t iter = 0; iter < visitors.size(); ++iter)
    {
      if(m_isOperationCancelled)
        break;

      auto &&visitor_sp = visitors.at(iter);

      const pappso::MapTrace &map_trace = visitor_sp->getMapTrace();

      // qDebug() << "In the consolidating loop, iter:" << iter
      //<< " with a map trace of this size : " << map_trace.size();

      emit setStatusTextSignal(
        QString("Consolidating mass spectra from thread %1").arg(iter + 1));

      emit setProgressBarCurrentValueSignal(iter + 1);

      visitor_sp->getCombiner()->combine(m_mapTrace, map_trace.toTrace());
    }

  // qDebug() << "The consolidated integrator map trace has size:"
  //<< m_mapTrace.size();

  // At this point, we have really combined all the traces into a single map
  // trace!

  return true;
}


} // namespace minexpert

} // namespace msxps
