/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

#pragma once

/////////////////////// StdLib includes


/////////////////////// Qt includes


/////////////////////// pappsomspp includes
#include <pappsomspp/massspectrum/qualifiedmassspectrum.h>
#include <pappsomspp/msrun/msrundatasettreenode.h>
#include <pappsomspp/msrun/msrundatasettreevisitor.h>
#include <pappsomspp/trace/maptrace.h>
#include <pappsomspp/processing/combiners/tracepluscombiner.h>


/////////////////////// Local includes
#include "ProcessingFlow.hpp"
#include "MsRunDataSet.hpp"
#include "BaseMsRunDataSetTreeNodeVisitor.hpp"
#include "MzIntegrationParams.hpp"


namespace msxps
{
namespace minexpert
{

class MassDataIntegrator;

class TraceTreeNodeCombinerVisitor;

typedef std::shared_ptr<TraceTreeNodeCombinerVisitor>
  TraceTreeNodeCombinerVisitorSPtr;

class TraceTreeNodeCombinerVisitor : public BaseMsRunDataSetTreeNodeVisitor
{

  friend class MassDataIntegrator;

  public:
  TraceTreeNodeCombinerVisitor(
    MsRunDataSetCstSPtr ms_run_data_set_csp,
    const ProcessingFlow &processing_flow,
    pappso::TracePlusCombinerSPtr &trace_combiner_sp);

  TraceTreeNodeCombinerVisitor(
    MsRunDataSetCstSPtr ms_run_data_set_csp,
    const ProcessingFlow &processing_flow,
    pappso::TracePlusCombinerSPtr &trace_combiner_sp,
    const MzIntegrationParams &mz_integration_params);

  TraceTreeNodeCombinerVisitor(const TraceTreeNodeCombinerVisitor &other);

  virtual ~TraceTreeNodeCombinerVisitor();

  void setCombiner(const pappso::TracePlusCombinerSPtr &trace_combiner_sp);
  pappso::TracePlusCombinerSPtr getCombiner();

  const pappso::MapTrace &getMapTrace() const;

  void setMzIntegrationParams(const MzIntegrationParams &mz_integration_params);
  const MzIntegrationParams &getMzIntegrationParams();

  TraceTreeNodeCombinerVisitor &
  operator=(const TraceTreeNodeCombinerVisitor &other);

  // Overrides the base class.
  virtual bool visit(const pappso::MsRunDataSetTreeNode &node) override;

  virtual bool
  checkProcessingStep(const ProcessingStep &step,
                      const pappso::MsRunDataSetTreeNode &node) override;

  virtual bool checkProcessingSpec(
    const std::pair<ProcessingType, ProcessingSpec *> &type_spec_pair,
    const pappso::MsRunDataSetTreeNode &node) override;

  virtual bool checkProcessingTypeByRt(
    const std::pair<ProcessingType, ProcessingSpec *> &spec_type_pair,
    const pappso::MsRunDataSetTreeNode &node) override;

  virtual bool checkProcessingTypeByMz(
    const std::pair<ProcessingType, ProcessingSpec *> &spec_type_pair,
    const pappso::MsRunDataSetTreeNode &node) override;

  virtual bool checkProcessingTypeByDt(
    const std::pair<ProcessingType, ProcessingSpec *> &spec_type_pair,
    const pappso::MsRunDataSetTreeNode &node) override;

  protected:
  pappso::TracePlusCombinerSPtr msp_combiner;

  MzIntegrationParams m_mzIntegrationParams;
  pappso::MapTrace m_mapTrace;
};


} // namespace minexpert

} // namespace msxps
