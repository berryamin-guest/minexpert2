/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes
#include <map>
#include <vector>
#include <memory>
#include <bitset>
#include <unordered_map>


/////////////////////// Qt includes
#include <QStringList>
#include <QString>
#include <QDateTime>


/////////////////////// pappsomspp includes


/////////////////////// Local includes


namespace msxps
{
namespace minexpert
{

typedef std::pair<QString, QString> ProcessingTypeStringPair;

class ProcessingType
{
  public:
  ProcessingType(std::bitset<64> bit_set);
  ProcessingType(const QString &brief_desc);
  ProcessingType(const ProcessingType &other);

  virtual ~ProcessingType();

  const QString &getBriefDesc() const;

  /*/// & = == operators */
  ProcessingType &operator=(const ProcessingType &other);
  bool operator==(const ProcessingType &other);
  std::bitset<64> operator&(const ProcessingType &mask);
  /* & = == operators */ ///

  std::bitset<64> bitSet() const;

  /*/// static bit set -- brief/detailed desc relating functions */
  static std::bitset<64> bitSet(QString brief_desc);
  static std::bitset<64> bitSet(const ProcessingType &processing_type);
  static QString briefDesc(std::bitset<64> bit_set);
  static QString detailedDesc(std::bitset<64> bit_set);
  /* static bit set -- brief/detailed desc relating functions */ ///

  /*/// static comparison and bit matching functions */
  static bool compare(const ProcessingType &a, const ProcessingType &b);

  static bool bitMatches(const ProcessingType &processing_type,
                         std::bitset<64> mask);
  static bool bitMatches(const ProcessingType &processing_type,
                         const QString &mask);
  static bool bitMatches(const ProcessingType &processing_type,
                         const ProcessingType &mask);
  /* static comparison and bit matching */ ///

  /*/// Begin non static comparison and bit matching */
  bool compare(const ProcessingType &other);

  bool bitMatches(std::bitset<64> mask) const;
  bool bitMatches(const ProcessingType &mask) const;
  bool bitMatches(const QString &mask) const;
  /* non static comparison and bit matching */ ///

  QString toString() const;

  private:
  static bool mapFilled;
  QString m_briefDesc = "[NOT_SET]";

  // This is a vector of ProcessingTypeDescription items that describe all the
  // processing types available in the program.

  void registerProcessingTypes();
  void
  registerProcessingCompositeTypes(std::vector<QString> brief_desc_list,
                                   ProcessingTypeStringPair dest_desc_pair);

  /// Begin static private data and functions
  static std::vector<ProcessingTypeStringPair> processingTypes;

  static std::unordered_map<std::bitset<64>, ProcessingTypeStringPair>
    processingTypesRegister;

  static bool isRegistered(QString brief_desc);
  static bool isRegistered(std::bitset<64> bit_set);
  /// End static private data and functions
};


#if 0
enum class ProcessingType : int64_t
{
  NOT_SET = 0x0000,

  FILE_TO_RT = 1LLU << 0,
  /*!< Integrate from mass data file to retention time (TIC chromatogram) */

  FILE_TO_RT_MZ = 1LLU << 1,
  /*!< Integrate from mass data file to retention time (TIC chromatogram) */

  FILE_TO_MZ = 1LLU << 2,
  /*!< Integrate from mass data file to retention time (TIC chromatogram) */

  FILE_TO_DT = 1LLU << 3,
  /*!< Integrate from mass data file to retention time (TIC chromatogram) */

  FILE_TO_DT_MZ_COLORMAP = 1LLU << 4,
  /*!< Integrate from mass data file to retention time (TIC chromatogram) */

  FILE_TO_TIC_INT = 1LLU << 5,
  /*!< Integrate from mass data file to retention time (XIC chromatogram) */

  FILE_TO_XIC = 1LLU << 6,
  /*!< Integrate from mass data file to retention time (XIC chromatogram) */

  RT_TO_MZ = 1LLU << 7,
  /*!< Integrate from TIC or XIC chromatogram to mass spectrum */

  RT_TO_DT = 1LLU << 8,
  /*!< Integrate from TIC or XIC chromatogram to drift spectrum */

  RT_TO_TIC_INT = 1LLU << 9,
  /*!< Integrate from TIC chrom range to TIC intensity single value */

  MZ_TO_RT = 1LLU << 10,
  /*!< Integrate from mass spectrum to XIC chromatogram (retention time) */

  MZ_TO_MZ = 1LLU << 11,
  /*!< Integrate from mass spectrum to mass spectrum (from color map)*/

  MZ_TO_DT = 1LLU << 12,
  /*!< Integrate from mass spectrum to drift spectrum*/

  MZ_TO_TIC_INT = 1LLU << 13,
  /*!< Integrate from mass spectrum range to TIC intensity single value */

  DT_TO_RT = 1LLU << 14,
  /*!< Integrate from drift spectrum to XIC (retention time) */

  DT_TO_MZ = 1LLU << 15,
  /*!< Integrate from drift spectrum to mass spectrum */

  DT_TO_DT = 1LLU << 16,
  /*!< Integrate from drift spectrum to drift spectrum (from color map)*/

  DT_TO_TIC_INT = 1LLU << 17,
  /*!< Integrate from drift spectrum range to TIC intensity single value */

  // Now the specific color map integration types:
  DTMZ_DT_TO_RT = 1LLU << 18,
  /*!< Integrate from mz/dt color map, dt range, to retention time (XIC
     chromatogram) */

  DTMZ_MZ_TO_RT = 1LLU << 19,
  /*!< Integrate from mz/dt color map, mz range, to retention time (XIC
     chromatogram) */

  // Now the specific color map integration types:
  DTMZ_DT_TO_MZ = 1LLU << 20,
  /*!< Integrate from mz/dt color map, dt range, to mass spectrum */

  DTMZ_MZ_TO_MZ = 1LLU << 21,
  /*!< Integrate from mz/dt color map, mz range, to mass spectrum */

  // Now the specific color map integration types:
  DTMZ_DT_TO_DT = 1LLU << 22,
  /*!< Integrate from mz/dt color map, dt range, to drift spectrum */

  DTMZ_MZ_TO_DT = 1LLU << 23,
  /*!< Integrate from mz/dt color map, mz range, to drift spectrum */

  // Now the specific color map integration types:
  DTMZ_DT_TO_TIC_INT = 1LLU << 24,
  /*!< Integrate from mz/dt color map, dt range, to TIC intensity value */

  DTMZ_MZ_TO_TIC_INT = 1LLU << 25,
  /*!< Integrate from mz/dt color map, mz range, to TIC intensity value */

  // Now the generic XXX_TO_YY
  XXX_TO_RT      = 1LLU << 26,
  XXX_TO_MZ      = 1LLU << 27,
  XXX_TO_DT      = 1LLU << 28,
  XXX_TO_TIC_INT = 1LLU << 29,

  // And the generic YY_TO_XXX
  RT_TO_XXX = 1LLU << 30,
  MZ_TO_XXX = 1LLU << 31,
  DT_TO_XXX = 1LLU << 32,

  RT_TO_ANY = (RT_TO_XXX | RT_TO_MZ | RT_TO_DT | RT_TO_TIC_INT),

  MZ_TO_ANY =
    (MZ_TO_XXX | MZ_TO_RT | DTMZ_MZ_TO_RT | DTMZ_MZ_TO_MZ | DTMZ_MZ_TO_DT |
     FILE_TO_XIC | MZ_TO_MZ | MZ_TO_DT | MZ_TO_TIC_INT),

  DT_TO_ANY = (DT_TO_XXX | DT_TO_RT | DTMZ_DT_TO_RT | DTMZ_DT_TO_MZ |
               DTMZ_DT_TO_DT | DT_TO_MZ | DT_TO_DT | DT_TO_TIC_INT),

  ANY_TO_RT = (XXX_TO_RT | FILE_TO_RT | FILE_TO_RT_MZ),

  ANY_TO_MZ = (XXX_TO_MZ | FILE_TO_MZ | RT_TO_MZ | MZ_TO_MZ | DT_TO_MZ |
               DTMZ_DT_TO_MZ | DTMZ_MZ_TO_MZ),

  ANY_TO_DT = (XXX_TO_DT | FILE_TO_DT | RT_TO_DT | MZ_TO_DT | DT_TO_DT |
               DTMZ_DT_TO_DT | DTMZ_MZ_TO_DT),

  ANY_TO_TIC_INT = (XXX_TO_TIC_INT | RT_TO_TIC_INT | MZ_TO_TIC_INT |
                    DT_TO_TIC_INT | DTMZ_DT_TO_TIC_INT | DTMZ_MZ_TO_TIC_INT),
};
#endif


} // namespace minexpert

} // namespace msxps
