/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes
#include <unordered_map>
#include <cmath>


/////////////////////// Qt includes
#include <QDebug>
#include <QString>
#include <QFile>
#include <QDateTime>


/////////////////////// pappsomspp includes
#include <pappsomspp/utils.h>
#include <pappsomspp/massspectrum/massspectrum.h>


/////////////////////// Local includes
#include "MzIntegrationParams.hpp"


namespace msxps
{
namespace minexpert
{


//! Map relating the BinningType to a textual representation
std::unordered_map<BinningType, QString> binningTypeMap{
  {BinningType::NONE, "NONE"},
  {BinningType::DATA_BASED, "DATA_BASED"},
  {BinningType::ARBITRARY, "ARBITRARY"}};


MzIntegrationParams::MzIntegrationParams()
{
  m_binningType = BinningType::NONE;
  mp_precision  = pappso::PrecisionFactory::getPpmInstance(20);
}


MzIntegrationParams::MzIntegrationParams(pappso::pappso_double minMz,
                                         pappso::pappso_double maxMz,
                                         BinningType binningType,
                                         int decimalPlaces,
                                         pappso::PrecisionPtr precisionPtr,
                                         bool applyMzShift,
                                         pappso::pappso_double mzShift,
                                         bool removeZeroValDataPoints)
  : m_smallestMzBin(minMz),
    m_greatestMzBin(maxMz),
    m_binningType(binningType),
    m_decimalPlaces(decimalPlaces),
    mp_precision(precisionPtr),
    m_applyMzShift(applyMzShift),
    m_mzShift(mzShift),
    m_removeZeroValDataPoints(removeZeroValDataPoints)
{
  if(mp_precision == nullptr)
    mp_precision = pappso::PrecisionFactory::getPpmInstance(20);
}


MzIntegrationParams::MzIntegrationParams(const MzIntegrationParams &other)
  : m_smallestMzBin(other.m_smallestMzBin),
    m_greatestMzBin(other.m_greatestMzBin),
    m_binningType(other.m_binningType),
    m_decimalPlaces(other.m_decimalPlaces),
    mp_precision(other.mp_precision),
    m_applyMzShift(other.m_applyMzShift),
    m_mzShift(other.m_mzShift),
    m_removeZeroValDataPoints(other.m_removeZeroValDataPoints),
    m_applySavGolFilter(other.m_applySavGolFilter)
{
  if(mp_precision == nullptr)
    mp_precision = pappso::PrecisionFactory::getPpmInstance(20);

  m_savGolParams.initialize(other.m_savGolParams);
}


MzIntegrationParams::MzIntegrationParams(
  const pappso::SavGolParams &savGolParams)
{
  setSavGolParams(savGolParams);
  mp_precision = pappso::PrecisionFactory::getPpmInstance(20);
}


MzIntegrationParams::~MzIntegrationParams()
{
}


MzIntegrationParams &
MzIntegrationParams::operator=(const MzIntegrationParams &other)
{
  if(this == &other)
    return *this;

  m_smallestMzBin = other.m_smallestMzBin;
  m_greatestMzBin = other.m_greatestMzBin;
  m_binningType   = other.m_binningType;

  m_decimalPlaces = other.m_decimalPlaces;

  mp_precision = other.mp_precision;
  if(mp_precision == nullptr)
    mp_precision = pappso::PrecisionFactory::getPpmInstance(20);

  m_applyMzShift            = other.m_applyMzShift;
  m_mzShift                 = other.m_mzShift;
  m_removeZeroValDataPoints = other.m_removeZeroValDataPoints;

  setSavGolParams(other.m_savGolParams);

  m_applySavGolFilter = other.m_applySavGolFilter;

  return *this;
}


void
MzIntegrationParams::setSmallestMz(pappso::pappso_double value)
{
  m_smallestMzBin = value;
}


void
MzIntegrationParams::updateSmallestMz(pappso::pappso_double value)
{
  m_smallestMzBin = m_smallestMzBin > value ? value : m_smallestMzBin;
}


pappso::pappso_double
MzIntegrationParams::getSmallestMz() const
{
  return m_smallestMzBin;
}


void
MzIntegrationParams::setGreatestMz(pappso::pappso_double value)
{
  m_greatestMzBin = value;
}


void
MzIntegrationParams::updateGreatestMz(pappso::pappso_double value)
{
  m_greatestMzBin = m_greatestMzBin < value ? value : m_greatestMzBin;
}


pappso::pappso_double
MzIntegrationParams::getGreatestMz() const
{
  return m_greatestMzBin;
}

void
MzIntegrationParams::setBinningType(BinningType binningType)
{
  m_binningType = binningType;
}

BinningType
MzIntegrationParams::getBinningType() const
{
  return m_binningType;
}

void
MzIntegrationParams::setDecimalPlaces(int decimal_places)
{
  m_decimalPlaces = decimal_places;
}


int
MzIntegrationParams::getDecimalPlaces() const
{
  return m_decimalPlaces;
}

void
MzIntegrationParams::setPrecision(pappso::PrecisionPtr precisionPtr)
{
  mp_precision = precisionPtr;

  if(mp_precision == nullptr)
    mp_precision = pappso::PrecisionFactory::getDaltonInstance(0.05);
}

pappso::PrecisionPtr
MzIntegrationParams::getPrecision() const
{
  return mp_precision;
}


void
MzIntegrationParams::setApplyMzShift(bool applyMzShift)
{
  m_applyMzShift = applyMzShift;
}


bool
MzIntegrationParams::isApplyMzShift() const
{
  return m_applyMzShift;
}


void
MzIntegrationParams::setRemoveZeroValDataPoints(bool removeOrNot)
{
  m_removeZeroValDataPoints = removeOrNot;
}


bool
MzIntegrationParams::isRemoveZeroValDataPoints() const
{
  return m_removeZeroValDataPoints;
}


void
MzIntegrationParams::setApplySavGolFilter(bool applySavGolFilter)
{
  m_applySavGolFilter = applySavGolFilter;
}


bool
MzIntegrationParams::isApplySavGolFilter() const
{
  return m_applySavGolFilter;
}


void
MzIntegrationParams::setSavGolParams(
  int nL, int nR, int m, int lD, bool convolveWithNr)
{
  m_savGolParams.nL             = nL;
  m_savGolParams.nR             = nR;
  m_savGolParams.m              = m;
  m_savGolParams.lD             = lD;
  m_savGolParams.convolveWithNr = convolveWithNr;
}


void
MzIntegrationParams::setSavGolParams(const pappso::SavGolParams &params)
{
  m_savGolParams.nL             = params.nL;
  m_savGolParams.nR             = params.nR;
  m_savGolParams.m              = params.m;
  m_savGolParams.lD             = params.lD;
  m_savGolParams.convolveWithNr = params.convolveWithNr;
}


pappso::SavGolParams
MzIntegrationParams::getSavGolParams() const
{
  return m_savGolParams;
}


//! Reset the instance to default values.
void
MzIntegrationParams::reset()
{
  m_smallestMzBin = std::numeric_limits<double>::min();
  m_greatestMzBin = std::numeric_limits<double>::min();
  m_binningType   = BinningType::NONE;

  // Special case for this member datum
  mp_precision = pappso::PrecisionFactory::getPpmInstance(20);

  m_applyMzShift            = false;
  m_mzShift                 = 0;
  m_removeZeroValDataPoints = false;

  m_savGolParams.nL             = 15;
  m_savGolParams.nR             = 15;
  m_savGolParams.m              = 4;
  m_savGolParams.lD             = 0;
  m_savGolParams.convolveWithNr = false;

  m_applySavGolFilter = false;
}


bool
MzIntegrationParams::isValid() const
{
  int errors = 0;

  if(m_binningType != BinningType::NONE)
    {
      errors += (m_smallestMzBin == std::numeric_limits<double>::max() ? 1 : 0);
      errors += (m_greatestMzBin == std::numeric_limits<double>::min() ? 1 : 0);

      errors += (mp_precision == nullptr ? 1 : 0);
    }

  if(errors)
    {
      qDebug()
        << "The m/z integration parameters are not valid or do not apply...";
    }

  return !errors;
}


bool
MzIntegrationParams::hasValidMzBinRange() const
{
  return (m_smallestMzBin != std::numeric_limits<double>::max()) &&
         (m_greatestMzBin != std::numeric_limits<double>::min());
}


std::vector<double>
MzIntegrationParams::createBins()
{

  // qDebug();

  std::vector<double> bins;

  if(m_binningType == BinningType::NONE)
    {
      // If no binning is to be performed, fine.
      return bins;
    }
  else if(m_binningType == BinningType::ARBITRARY)
    {
      // Use only data in the MzIntegrationParams member data.
      return createArbitraryBins();
    }
  else if(m_binningType == BinningType::DATA_BASED)
    {
      // qDebug();

      qFatal(
        "Fatal error at %s@%d -- %s(). "
        "Cannot create data base bins if no mass spectrum is passed as "
        "argument."
        "Program aborted.",
        __FILE__,
        __LINE__,
        __FUNCTION__);
    }

  return bins;
}


std::vector<double>
MzIntegrationParams::createBins(pappso::MassSpectrumCstSPtr mass_spectrum_csp)
{

  // qDebug();

  std::vector<double> bins;

  if(m_binningType == BinningType::NONE)
    {
      // If no binning is to be performed, fine.
      return bins;
    }
  else if(m_binningType == BinningType::ARBITRARY)
    {
      // Use only data in the MzIntegrationParams member data.
      return createArbitraryBins();
    }
  else if(m_binningType == BinningType::DATA_BASED)
    {
      // qDebug();

      // Use the first spectrum to perform the data-based bins

      return createDataBasedBins(mass_spectrum_csp);
    }

  return bins;
}


std::vector<double>
MzIntegrationParams::createArbitraryBins()
{

  // qDebug();

  // Now starts the tricky stuff. Depending on how the binning has been
  // configured, we need to take diverse actions.

  // qDebug() << "Bin size:" << mp_precision->toString();

  pappso::pappso_double min_mz = m_smallestMzBin;
  pappso::pappso_double max_mz = m_greatestMzBin;

  // qDebug() << QString::asprintf("min_mz: %.6f\n", min_mz)
  //<< QString::asprintf("max_mz: %.6f\n", max_mz);

  pappso::pappso_double binSize = mp_precision->delta(min_mz);

  // qDebug() << QString::asprintf(
  //"binSize is the precision delta for min_mz: %.6f\n", binSize);

  // Only compute the decimal places if they were not configured already.
  if(m_decimalPlaces == -1)
    {

      // We want as many decimal places as there are 0s between the integral
      // part of the double and the first non-0 cipher.  For example, if
      // binSize is 0.004, zero decimals is 2 and m_decimalPlaces is set to 3,
      // because we want decimals up to 4 included.

      m_decimalPlaces = pappso::Utils::zeroDecimalsInValue(binSize) + 1;

      // qDebug() << "With the binSize m_decimalPlaces was computed to be:"
      //<< m_decimalPlaces;
    }

  // Now that we have defined the value of m_decimalPlaces, let's use that
  // value.

  double first_mz = ceil((min_mz * std::pow(10, m_decimalPlaces)) - 0.49) /
                    pow(10, m_decimalPlaces);
  double last_mz =
    ceil((max_mz * pow(10, m_decimalPlaces)) - 0.49) / pow(10, m_decimalPlaces);

  // qDebug() << "After having accounted for the decimals, new min/max values:"
  //<< QString::asprintf("Very first data point: %.6f\n", first_mz)
  //<< QString::asprintf("Very last data point to reach: %.6f\n",
  // last_mz);

  // Instanciate the vector of mz double_s that we'll feed with the bins.

  std::vector<pappso::pappso_double> bins;

  // Store that very first value for later use in the loop.
  // The bins are notking more than:
  //
  // 1. The first mz (that is the smallest mz value found in all the spectra
  // 2. A sequence of mz values corresponding to that first mz value
  // incremented by the bin size.

  // Seed the root of the bin vector with the first mz value rounded as
  // requested.
  pappso::pappso_double previous_mz_bin = first_mz;
  previous_mz_bin = ceil((previous_mz_bin * pow(10, m_decimalPlaces)) - 0.49) /
                    pow(10, m_decimalPlaces);

  bins.push_back(previous_mz_bin);

  // Now continue adding mz values until we have reached the end of the
  // spectrum, that is the max_mz value, as converted using the decimals to
  // last_mz.

  // debugCount value used below for debugging purposes.
  // int debugCount = 0;

  while(previous_mz_bin <= last_mz)
    {

      // Calculate dynamically the precision delta according to the current mz
      // value.

      double current_mz =
        previous_mz_bin + mp_precision->delta(previous_mz_bin);

      // qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "()"
      //<< QString::asprintf(
      //"previous_mzBin: %.6f and current_mz: %.6f\n",
      // previous_mz_bin,
      // current_mz);

      // Now apply on the obtained mz value the decimals that were either set
      // or computed earlier.

      double current_rounded_mz =
        ceil((current_mz * pow(10, m_decimalPlaces)) - 0.49) /
        pow(10, m_decimalPlaces);

      // qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "()"
      //<< QString::asprintf(
      //"current_mz: %.6f and current_rounded_mz: %.6f and previous_mzBin "
      //": % .6f\n ",
      // current_mz,
      // current_rounded_mz,
      // previous_mz_bin);

      // If rounding makes the new value identical to the previous one, then
      // that means that we need to increase roughness.

      if(current_rounded_mz == previous_mz_bin)
        {
          ++m_decimalPlaces;

          current_rounded_mz =
            ceil((current_mz * pow(10, m_decimalPlaces)) - 0.49) /
            pow(10, m_decimalPlaces);

          // qDebug().noquote() << "Had to increment decimal places by one.";
        }

      bins.push_back(current_rounded_mz);

      // Use the local_mz value for the storage of the previous mz bin.
      previous_mz_bin = current_rounded_mz;
    }


#if 0

  QString fileName = "/tmp/massSpecArbitraryBins.txt-at-" +
                     QDateTime::currentDateTime().toString("yyyyMMdd-HH-mm-ss");

  qDebug() << __FILE__ << __LINE__
           << "Writing the list of bins setup in the "
              "mass spectrum in file "
           << fileName;

  QFile file(fileName);
  file.open(QIODevice::WriteOnly);

  QTextStream fileStream(&file);

  for(auto &&bin : bins)
    fileStream << QString("%1\n").arg(bin, 0, 'f', 10);

  fileStream.flush();
  file.close();

#endif

  // qDebug() << __FILE__ << __LINE__ << qSetRealNumberPrecision(10)
  //<< "Prepared bins with " << bins.size() << "elements."
  //<< "starting with mz" << bins.front() << "ending with mz"
  //<< bins.back();

  return bins;
}


std::vector<double>
MzIntegrationParams::createDataBasedBins(
  pappso::MassSpectrumCstSPtr mass_spectrum_csp)
{
  // qDebug();

  // The bins in *this mass spectrum must be calculated starting from the
  // data in the mass_spectrum_csp parameter.

  // Instanciate the vector of mz double_s that we'll feed with the bins.

  std::vector<pappso::pappso_double> bins;

  if(mass_spectrum_csp->size() < 2)
    return bins;

  // Make sure the spectrum is sorted, as this functions takes for granted
  // that the DataPoint instances are sorted in ascending x (== mz) value
  // order.
  pappso::MassSpectrum local_mass_spectrum = *mass_spectrum_csp;
  local_mass_spectrum.sortMz();

  pappso::pappso_double min_mz = m_smallestMzBin;

  // qDebug() << "The min_mz:" << min_mz;

  if(m_decimalPlaces != -1)
    min_mz = ceil((min_mz * pow(10, m_decimalPlaces)) - 0.49) /
             pow(10, m_decimalPlaces);


  // Two values for the definition of a MassSpectrumBin.

  // The first value of the mz range that defines the bin. This value is part
  // of the bin.
  pappso::pappso_double start_mz_in = min_mz;

  // The second value of the mz range that defines the bin. This value is
  // *not* part of the bin.
  pappso::pappso_double end_mz_out;

  std::vector<pappso::DataPoint>::const_iterator it =
    local_mass_spectrum.begin();

  pappso::pappso_double prev_mz = it->x;

  if(m_decimalPlaces != -1)
    prev_mz = ceil((prev_mz * pow(10, m_decimalPlaces)) - 0.49) /
              pow(10, m_decimalPlaces);

  ++it;

  while(it != local_mass_spectrum.end())
    {
      pappso::pappso_double next_mz = it->x;

      if(m_decimalPlaces != -1)
        next_mz = ceil((next_mz * pow(10, m_decimalPlaces)) - 0.49) /
                  pow(10, m_decimalPlaces);

      pappso::pappso_double step = next_mz - prev_mz;
      end_mz_out                 = start_mz_in + step;

      if(m_decimalPlaces != -1)
        end_mz_out = ceil((end_mz_out * pow(10, m_decimalPlaces)) - 0.49) /
                     pow(10, m_decimalPlaces);

      // The data point that is crafted has a 0 y-value. The binning must
      // indeed not create artificial intensity data.

      // qDebug() << "Pushing back bin:" << start_mz_in << end_mz_out;

      bins.push_back(start_mz_in);

      // Prepare next bin
      start_mz_in = end_mz_out;

      // Update prev_mz to be the current one for next iteration.
      prev_mz = next_mz;

      // Now got the next DataPoint instance.
      ++it;
    }

#if 0

  QString fileName = "/tmp/massSpecDataBasedBins.txt";

  qDebug() << __FILE__ << __LINE__
           << "Writing the list of bins setup in the "
              "mass spectrum in file "
           << fileName;

  QFile file(fileName);
  file.open(QIODevice::WriteOnly);

  QTextStream fileStream(&file);

  for(auto &&bin : m_bins)
    fileStream << QString("[%1-%2]\n")
                    .arg(bin.startMzIn, 0, 'f', 10)
                    .arg(bin.endMzOut, 0, 'f', 10);

  fileStream.flush();
  file.close();

  qDebug() << __FILE__ << __LINE__ << "Prepared bins with " << m_bins.size()
           << "elements."
           << "starting with mz" << m_bins.front().startMzIn << "ending with mz"
           << m_bins.back().endMzOut;

#endif

  return bins;
}


QString
MzIntegrationParams::toString(int offset, const QString &spacer) const
{
  QString lead;

  for(int iter = 0; iter < offset; ++iter)
    lead += spacer;

  QString text = lead;
  text += "m/z integration parameters:\n";

  text += lead;
  text += spacer;
  if(m_smallestMzBin != std::numeric_limits<double>::min())
    text.append(
      QString::asprintf("Smallest (first) m/z bin: %.6f\n", m_smallestMzBin));

  text += lead;
  text += spacer;
  if(m_greatestMzBin != std::numeric_limits<double>::min())
    text.append(
      QString::asprintf("Greatest (last) m/z bin: %.6f\n", m_greatestMzBin));

  text += lead;
  text += spacer;
  text.append(QString("Decimal places: %1\n").arg(m_decimalPlaces));

  std::unordered_map<BinningType, QString>::iterator it;
  it = binningTypeMap.find(m_binningType);

  if(it == binningTypeMap.end())
    qFatal(
      "Fatal error at %s@%d -- %s(). "
      "Cannot be that the binning type not found in the map."
      "Program aborted.",
      __FILE__,
      __LINE__,
      __FUNCTION__);

  text += lead;
  text += spacer;
  text.append(QString("Binning type: %1\n").arg(it->second.toLatin1().data()));

  // Only provide the details relative to the ARBITRARY binning type.

  if(m_binningType == BinningType::ARBITRARY)
    {
      text += lead;
      text += spacer;
      text += spacer;
      text.append(QString("Bin nominal size: %1\n")
                    .arg(mp_precision->getNominal(), 0, 'f', 6));

      text += lead;
      text += spacer;
      text += spacer;
      text.append(QString("Bin size: %2\n")
                    .arg(mp_precision->toString().toLatin1().data()));
    }

  // Now other data that are independent of the bin settings.

  text += lead;
  text += spacer;
  text +=
    QString("Apply m/z shift: %1\n").arg(m_applyMzShift ? "true" : "false");

  if(m_applyMzShift)
    {
      text += lead;
      text += spacer;
      text += spacer;
      text += QString("m/z shift: %1").arg(m_mzShift, 0, 'f', 6);
    }

  text += lead;
  text += spacer;
  text += QString("Remove 0-val data points: %1\n")
            .arg(m_removeZeroValDataPoints ? "true" : "false");

  // Finally the Savitzy-Golay parameters (if requested)

  if(m_applySavGolFilter)
    {
      text += lead;
      text += spacer;
      text.append("Savitzky-Golay parameters\n");
      text += lead;
      text += spacer;
      text += spacer;
      text +=
        QString("nL = %1 ; nR = %2 ; m = %3 ; lD = %4 ; convolveWithNr = %5\n")
          .arg(m_savGolParams.nL)
          .arg(m_savGolParams.nR)
          .arg(m_savGolParams.m)
          .arg(m_savGolParams.lD)
          .arg(m_savGolParams.convolveWithNr ? "true" : "false");
    }

  return text;
}


} // namespace minexpert
} // namespace msxps
