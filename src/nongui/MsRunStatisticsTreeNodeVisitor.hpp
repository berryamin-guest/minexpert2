/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

#pragma once

/////////////////////// StdLib includes


/////////////////////// Qt includes


/////////////////////// pappsomspp includes
#include <pappsomspp/massspectrum/qualifiedmassspectrum.h>
#include <pappsomspp/msrun/msrundatasettreenode.h>
#include <pappsomspp/msrun/msrundatasettreevisitor.h>


/////////////////////// Local includes
#include "ProcessingFlow.hpp"
#include "MsRunDataSet.hpp"
#include "BaseMsRunDataSetTreeNodeVisitor.hpp"
#include "MzIntegrationParams.hpp"
#include "MsRunDataSetStats.hpp"


namespace msxps
{
namespace minexpert
{

class MsRunStatisticsTreeNodeVisitor;

typedef std::shared_ptr<MsRunStatisticsTreeNodeVisitor>
  MsRunStatisticsTreeNodeVisitorSPtr;

class MsRunStatisticsTreeNodeVisitor : public BaseMsRunDataSetTreeNodeVisitor
{

  public:
  MsRunStatisticsTreeNodeVisitor(
    MsRunDataSetCstSPtr ms_run_data_set_csp,
    const ProcessingFlow &processing_flow,
    const MsRunDataSetStats &ms_run_data_set_stats);

  MsRunStatisticsTreeNodeVisitor(const MsRunStatisticsTreeNodeVisitor &other);

  virtual ~MsRunStatisticsTreeNodeVisitor();

  virtual void setProcessingFlow(const ProcessingFlow &processing_flow);

  void setLimitMzRangeStart(double value);
  void setLimitMzRangeEnd(double value);

  const MsRunDataSetStats &getMsRunDataSetStats() const;

  MsRunStatisticsTreeNodeVisitor &
  operator=(const MsRunStatisticsTreeNodeVisitor &other);

  // Overrides the base class.
  virtual bool visit(const pappso::MsRunDataSetTreeNode &node) override;

  virtual bool
  checkProcessingStep(const ProcessingStep &step,
                      const pappso::MsRunDataSetTreeNode &node) override;

  virtual bool checkProcessingSpec(
    const std::pair<ProcessingType, ProcessingSpec *> &type_spec_pair,
    const pappso::MsRunDataSetTreeNode &node) override;

  virtual bool checkProcessingTypeByRt(
    const std::pair<ProcessingType, ProcessingSpec *> &spec_type_pair,
    const pappso::MsRunDataSetTreeNode &node) override;

  virtual bool checkProcessingTypeByMz(
    const std::pair<ProcessingType, ProcessingSpec *> &spec_type_pair,
    const pappso::MsRunDataSetTreeNode &node) override;

  virtual bool checkProcessingTypeByDt(
    const std::pair<ProcessingType, ProcessingSpec *> &spec_type_pair,
    const pappso::MsRunDataSetTreeNode &node) override;

  protected:
  MsRunDataSetStats m_msRunDataSetStats;

  // We almost systematically need these m/z range values when making visits:
  // one criterium might be that the m/z value of data points (the x member of
  // DataPoint) be contained within the limit m/z range.
  double m_limitMzRangeStart = std::numeric_limits<double>::max();
  double m_limitMzRangeEnd   = std::numeric_limits<double>::max();
};


} // namespace minexpert

} // namespace msxps
