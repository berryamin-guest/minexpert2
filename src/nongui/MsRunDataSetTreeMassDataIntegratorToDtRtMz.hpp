/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes
#include <vector>


/////////////////////// Qt includes


/////////////////////// pappsomspp includes
#include <pappsomspp/trace/maptrace.h>


/////////////////////// Local includes
#include "globals.hpp"
#include "MsRunDataSet.hpp"
#include "ProcessingStep.hpp"
#include "ProcessingFlow.hpp"
#include "MassDataIntegrator.hpp"


namespace msxps
{
namespace minexpert
{

class MsRunDataSetTreeMassDataIntegratorToDtRtMz;

typedef std::shared_ptr<MsRunDataSetTreeMassDataIntegratorToDtRtMz> MsRunDataSetTreeMassDataIntegratorToDtRtMzSPtr;

class MsRunDataSetTreeMassDataIntegratorToDtRtMz : public MassDataIntegrator
{
  Q_OBJECT;

  public:
  MsRunDataSetTreeMassDataIntegratorToDtRtMz();

  MsRunDataSetTreeMassDataIntegratorToDtRtMz(MsRunDataSetCstSPtr ms_run_data_set_csp);

  MsRunDataSetTreeMassDataIntegratorToDtRtMz(MsRunDataSetCstSPtr ms_run_data_set_csp,
                     const ProcessingFlow &processing_flow);

  MsRunDataSetTreeMassDataIntegratorToDtRtMz(const MsRunDataSetTreeMassDataIntegratorToDtRtMz &other);

  virtual ~MsRunDataSetTreeMassDataIntegratorToDtRtMz();

  const ProcessingFlow &getProcessingFlow() const;
  void appendProcessingStep(ProcessingStep *processing_step_p);

  std::size_t getColorMapKeyCellCount() const;
  std::size_t getColorMapMzCellCount() const;

  double getColorMapMinKey() const;
  double getColorMapMaxKey() const;

  double getColorMapMinMz() const;
  double getColorMapMaxMz() const;


  public slots:

  bool integrateToDtRtMz(pappso::DataKind data_kind);

  signals:

  protected:
  std::size_t m_colorMapKeyCellCount = 0;
  std::size_t m_colorMapMzCellCount  = 0;

  double m_colorMapMinKey = std::numeric_limits<double>::max();
  double m_colorMapMaxKey = std::numeric_limits<double>::min();

  double m_colorMapMinMz = std::numeric_limits<double>::max();
  double m_colorMapMaxMz = std::numeric_limits<double>::min();
};

} // namespace minexpert

} // namespace msxps

Q_DECLARE_METATYPE(msxps::minexpert::MsRunDataSetTreeMassDataIntegratorToDtRtMz);
extern int msRunDataSetTreeMassDataIntegratorToDtRtMzMetaTypeId;

Q_DECLARE_METATYPE(msxps::minexpert::MsRunDataSetTreeMassDataIntegratorToDtRtMzSPtr);
extern int msRunDataSetTreeMassDataIntegratorToDtRtMzSPtrMetaTypeId;
