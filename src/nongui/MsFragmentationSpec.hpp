/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes
#include <map>
#include <vector>
#include <memory>


/////////////////////// Qt includes
#include <QString>
#include <QDateTime>


/////////////////////// pappsomspp includes
#include <pappsomspp/widget/precisionwidget/precisionwidget.h>


/////////////////////// Local includes


namespace msxps
{
namespace minexpert
{

class ProcessingSpec;

class MsFragmentationSpec
{
  friend class ProcessingSpec;
  friend class MsFragmentationSpecDlg;

  public:
  MsFragmentationSpec();
  MsFragmentationSpec(std::size_t ms_level,
                      const std::vector<double> &precursors,
                      std::vector<std::size_t> precursor_spectrum_indices);

  MsFragmentationSpec(const MsFragmentationSpec &other);

  virtual ~MsFragmentationSpec();

  MsFragmentationSpec &operator=(const MsFragmentationSpec &other);

	// Levels that are considered are all the levels in the mass spec acquisition
	// that are >= ms_level_value
  void setMsLevel(std::size_t ms_level);
  std::size_t getMsLevel() const;

  // The precision instance pointer to describe the tolerance with which the m/z
  // precursor values need to be searched.
  void setPrecisionPtr(pappso::PrecisionPtr precision_p);
  pappso::PrecisionPtr getPrecisionPtr() const;

  bool containsMzPrecursor(double searched_mz_value) const;

  bool
  containsSpectrumPrecursorIndex(std::size_t searched_precursor_index) const;

  std::size_t precursorsCount() const;
  std::size_t precursorSpectrumIndicesCount() const;

  bool isValid() const;

  QString msLevelToString() const;
  QString mzPrecursorsToString() const;
  QString precursorSpectrumIndicesToString() const;
  QString toString(int offset = 0, const QString &spacer = QString()) const;

  protected:
  std::size_t m_msLevel = 0;
  pappso::PrecisionPtr mp_precision =
    pappso::PrecisionFactory::getDaltonInstance(0.05);
  std::vector<double> m_precursors;
  std::vector<std::size_t> m_precursorSpectrumIndices;
};


} // namespace minexpert

} // namespace msxps
