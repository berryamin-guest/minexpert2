/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QString>
#include <QList>


/////////////////////// Local includes


namespace msxps
{
namespace minexpert
{


// #include <libisospec++/isoSpec++.h>
//
// extern const int elem_table_atomicNo[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const double
// elem_table_probability[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const double elem_table_mass[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const int elem_table_massNo[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const int
// elem_table_extraNeutrons[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const char* elem_table_element[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const char* elem_table_symbol[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const bool elem_table_Radioactive[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const double
// elem_table_log_probability[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];


class IsoSpecEntity
{
  protected:
  QString m_element;
  QString m_symbol;
  int m_atomicNo;
  double m_mass;
  int m_massNo;
  int m_extraNeutrons;
  double m_probability;
  double m_logProbability;
  bool m_radioactive;

  public:
  IsoSpecEntity(QString element,
                QString symbol,
                int atomicNo,
                double mass,
                int massNo,
                int extraNeutrons,
                double probability,
                double logProbability,
                bool radioactiveQString);

  IsoSpecEntity(const IsoSpecEntity &other);
  virtual ~IsoSpecEntity();

  QString getElement() const;
  QString getSymbol() const;
  int getAtomicNo() const;
  double getMass() const;
  int getMassNo() const;
  int getExtraNeutrons() const;
  double getProbability() const;
  double getLogProbability() const;
  bool getRadioactive() const;

  virtual IsoSpecEntity &operator=(const IsoSpecEntity &other);

  QString asText();
};

} // namespace minexpert

} // namespace msxps

