/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QString>

/////////////////////// Local includes
#include "IsoSpecEntity.hpp"

namespace msxps
{
namespace minexpert
{


// #include <libisospec++/isoSpec++.h>
//
// extern const int elem_table_atomicNo[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const double
// elem_table_probability[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const double elem_table_mass[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const int elem_table_massNo[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const int
// elem_table_extraNeutrons[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const char* elem_table_element[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const char* elem_table_symbol[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const bool elem_table_Radioactive[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const double
// elem_table_log_probability[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];

// This is the order of the columns in the gui TableView
// ELEMENT,
// SYMBOL,
// ATOMIC_NO,
// MASS,
// MASS_NO,
// EXTRA_NEUTRONS,
// PROBABILITY,
// LOG_PROBABILITY,
// RADIOACTIVE,

IsoSpecEntity::IsoSpecEntity(QString element,
                             QString symbol,
                             int atomicNo,
                             double mass,
                             int massNo,
                             int extraNeutrons,
                             double probability,
                             double logProbability,
                             bool radioactive)
  : m_element{element},
    m_symbol{symbol},
    m_atomicNo{atomicNo},
    m_mass{mass},
    m_massNo{massNo},
    m_extraNeutrons{extraNeutrons},
    m_probability{probability},
    m_logProbability{logProbability},
    m_radioactive{radioactive}
{
}


IsoSpecEntity::IsoSpecEntity(const IsoSpecEntity &other)
{
  m_element        = other.m_element;
  m_symbol         = other.m_symbol;
  m_atomicNo       = other.m_atomicNo;
  m_mass           = other.m_mass;
  m_massNo         = other.m_massNo;
  m_extraNeutrons  = other.m_extraNeutrons;
  m_probability    = other.m_probability;
  m_logProbability = other.m_logProbability;
  m_radioactive    = other.m_radioactive;
}

IsoSpecEntity::~IsoSpecEntity()
{
}

QString
IsoSpecEntity::getElement() const
{
  return m_element;
}

QString
IsoSpecEntity::getSymbol() const
{
  return m_symbol;
}

int
IsoSpecEntity::getAtomicNo() const
{
  return m_atomicNo;
}

double
IsoSpecEntity::getMass() const
{
  return m_mass;
}

int
IsoSpecEntity::getMassNo() const
{
  return m_massNo;
}

int
IsoSpecEntity::getExtraNeutrons() const
{
  return m_extraNeutrons;
}

double
IsoSpecEntity::getProbability() const
{
  return m_probability;
}

double
IsoSpecEntity::getLogProbability() const
{
  return m_logProbability;
}

bool
IsoSpecEntity::getRadioactive() const
{
  return m_radioactive;
}


IsoSpecEntity &
IsoSpecEntity::operator=(const IsoSpecEntity &other)
{
  if(&other == this)
    return *this;

  m_element        = other.m_element;
  m_symbol         = other.m_symbol;
  m_atomicNo       = other.m_atomicNo;
  m_mass           = other.m_mass;
  m_massNo         = other.m_massNo;
  m_extraNeutrons  = other.m_extraNeutrons;
  m_probability    = other.m_probability;
  m_logProbability = other.m_logProbability;
  m_radioactive    = other.m_radioactive;

  return *this;
};

QString
IsoSpecEntity::asText()
{
  return QString("%1, %2, %3, %4, %5, %6, %7, %8, %9")
    .arg(m_element)
    .arg(m_symbol)
    .arg(m_atomicNo)
    .arg(m_mass, 0, 'f', 60)
    .arg(m_massNo)
    .arg(m_extraNeutrons)
    .arg(m_probability, 0, 'f', 60)
    .arg(m_logProbability, 0, 'f', 60)
    .arg(m_radioactive);
}

} // namespace minexpert

} // namespace msxps
