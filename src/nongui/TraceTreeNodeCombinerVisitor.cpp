/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QDebug>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "TraceTreeNodeCombinerVisitor.hpp"


namespace msxps
{
namespace minexpert
{


TraceTreeNodeCombinerVisitor::TraceTreeNodeCombinerVisitor(
  MsRunDataSetCstSPtr ms_run_data_set_csp,
  const ProcessingFlow &processing_flow,
  pappso::TracePlusCombinerSPtr &trace_combiner_sp)
  : BaseMsRunDataSetTreeNodeVisitor(ms_run_data_set_csp, processing_flow),
    msp_combiner(trace_combiner_sp)
{
  //  qDebug() << "Processing flow has" << m_processingFlow.size() << "steps";
}


TraceTreeNodeCombinerVisitor::TraceTreeNodeCombinerVisitor(
  MsRunDataSetCstSPtr ms_run_data_set_csp,
  const ProcessingFlow &processing_flow,
  pappso::TracePlusCombinerSPtr &trace_combiner_sp,
  const MzIntegrationParams &mz_integration_params)
  : BaseMsRunDataSetTreeNodeVisitor(ms_run_data_set_csp, processing_flow),
    msp_combiner(trace_combiner_sp),
    m_mzIntegrationParams(mz_integration_params)
{
}


TraceTreeNodeCombinerVisitor::TraceTreeNodeCombinerVisitor(
  const TraceTreeNodeCombinerVisitor &other)
  : BaseMsRunDataSetTreeNodeVisitor(other),
    msp_combiner(other.msp_combiner),
    m_mzIntegrationParams(other.m_mzIntegrationParams),
    m_mapTrace(other.m_mapTrace)
{
}


TraceTreeNodeCombinerVisitor &
TraceTreeNodeCombinerVisitor::
operator=(const TraceTreeNodeCombinerVisitor &other)
{
  if(this == &other)
    return *this;

  BaseMsRunDataSetTreeNodeVisitor::operator=(other);

  m_mzIntegrationParams = other.m_mzIntegrationParams;
  m_mapTrace            = other.m_mapTrace;
  msp_combiner          = other.msp_combiner;

  return *this;
}


TraceTreeNodeCombinerVisitor::~TraceTreeNodeCombinerVisitor()
{
}


void
TraceTreeNodeCombinerVisitor::setCombiner(
  const pappso::TracePlusCombinerSPtr &trace_combiner_sp)
{
  msp_combiner = trace_combiner_sp;
}


pappso::TracePlusCombinerSPtr
TraceTreeNodeCombinerVisitor::getCombiner()
{
  return msp_combiner;
}


const pappso::MapTrace &
TraceTreeNodeCombinerVisitor::getMapTrace() const
{
  return m_mapTrace;
}


void
TraceTreeNodeCombinerVisitor::setMzIntegrationParams(
  const MzIntegrationParams &mz_integration_params)
{
  m_mzIntegrationParams = mz_integration_params;
}


const MzIntegrationParams &
TraceTreeNodeCombinerVisitor::getMzIntegrationParams()
{
  return m_mzIntegrationParams;
}


bool
TraceTreeNodeCombinerVisitor::visit(const pappso::MsRunDataSetTreeNode &node)
{

  // We visit a node, such that we can compute the trace.

  // qDebug() << "Visiting node:" << node.toString();

  if(!m_isProgressFeedbackSilenced)
    {
      // Whatever the status of this node (to be or not processed) let the user
      // know that we have gone through one more.

      // The "%c" format refers to the current value in the task monitor widget
      // that will craft the new text according to the current value after
      // having incremented it in the call below. This is necessary because this
      // visitor might be in a thread and the  we need to account for all the
      // thread when giving feedback to the user.

      emit incrementProgressBarCurrentValueAndSetStatusTextSignal(
        1, "Processed %c nodes");
    }

  // qDebug().noquote() << "Visiting node:" << &node << "text format:" <<
  // node.toString()
  //<< "with quaified mass spectrum:"
  //<< node.getQualifiedMassSpectrum()->toString();

  pappso::QualifiedMassSpectrumCstSPtr qualified_mass_spectrum_csp =
    checkQualifiedMassSpectrum(node);

  // Immediately check if the qualified mass spectrum is of a MS level that
  // matches the greatest MS level found in the member processing flow instance.

  if(!checkMsLevel(node))
    return false;

  // At this point we need to go deep in the processing flow's steps to check
  // if the node matches the whole set of the steps' specs.

  // qDebug() << "The processing flow has" << m_processingFlow.size() <<
  // "steps.";

  for(auto &&step : m_processingFlow)
    {
      if(checkProcessingStep(*step, node))
        {
          // qDebug() << "The node:" << &node << "matches the iterated step.";
        }
      else
        {
          return false;
        }
    }

  // qDebug().noquote() << "The qualified mass spectrum:"
  //<< qualified_mass_spectrum_csp->toString()
  //<< "rt in minutes:" << qualified_mass_spectrum_csp->getRtInMinutes()
  //<< "dt in milliseconds:"
  //<< qualified_mass_spectrum_csp->getDtInMilliSeconds();

  // qDebug() << "The current qualified mass spectrum has size:"
  //<< qualified_mass_spectrum_csp->size();

  m_mzIntegrationParams.updateSmallestMz(
    qualified_mass_spectrum_csp->getMassSpectrumSPtr()->front().x);
  m_mzIntegrationParams.updateGreatestMz(
    qualified_mass_spectrum_csp->getMassSpectrumSPtr()->back().x);

  // At this point, we need to do the combination!

  msp_combiner->combine(m_mapTrace,
                        *(qualified_mass_spectrum_csp->getMassSpectrumSPtr()));

  return true;
}


bool
TraceTreeNodeCombinerVisitor::checkProcessingStep(
  const ProcessingStep &step, const pappso::MsRunDataSetTreeNode &node)
{
  //  qDebug();

  // A processing step is a collection of mapped items:
  // std::map<ProcessingType, ProcessingSpec> m_processingTypeSpecMap;
  //
  // For each item, we need to check if the node matches the spec.

  for(auto &&pair : step.getProcessingTypeSpecMap())
    {
      if(checkProcessingSpec(pair, node))
        {
          //          qDebug() << "The node matches the iterated spec.";
        }
      else
        {
          //          qDebug() << "The node does not match the iterated
          //          spec.";

          return false;
        }
    }

  return true;
}


bool
TraceTreeNodeCombinerVisitor::checkProcessingSpec(
  const std::pair<ProcessingType, ProcessingSpec *> &type_spec_pair,
  const pappso::MsRunDataSetTreeNode &node)
{
  //  qDebug();

  // This is the most elemental component of a ProcessingFlow, so here we
  // actually look into the node.

  const ProcessingSpec *spec_p = type_spec_pair.second;

  pappso::QualifiedMassSpectrumCstSPtr qualified_mass_spectrum_csp =
    node.getQualifiedMassSpectrum();

  const MsFragmentationSpec fragmentation_spec =
    spec_p->getMsFragmentationSpec();

  if(fragmentation_spec.isValid())
    {
      // qDebug() << "Node:" << &node << "Spec's fragmentation spec *is* valid:"
      //<< fragmentation_spec.toString();

      // For each member of the MsFragmentationSpec structure, check if it is
      // to be used for the check.

#if 0
      if(fragmentation_spec.getMinimumMsLevel())
        {
          // qDebug() << "Node:" << &node
          //<< "The fragmentation spec has ms levels.";

          // Logically, if the currently handled mass spectrum has a ms level
          // that is greater than the fragmentation spec ms level, then that
          // means that it should be visited: in the process of making data
          // mining, one goes from the lowest ms level to higher ms levels, so
          // we need to keep the mass spectrum. At some point down the
          // processing datetime, a process spec will limit the mass spectra
          // that are actually validated.

          if(qualified_mass_spectrum_csp->getMsLevel() >=
             fragmentation_spec.getMinimumMsLevel())
            {
              // qDebug() << "Node:" << &node
              //<< "The ms level matches, with fragspec level:"
              //<< fragmentation_spec.msLevelsToString()
              //<< "and the node spectrum level:"
              //<< qualified_mass_spectrum_csp->getMsLevel();
            }
          else
            {
              // qDebug() << "Node:" << &node
              //<< "The ms level does not match, with fragspec level:"
              //<< fragmentation_spec.msLevelsToString()
              //<< "and the node spectrum level:"
              //<< qualified_mass_spectrum_csp->getMsLevel();

              return false;
            }
        }

#endif

      if(fragmentation_spec.precursorsCount())
        {
          if(!fragmentation_spec.containsMzPrecursor(
               qualified_mass_spectrum_csp->getPrecursorMz()))
            {
              // qDebug() << "Node:" << &node
              //<< "The precursor mz does not match.";

              return false;
            }
        }

      if(fragmentation_spec.precursorSpectrumIndicesCount())
        {
          if(!fragmentation_spec.containsSpectrumPrecursorIndex(
               qualified_mass_spectrum_csp->getPrecursorSpectrumIndex()))
            {
              // qDebug() << "Node:" << &node
              //<< "The precursor spectrum index does not match.";

              return false;
            }
        }
    }
  else
    {
      // Else, fragmentation is not a criterion for filtering data. So go on.
      // qDebug() << "The fragmentation spec is *not* valid.";
    }

  // qDebug() << "Frag spec ok, going on with the spec check.";

  ProcessingType type = type_spec_pair.first;

  if(type.bitMatches("RT_TO_ANY") || type.bitMatches("FILE_TO_MZ"))
    {
      //      qDebug();
      if(!checkProcessingTypeByRt(type_spec_pair, node))
        return false;
    }
  else if(type.bitMatches("MZ_TO_ANY"))
    {
      //      qDebug();
      if(!checkProcessingTypeByMz(type_spec_pair, node))
        return false;
    }
  else if(type.bitMatches("DT_TO_ANY"))
    {
      //      qDebug();
      if(!checkProcessingTypeByDt(type_spec_pair, node))
        return false;
    }

  // At this point we seem to understand that the node matched!

  return true;
}


bool
TraceTreeNodeCombinerVisitor::checkProcessingTypeByRt(
  const std::pair<ProcessingType, ProcessingSpec *> &type_spec_pair,
  const pappso::MsRunDataSetTreeNode &node)
{
  //  qDebug();

  pappso::QualifiedMassSpectrumCstSPtr qualified_mass_spectrum_csp =
    node.getQualifiedMassSpectrum();

  const ProcessingSpec *spec_p = type_spec_pair.second;

  if(spec_p->hasValidRange())
    {
      double rt = qualified_mass_spectrum_csp->getRtInMinutes();

      if(rt >= spec_p->getStart() && rt <= spec_p->getEnd())
        {
          //      qDebug() << "Returning true for Rt.";
          return true;
        }
      else
        return false;
    }

  return true;
}


bool
TraceTreeNodeCombinerVisitor::checkProcessingTypeByMz(
  const std::pair<ProcessingType, ProcessingSpec *> &type_spec_pair,
  const pappso::MsRunDataSetTreeNode &node)
{
  //  qDebug();

  // Are there specs about the m/z range to be accounted for in the
  // computation?

  const ProcessingSpec *spec_p = type_spec_pair.second;

  if(spec_p->hasValidRange())
    {
      // qDebug() << "Needed a m/z range filtering:" << spec_p->toString();

      // The point is that if there are more than one spec in the the flow's
      // steps that limit the m/z range, we want to be sure to perform the
      // combination using the innermost range. So ask the ProcessingFlow to
      // actually compute that innermost m/z range.

      // Make a local copy of the type_spec_pair
      std::pair<ProcessingType, ProcessingSpec *> local_type_spec_pair(
        type_spec_pair);

      if(m_processingFlow.innermostMzRange(local_type_spec_pair))
        {
          msp_combiner->setFilterResampleKeepXRange(
            pappso::FilterResampleKeepXRange(
              local_type_spec_pair.second->getStart(),
              local_type_spec_pair.second->getEnd()));
        }
      else
        qFatal(
          "Cannot be that there is no innermost m/z range if there is a valid "
          "m/z range in a spec.");
    }

  return true;
}


bool
TraceTreeNodeCombinerVisitor::checkProcessingTypeByDt(
  const std::pair<ProcessingType, ProcessingSpec *> &type_spec_pair,
  const pappso::MsRunDataSetTreeNode &node)
{
  //  qDebug();

  pappso::QualifiedMassSpectrumCstSPtr qualified_mass_spectrum_csp =
    node.getQualifiedMassSpectrum();

  const ProcessingSpec *spec_p = type_spec_pair.second;

  if(spec_p->hasValidRange())
    {
      double dt = qualified_mass_spectrum_csp->getDtInMilliSeconds();

      if(dt >= spec_p->getStart() && dt <= spec_p->getEnd())
        return true;
      else
        return false;
    }

  return true;
}


} // namespace minexpert

} // namespace msxps
