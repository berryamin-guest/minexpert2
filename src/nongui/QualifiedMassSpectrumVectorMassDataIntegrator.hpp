/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes
#include <vector>


/////////////////////// Qt includes


/////////////////////// pappsomspp includes
#include <pappsomspp/massspectrum/qualifiedmassspectrum.h>
#include <pappsomspp/processing/combiners/massspectrumpluscombiner.h>


/////////////////////// Local includes
#include "globals.hpp"
#include "MsRunDataSet.hpp"
#include "ProcessingFlow.hpp"


namespace msxps
{
namespace minexpert
{

class QualifiedMassSpectrumVectorMassDataIntegrator;

typedef std::shared_ptr<QualifiedMassSpectrumVectorMassDataIntegrator>
  QualifiedMassSpectrumVectorMassDataIntegratorSPtr;

typedef std::vector<pappso::QualifiedMassSpectrumCstSPtr>
  QualifiedMassSpectraVector;

typedef std::shared_ptr<QualifiedMassSpectraVector>
  QualifiedMassSpectraVectorSPtr;

typedef std::shared_ptr<const QualifiedMassSpectraVector>
  QualifiedMassSpectraVectorCstSPtr;

class QualifiedMassSpectrumVectorMassDataIntegrator : public QObject
{
  Q_OBJECT;

  public:
  QualifiedMassSpectrumVectorMassDataIntegrator();

  QualifiedMassSpectrumVectorMassDataIntegrator(
    MsRunDataSetCstSPtr ms_run_data_set_csp,
    const ProcessingFlow &processing_flow,
    QualifiedMassSpectraVectorSPtr &qualified_mass_spectra_to_integrate_sp);

  QualifiedMassSpectrumVectorMassDataIntegrator(
    const QualifiedMassSpectrumVectorMassDataIntegrator &other);

  virtual ~QualifiedMassSpectrumVectorMassDataIntegrator();

  MsRunDataSetCstSPtr getMsRunDataSet() const;

  void setProcessingFlow(const ProcessingFlow &processing_flow);
  const ProcessingFlow &getProcessingFlow() const;

  std::vector<pappso::QualifiedMassSpectrumCstSPtr> &getQualifiedMassSpectra();

  virtual pappso::QualifiedMassSpectrumCstSPtr checkQualifiedMassSpectrum(
    pappso::QualifiedMassSpectrumCstSPtr &qualified_mass_spectrum_csp);

  const pappso::MapTrace &getMapTrace() const;

  public slots:

  void cancelOperation();


  signals:
  void cancelOperationSignal();

  void setTaskDescriptionTextSignal(QString text);

  void setupProgressBarSignal(std::size_t min_value, std::size_t max_value);
  void setProgressBarMinValueSignal(std::size_t value);
  void setProgressBarMaxValueSignal(std::size_t value);
  void incrementProgressBarMaxValueSignal(std::size_t increment);
  void setProgressBarCurrentValueSignal(std::size_t number_of_processed_nodes);
  void incrementProgressBarCurrentValueSignal(std::size_t increment);

  void setStatusTextSignal(QString text);
  void appendStatusTextSignal(QString text);
  void setStatusTextAndCurrentValueSignal(QString text, int value);

  void
  incrementProgressBarCurrentValueAndSetStatusTextSignal(std::size_t increment,
                                                         const QString &text);

  void logTextToConsoleSignal(QString text);
  void logColoredTextToConsoleSignal(QString text, QColor color);

  void lockTaskMonitorCompositeWidgetSignal();
  void unlockTaskMonitorCompositeWidgetSignal();

  protected:
  MsRunDataSetCstSPtr mcsp_msRunDataSet = nullptr;

  ProcessingFlow m_processingFlow;

  std::pair<double, double> m_rtRange = std::pair<double, double>(
    std::numeric_limits<double>::max(), std::numeric_limits<double>::min());

  std::pair<double, double> m_dtRange = std::pair<double, double>(
    std::numeric_limits<double>::max(), std::numeric_limits<double>::min());

  const QualifiedMassSpectraVectorSPtr
    mcsp_qualifiedMassSpectraToIntegrateVector;

  pappso::MapTrace m_mapTrace;

  bool m_isOperationCancelled = false;

  std::size_t bestThreadCount();

  // In the pair below, first is the ideal number of threads and second is the
  // number of nodes per thread.
  std::pair<std::size_t, std::size_t>
  bestParallelIntegrationParams(std::size_t node_count);

  virtual void setInnermostRanges();

  virtual bool checkMsLevel(
    pappso::QualifiedMassSpectrumCstSPtr &qualified_mass_spectrum_csp);

  virtual bool checkRtRange(
    pappso::QualifiedMassSpectrumCstSPtr &qualified_mass_spectrum_csp);

  virtual bool checkDtRange(
    pappso::QualifiedMassSpectrumCstSPtr &qualified_mass_spectrum_csp);

  virtual bool checkProcessingStep(
    const ProcessingStep &step,
    pappso::QualifiedMassSpectrumCstSPtr &qualified_mass_spectrum_csp);

  // Special for integrations to a mass spectrum (need a combiner)
  virtual bool checkProcessingStep(
    const ProcessingStep &step,
    pappso::QualifiedMassSpectrumCstSPtr &qualified_mass_spectrum_csp,
    pappso::MassDataCombinerInterface &mass_spectrum_combiner);

  virtual bool checkProcessingSpec(
    const std::pair<ProcessingType, ProcessingSpec *> &type_spec_pair,
    pappso::QualifiedMassSpectrumCstSPtr &qualified_mass_spectrum_csp);

  // Special for integrations to a mass spectrum (need a combiner)
  virtual bool checkProcessingSpec(
    const std::pair<ProcessingType, ProcessingSpec *> &type_spec_pair,
    pappso::QualifiedMassSpectrumCstSPtr &qualified_mass_spectrum_csp,
    pappso::MassDataCombinerInterface &mass_spectrum_combiner);

  virtual bool checkProcessingTypeByRt(
    const std::pair<ProcessingType, ProcessingSpec *> &spec_type_pair,
    pappso::QualifiedMassSpectrumCstSPtr &qualified_mass_spectrum_csp);

  virtual bool checkProcessingTypeByMz(
    const std::pair<ProcessingType, ProcessingSpec *> &spec_type_pair,
    pappso::QualifiedMassSpectrumCstSPtr &qualified_mass_spectrum_csp);

  // Special for integrations to a mass spectrum (need a combiner)
  virtual bool checkProcessingTypeByMz(
    const std::pair<ProcessingType, ProcessingSpec *> &spec_type_pair,
    pappso::QualifiedMassSpectrumCstSPtr &qualified_mass_spectrum_csp,
    pappso::MassDataCombinerInterface &mass_spectrum_combiner);

  virtual bool checkProcessingTypeByDt(
    const std::pair<ProcessingType, ProcessingSpec *> &spec_type_pair,
    pappso::QualifiedMassSpectrumCstSPtr &qualified_mass_spectrum_csp);

  protected:
  // We almost systematically need these m/z range values when making visits:
  // one criterium might be that the m/z value of data points (the x member of
  // DataPoint) be contained within the limit m/z range.
  double m_limitMzRangeStart = std::numeric_limits<double>::max();
  double m_limitMzRangeEnd   = std::numeric_limits<double>::max();

  using Iterator =
    std::vector<pappso::QualifiedMassSpectrumCstSPtr>::const_iterator;

  std::vector<std::pair<Iterator, Iterator>>
  calculateIteratorPairs(std::size_t thread_count, int nodes_per_thread);

  std::vector<std::pair<Iterator, Iterator>>
  calculateIteratorPairs(Iterator begin_iterator,
                         Iterator end_iterator,
                         std::size_t thread_count,
                         int nodes_per_thread);

  private:
};


} // namespace minexpert

} // namespace msxps


Q_DECLARE_METATYPE(
  msxps::minexpert::QualifiedMassSpectrumVectorMassDataIntegrator);
extern int qualifiedMassSpectrumVectorMassDataIntegratorMetaTypeId;

Q_DECLARE_METATYPE(
  msxps::minexpert::QualifiedMassSpectrumVectorMassDataIntegratorSPtr);
extern int qualifiedMassSpectrumVectorMassDataIntegratorSPtrMetaTypeId;

