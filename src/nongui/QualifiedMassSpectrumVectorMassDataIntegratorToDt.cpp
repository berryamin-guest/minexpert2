/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes
#include <vector>

/////////////////////// OPENMP include
#include <omp.h>

/////////////////////// Qt includes
#include <QDebug>
#include <QThread>


/////////////////////// pappsomspp includes
#include <pappsomspp/processing/combiners/tracepluscombiner.h>


/////////////////////// Local includes
#include "QualifiedMassSpectrumVectorMassDataIntegratorToDt.hpp"


int qualifiedMassSpectrumVectorMassDataIntegratorToDtMetaTypeId =
  qRegisterMetaType<
    msxps::minexpert::QualifiedMassSpectrumVectorMassDataIntegratorToDt>(
    "msxps::minexpert::QualifiedMassSpectrumVectorMassDataIntegratorToDt");

int qualifiedMassSpectrumVectorMassDataIntegratorToDtSPtrMetaTypeId =
  qRegisterMetaType<
    msxps::minexpert::QualifiedMassSpectrumVectorMassDataIntegratorToDtSPtr>(
    "msxps::minexpert::QualifiedMassSpectrumVectorMassDataIntegratorToDtSPtr");


namespace msxps
{
namespace minexpert
{

// Needed for qRegisterMetaType
QualifiedMassSpectrumVectorMassDataIntegratorToDt::
  QualifiedMassSpectrumVectorMassDataIntegratorToDt()
{
}


QualifiedMassSpectrumVectorMassDataIntegratorToDt::
  QualifiedMassSpectrumVectorMassDataIntegratorToDt(
    MsRunDataSetCstSPtr ms_run_data_set_csp,
    const ProcessingFlow &processing_flow,
    QualifiedMassSpectraVectorSPtr &qualified_mass_spectra_to_integrate_sp)
  : QualifiedMassSpectrumVectorMassDataIntegrator(
      ms_run_data_set_csp,
      processing_flow,
      qualified_mass_spectra_to_integrate_sp)
{
  m_processingFlow.setMsRunDataSetCstSPtr(mcsp_msRunDataSet);
}


// Needed for qRegisterMetaType
QualifiedMassSpectrumVectorMassDataIntegratorToDt::
  QualifiedMassSpectrumVectorMassDataIntegratorToDt(
    const QualifiedMassSpectrumVectorMassDataIntegratorToDt &other)
  : QualifiedMassSpectrumVectorMassDataIntegrator()
{
}


QualifiedMassSpectrumVectorMassDataIntegratorToDt::
  ~QualifiedMassSpectrumVectorMassDataIntegratorToDt()
{
}


// Old version
#if 0
void
QualifiedMassSpectrumVectorMassDataIntegratorToDt::integrateToDt()
{
  //qDebug();

  std::chrono::system_clock::time_point chrono_start_time =
    std::chrono::system_clock::now();

  // If there are no data, nothing to do.
  std::size_t mass_spectra_count = m_vectorOfQualifiedMassSpectraSPtr->size();

  if(!mass_spectra_count)
    {
      qDebug() << "The qualified mass spectrum vector is empty, nothing to do.";

      emit cancelOperationSignal();
    }

  // Nothing special to do about processing flow info because that must have
  // been set before calling this function.

  // We need to clear the map trace!
  m_mapTrace.clear();

  // Determine the best number of threads to use and how many spectra to provide
  // each of the threads.

  // In the pair below, first is the ideal number of threads and second is the
  // number of mass spectra per thread.
  std::pair<std::size_t, std::size_t> best_parallel_params =
    bestParallelIntegrationParams(mass_spectra_count);

  // Handy alias.
  using Iterator =
    std::vector<pappso::QualifiedMassSpectrumCstSPtr>::const_iterator;

  // Prepare a set of iterators that will distribute all the mass spectra
  // uniformly.
  std::vector<std::pair<Iterator, Iterator>> iterators =
    calculateQualifiedMassSpectrumVectorIteratorPairs(
      best_parallel_params.first, best_parallel_params.second);

  // Set aside a vector of std::map<double, double> to store retention time
  // values and their TIC intensity

  // Handy aliases.
  using Pair    = std::pair<double, double>;
  using Map     = std::map<double, double>;
  using MapSPtr = std::shared_ptr<Map>;

  std::vector<MapSPtr> vector_of_dt_spec_map;
  for(std::size_t iter = 0; iter < iterators.size(); ++iter)
    vector_of_dt_spec_map.push_back(
      std::make_shared<Map>());

  // Now that we have all the parallel material, we can start the parallel work.
  emit setProgressBarMaxValueSignal(mass_spectra_count);

  omp_set_num_threads(iterators.size());
#pragma omp parallel for ordered
  for(std::size_t iter = 0; iter < iterators.size(); ++iter)
    {
      qDebug() << "thread index:" << iter;

      // Get the iterator range that we need to deal in this thread.
      Iterator current_iterator = iterators.at(iter).first;
      Iterator end_iterator     = iterators.at(iter).second;

      // Get the map for this thread.

      MapSPtr current_dt_spec_map_sp = vector_of_dt_spec_map.at(iter);

      std::size_t qualified_mass_spectrum_count =
        std::distance(current_iterator, end_iterator);

      qDebug() << "For thread index:" << iter
               << "the number of mass spectra to process:"
               << qualified_mass_spectrum_count;

      while(current_iterator != end_iterator)
        {
          // We want to be able to intercept any cancellation of the
          // operation.

          if(m_isOperationCancelled)
            break;

          // Make the computation.

          // For each mass spectrum all we need is compute its TIC value, that
          // is, the sum of all its intensities (the y value of the DataPoint
          // instances it contains).

          pappso::QualifiedMassSpectrumCstSPtr qualified_mass_spectrum_csp =
            *current_iterator;

          // Test if the mass spectrum is nullptr.

          pappso::MassSpectrumCstSPtr mass_spectrum_csp =
            qualified_mass_spectrum_csp->getMassSpectrumCstSPtr();

          if(qualified_mass_spectrum_csp == nullptr)
            qFatal("Cannot be nullptr");
          if(qualified_mass_spectrum_csp.get() == nullptr)
            qFatal("Cannot be nullptr");

          if(qualified_mass_spectrum_csp->getMassSpectrumSPtr() == nullptr)
            {
              //qDebug() << "Need to access the mass data right from the file.";

              std::size_t mass_spectrum_index =
                mcsp_msRunDataSet->getMsRunDataSetTreeCstSPtr()
                  ->massSpectrumIndex(qualified_mass_spectrum_csp);

              pappso::MsRunReaderCstSPtr ms_run_reader_csp =
                mcsp_msRunDataSet->getMsRunReaderCstSPtr();

              qualified_mass_spectrum_csp =
                std::make_shared<pappso::QualifiedMassSpectrum>(
                  ms_run_reader_csp->qualifiedMassSpectrum(mass_spectrum_index,
                                                           true));
            }

          if(qualified_mass_spectrum_csp == nullptr)
            {
              qFatal("Failed to read the mass spectral data from the file.");
            }

          // Great, at this point we actually have the mass spectrum!

          double sumY = 0;
          sumY = qualified_mass_spectrum_csp->getMassSpectrumSPtr()->sumY();

          if(!sumY)
            {
              continue;
            }

          double dt = qualified_mass_spectrum_csp->getDtInMilliSeconds();

          using Iterator = Map::iterator;

          // Try to insert the pair into the map. Check if that was done or not.
          std::pair<Iterator, bool> res =
            current_dt_spec_map_sp->insert(Pair(dt, sumY));

          if(!res.second)
            {
              // One other same rt value was seen already (like in ion mobility
              // mass spectrometry, for example). Only increment the y value.

              res.first->second += sumY;
            }

          // qDebug() << "Processed one more spectrum.";

          emit incrementProgressBarCurrentValueAndSetStatusTextSignal(
            1, "Processed spectrum");

          // Now we can go to the next mass spectrum.
          ++current_iterator;
        }
        // End of
        // while(current_iterator != end_iterator)
    }
  // End of
  // #pragma omp parallel for ordered
  // for(std::size_t iter = 0; iter < iterators.size(); ++iter)

  // In the loop above, for each thread, a std::map<double, double> map was
  // filled in with the rt, TIC pairs.

  // If the task was cancelled, the monitor widget was locked. We need to
  // unlock it.
  emit unlockTaskMonitorCompositeWidgetSignal();
  emit setupProgressBarSignal(0, vector_of_dt_spec_map.size() - 1);

  // We might be here because the user cancelled the operation in the for loop
  // above (visiting all the visitors). In this case m_isOperationCancelled is
  // true. We want to set it back to false, so that the following loop is gone
  // through. The user can ask that the operation be cancelled once more. But
  // we want that at least the performed work be used to show the trace.

  m_isOperationCancelled = false;
  int iter               = 0;

  for(auto &&map_sp : vector_of_dt_spec_map)
    {
      ++iter;

      if(m_isOperationCancelled)
        break;

      //qDebug() << "In the consolidating loop, iter:" << iter
               //<< "with a map of this size : " << map_sp->size();

      for(auto &&pair : *map_sp)
        {

          emit setStatusTextSignal(
            QString("Consolidating drift spectra from thread %1")
              .arg(iter));

          emit setProgressBarCurrentValueSignal(iter);

          double dt  = pair.first;
          double tic = pair.second;

          using Iterator = Map::iterator;

          std::pair<Iterator, bool> res = m_mapTrace.insert(Pair(dt, tic));

          if(!res.second)
            {
              // One other same rt value was seen already (like in ion mobility
              // mass spectrometry, for example). Only increment the y value.

              res.first->second += tic;
            }
        }
    }

	// Write the data to file so that we can check. It works !!!
  // pappso::Utils::appendToFile(m_mapTrace.toString(), "/tmp/prova.txt");

  std::chrono::system_clock::time_point chrono_end_time =
    std::chrono::system_clock::now();

  QString chrono_string = pappso::Utils::chronoIntervalDebugString(
    "Integration to drift spectrum from the MS run data set table view "
    "took:",
    chrono_start_time,
    chrono_end_time);

  emit logTextToConsoleSignal(chrono_string);

  //qDebug().noquote() << chrono_string;

  return;
}

// Old version
#endif


void
QualifiedMassSpectrumVectorMassDataIntegratorToDt::integrateToDt()
{
  // qDebug();

  std::chrono::system_clock::time_point chrono_start_time =
    std::chrono::system_clock::now();

  // If there are no data, nothing to do.
  std::size_t mass_spectra_count =
    mcsp_qualifiedMassSpectraToIntegrateVector->size();

  if(!mass_spectra_count)
    {
      qDebug() << "The qualified mass spectrum vector is empty, nothing to do.";

      emit cancelOperationSignal();
    }

  // Nothing special to do about processing flow info because that must have
  // been set before calling this function.

  // We need to clear the map trace!
  m_mapTrace.clear();

  emit setTaskDescriptionTextSignal("Integrating to a drift spectrum");

  // We need a processing flow to work, and, in particular, a processing step
  // from which to find the specifics of the calculation.

  if(!m_processingFlow.size())
    qFatal("The processing flow cannot be empty. Program aborted.");

  // Get the most recent step that holds all the specifics of this integration.
  const ProcessingStep *most_recent_processing_step_p =
    m_processingFlow.mostRecentStep();

  // There must be at least one ProcessingSpec (and thus a type) in the
  // processing step.

  if(!most_recent_processing_step_p->processingTypes().size())
    qFatal("The processing step cannot be empty. Program aborted.");

  // We are integrating to m/z so we need a spec description that matches
  // "ANY_TO_MZ".

  if(!most_recent_processing_step_p->matches("ANY_TO_DT"))
    qFatal("There should be one ProcessingType = ANY_TO_DT. Program aborted.");

  // At this point, allocate a visitor that is specific for the calculation of
  // the mass spectrum. Since we are not binning, we can use a visitor that
  // integrates to a pappso::Trace, not a pappso::MassSpectrum.

  // But we want to parallelize the computation. Se we will allocate a vector of
  // visitors (as many as possible for the available processor threads), each
  // visitor having a subset of the initial data to integrate.

  // Determine the best number of threads to use and how many spectra to provide
  // each of the threads.

  // In the pair below, first is the ideal number of threads and second is the
  // number of mass spectra per thread.
  std::pair<std::size_t, std::size_t> best_parallel_params =
    bestParallelIntegrationParams(
      mcsp_qualifiedMassSpectraToIntegrateVector->size());

  // Handy alias.
  using MassSpecVector = std::vector<pappso::QualifiedMassSpectrumCstSPtr>;
  using VectorIterator = MassSpecVector::const_iterator;

  // Prepare a set of iterators that will distribute all the mass spectra
  // uniformly.
  std::vector<std::pair<VectorIterator, VectorIterator>> iterators =
    calculateIteratorPairs(best_parallel_params.first,
                           best_parallel_params.second);

  // Set aside a vector of std::map<double, double> to store retention time
  // values and their TIC intensity

  // Handy aliases.
  using Pair             = std::pair<double, double>;
  using DtTicMap         = std::map<double, double>;
  using DtTicMapSPtr     = std::shared_ptr<DtTicMap>;
  using DtTicMapIterator = DtTicMap::iterator;

  std::vector<DtTicMapSPtr> vector_of_dt_spec_map;
  for(std::size_t iter = 0; iter < iterators.size(); ++iter)
    vector_of_dt_spec_map.push_back(std::make_shared<DtTicMap>());


  // Now that we have all the parallel material, we can start the parallel work.
  emit setProgressBarMaxValueSignal(
    mcsp_qualifiedMassSpectraToIntegrateVector->size());

  // Now that we have all the parallel material, we can start the parallel work.
  emit setProgressBarMaxValueSignal(mass_spectra_count);

  omp_set_num_threads(iterators.size());
#pragma omp parallel for ordered
  for(std::size_t iter = 0; iter < iterators.size(); ++iter)
    {
      // qDebug() << "thread index:" << iter;

      // Get the iterator range that we need to deal in this thread.
      VectorIterator current_iterator = iterators.at(iter).first;
      VectorIterator end_iterator     = iterators.at(iter).second;

      // Get the map for this thread.

      DtTicMapSPtr current_dt_spec_map_sp = vector_of_dt_spec_map.at(iter);

      while(current_iterator != end_iterator)
        {
          // We want to be able to intercept any cancellation of the
          // operation.

          if(m_isOperationCancelled)
            break;

          // Make the computation.

          // For each mass spectrum all we need is compute its TIC value, that
          // is, the sum of all its intensities (the y value of the DataPoint
          // instances it contains).

          pappso::QualifiedMassSpectrumCstSPtr qualified_mass_spectrum_csp =
            *current_iterator;

          // Ensure that the qualified mass spectrum contains a mass spectrum
          // that effectively contains the binary (mz,i) data.

          qualified_mass_spectrum_csp =
            checkQualifiedMassSpectrum(qualified_mass_spectrum_csp);

          // Great, at this point we actually have the mass spectrum!

          // We now need to ensure that the processing flow allows for this mass
          // spectrum to be combined.

          // Immediately check if the qualified mass spectrum is of a MS level
          // that matches the greatest MS level found in the member processing
          // flow instance.

          if(!checkMsLevel(qualified_mass_spectrum_csp) ||
             !checkRtRange(qualified_mass_spectrum_csp) ||
             !checkDtRange(qualified_mass_spectrum_csp))
            {
              // if(!checkMsLevel(qualified_mass_spectrum_csp))
              // qDebug() << "MS level did not check.";

              // if(!checkRtRange(qualified_mass_spectrum_csp))
              // qDebug() << "RT range did not check.";

              // if(!checkDtRange(qualified_mass_spectrum_csp))
              // qDebug() << "DT range did not check.";

              // qDebug() << "The checks returned false";

              ++current_iterator;
              continue;
            }

          // At this point we need to go deep in the processing flow's steps to
          // check if the node matches the whole set of the steps' specs.

          // qDebug() << "The processing flow has" << m_processingFlow.size() <<
          // "steps.";

          for(auto &&step : m_processingFlow)
            {
              if(!checkProcessingStep(*step, qualified_mass_spectrum_csp))
                {
                  ++current_iterator;
                  continue;
                }
            }

          // At this point we need to go deep in the processing flow's steps to
          // check if the node matches the whole set of the steps' specs.

          // qDebug() << "The processing flow has" << m_processingFlow.size() <<
          // "steps.";

          for(auto &&step : m_processingFlow)
            {
              if(!checkProcessingStep(*step, qualified_mass_spectrum_csp))
                {
                  ++current_iterator;
                  continue;
                }
            }

          double sumY = 0;

          // These values are set in the checkProcessingSpec() of the base
          // class, more specifically in checkProcessingTypeByMz().

          if(m_limitMzRangeStart != std::numeric_limits<double>::max() &&
             m_limitMzRangeEnd != std::numeric_limits<double>::max())
            {
              // qDebug() << "Limiting the mz range to" << m_limitMzRangeStart
              //<< "->" << m_limitMzRangeEnd;

              sumY = qualified_mass_spectrum_csp->getMassSpectrumSPtr()->sumY(
                m_limitMzRangeStart, m_limitMzRangeEnd);
            }
          else
            {
              sumY = qualified_mass_spectrum_csp->getMassSpectrumSPtr()->sumY();
              // qDebug() << "sumY:" << sumY;
            }

          if(!sumY)
            {
              // qDebug() << "sumY is 0.";

              ++current_iterator;
              continue;
            }

          double dt = qualified_mass_spectrum_csp->getDtInMilliSeconds();

          // Try to insert the pair into the map. Check if that was done or not.
          std::pair<DtTicMapIterator, bool> res =
            current_dt_spec_map_sp->insert(Pair(dt, sumY));

          if(!res.second)
            {
              // One other same rt value was seen already (like in ion mobility
              // mass spectrometry, for example). Only increment the y value.

              res.first->second += sumY;
            }

          // qDebug() << "Processed one more spectrum.";

          emit incrementProgressBarCurrentValueAndSetStatusTextSignal(
            1, "Processed spectrum");

          // Now we can go to the next mass spectrum.
          ++current_iterator;
        }
        // End of
        // while(current_iterator != end_iterator)

#if 0
			qDebug() << "At the end of the current iterator pair, current map:";
			for(auto &&pair : *current_dt_spec_map_sp)
				qDebug().noquote() << "(" << pair.first << "," << pair.second << ")";
#endif
    }
  // End of
  // #pragma omp parallel for ordered
  // for(std::size_t iter = 0; iter < iterators.size(); ++iter)

  // In the loop above, for each thread, a std::map<double, double> map was
  // filled in with the rt, TIC pairs.

  // If the task was cancelled, the monitor widget was locked. We need to
  // unlock it.
  emit unlockTaskMonitorCompositeWidgetSignal();
  emit setupProgressBarSignal(0, vector_of_dt_spec_map.size() - 1);

  // We might be here because the user cancelled the operation in the for loop
  // above (visiting all the visitors). In this case m_isOperationCancelled is
  // true. We want to set it back to false, so that the following loop is gone
  // through. The user can ask that the operation be cancelled once more. But
  // we want that at least the performed work be used to show the trace.

  m_isOperationCancelled = false;
  int iter               = 0;

  for(auto &&map_sp : vector_of_dt_spec_map)
    {
      // qDebug();

      ++iter;

      if(m_isOperationCancelled)
        break;

      // qDebug() << "In the consolidating loop, iter:" << iter
      //<< "with a map of this size : " << map_sp->size();

      for(auto &&pair : *map_sp)
        {

          emit setStatusTextSignal(
            QString("Consolidating TIC chromatograms from thread %1")
              .arg(iter));

          emit setProgressBarCurrentValueSignal(iter);

          double rt  = pair.first;
          double tic = pair.second;

          std::pair<DtTicMapIterator, bool> res =
            m_mapTrace.insert(Pair(rt, tic));

          if(!res.second)
            {
              // One other same rt value was seen already (like in ion mobility
              // mass spectrometry, for example). Only increment the y value.

              res.first->second += tic;
            }
        }
    }

  // Write the data to file so that we can check. It works !!!
  // pappso::Utils::appendToFile(m_mapTrace.toString(), "/tmp/prova.txt");

  std::chrono::system_clock::time_point chrono_end_time =
    std::chrono::system_clock::now();

  QString chrono_string = pappso::Utils::chronoIntervalDebugString(
    "Integration to drift spectrum took:", chrono_start_time, chrono_end_time);

  emit logTextToConsoleSignal(chrono_string);

  // qDebug().noquote() << chrono_string;
}


} // namespace minexpert

} // namespace msxps
