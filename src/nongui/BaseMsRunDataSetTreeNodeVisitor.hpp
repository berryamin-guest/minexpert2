/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

#pragma once

/////////////////////// StdLib includes


/////////////////////// Qt includes


/////////////////////// pappsomspp includes
#include <pappsomspp/massspectrum/qualifiedmassspectrum.h>
#include <pappsomspp/msrun/msrundatasettreenode.h>
#include <pappsomspp/msrun/msrundatasettreevisitor.h>


/////////////////////// Local includes
#include "ProcessingFlow.hpp"
#include "MsRunDataSet.hpp"


namespace msxps
{
namespace minexpert
{


class BaseMsRunDataSetTreeNodeVisitor;

class MassDataIntegrator;

typedef std::shared_ptr<BaseMsRunDataSetTreeNodeVisitor>
  BaseMsRunDataSetTreeNodeVisitorSPtr;
typedef std::shared_ptr<const BaseMsRunDataSetTreeNodeVisitor>
  BaseMsRunDataSetTreeNodeVisitorCstSPtr;

class BaseMsRunDataSetTreeNodeVisitor
  : public QObject,
    public pappso::MsRunDataSetTreeNodeVisitorInterface
{
  Q_OBJECT;

  friend class MassDataIntegrator;

  public:
  BaseMsRunDataSetTreeNodeVisitor(MsRunDataSetCstSPtr ms_run_data_set_csp,
                                  const ProcessingFlow &processing_flow);

  BaseMsRunDataSetTreeNodeVisitor(const BaseMsRunDataSetTreeNodeVisitor &other);

  virtual ~BaseMsRunDataSetTreeNodeVisitor();

  void setProcessingFlow(const ProcessingFlow &processing_flow);

  void setLimitMzRangeStart(double limit_mz_range_start);
  double getLimitMzRangeStart() const;

  void setLimitMzRangeEnd(double limit_mz_range_end);
  double getLimitMzRangeEnd() const;

  BaseMsRunDataSetTreeNodeVisitor &
  operator=(const BaseMsRunDataSetTreeNodeVisitor &other);

  virtual pappso::QualifiedMassSpectrumCstSPtr
  checkQualifiedMassSpectrum(const pappso::MsRunDataSetTreeNode &node);

  virtual bool checkMsLevel(const pappso::MsRunDataSetTreeNode &node);

  // Pure virtual functions
  virtual bool
  checkProcessingStep(const ProcessingStep &step,
                      const pappso::MsRunDataSetTreeNode &node) = 0;

  virtual bool checkProcessingSpec(
    const std::pair<ProcessingType, ProcessingSpec *> &type_spec_pair,
    const pappso::MsRunDataSetTreeNode &node) = 0;

  virtual bool checkProcessingTypeByRt(
    const std::pair<ProcessingType, ProcessingSpec *> &spec_type_pair,
    const pappso::MsRunDataSetTreeNode &node) = 0;

  virtual bool checkProcessingTypeByMz(
    const std::pair<ProcessingType, ProcessingSpec *> &spec_type_pair,
    const pappso::MsRunDataSetTreeNode &node) = 0;

  virtual bool checkProcessingTypeByDt(
    const std::pair<ProcessingType, ProcessingSpec *> &spec_type_pair,
    const pappso::MsRunDataSetTreeNode &node) = 0;

  virtual void setNodesToProcessCount(std::size_t nodes_to_process);
  virtual bool shouldStop() const;
  virtual void silenceFeedback(bool silence_feedback = false);
  virtual bool shouldSilenceFeedback();

  public slots:
  virtual void cancelOperation();

  signals:
  void cancelOperationSignal();

  void setProgressBarMaxValueSignal(std::size_t nodes_to_process);
  void incrementProgressBarMaxValueSignal(std::size_t increment);
  void setProgressBarCurrentValueSignal(std::size_t processed_nodes);
  void
  incrementProgressBarCurrentValueAndSetStatusTextSignal(std::size_t increment,
                                                         QString text);

  void setStatusTextSignal(QString text);

  void setStatusTextAndCurrentValueSignal(QString text, std::size_t value);
  void setStatusTextAndIncrementCurrentValueSignal(QString text,
                                                   std::size_t increment);

  protected:
  MsRunDataSetCstSPtr mcsp_msRunDataSet;
  ProcessingFlow m_processingFlow;
  bool m_isOperationCancelled       = false;
  bool m_isProgressFeedbackSilenced = false;

  // We almost systematically need these m/z range values when making visits:
  // one criterium might be that the m/z value of data points (the x member of
  // DataPoint) be contained within the limit m/z range.
  double m_limitMzRangeStart = std::numeric_limits<double>::max();
  double m_limitMzRangeEnd   = std::numeric_limits<double>::max();
};


} // namespace minexpert

} // namespace msxps
