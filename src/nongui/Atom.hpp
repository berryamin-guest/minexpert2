/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QString>
#include <QList>


/////////////////////// Local includes
#include "Isotope.hpp"
#include "Ponderable.hpp"


namespace msxps
{
namespace minexpert
{


//! The Atom class provides an atom.
/*! An atom(the object that materializes a chemical element) is a chemical
 * entity that bears the following characteristics:
 *
 - a name(like Carbon);

 - a symbol(like C);

 - a monoisotopic mass that is the mass of the lightest isotope (see below).
 For the chemical elements found in biological matter, the lightest isotopes
 are also the most abundant ones;

 - an average mass that is the average mass of all the isotopes weighed by
 their respective abundance;

 - the list of all the isotopes that are comprised by the chemical
 element(for Carbon, there are two main isotopes \f$\mathrm{^{12}C}\f$ and
 \f$\mathrm{^{13}C}\f$.
 */
class Atom : public Ponderable
{
  protected:
  //! Name.
  QString m_name;

  //! Symbol
  /*!
   *
   * The symbol may not contain more than one uppercase character. The first
   * character must be uppercase, and the remaining ones lowercase. Note
   * that unlike other software, the number of characters allowed for a
   * given symbol is not restricted to two letters. An atom symbol cannot be
   * more than 3 letters-long.
   *
   * \sa validate()
   */
  QString m_symbol;

  //! List of the isotopes making up the atom.
  /*!
   *
   * This list is populated with dynamically allocated instances of the
   * Isotope type.
   */
  QList<Isotope *> m_isotopeList;

  public:
  Atom(const QString &name = QString(), const QString &symbol = QString());
  Atom(const Atom &other);
  virtual ~Atom();

  Atom *clone() const;

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Woverloaded-virtual"
  void clone(Atom *) const;
  void mold(const Atom &);
#pragma clang diagnostic pop

  virtual Atom &operator=(const Atom &);

  void clear();

  const QList<Isotope *> &isotopeList() const;

  void appendIsotope(Isotope *);
  void removeIsotopeAt(int);
  void insertIsotopeAt(int, Isotope *);

  void setName(const QString &);
  QString name() const;

  void setSymbol(const QString &);
  QString symbol() const;

  bool calculateMasses();
  double calculateMono();
  double calculateAvg();

  virtual bool accountMasses(const QList<Atom *> &,
                             double * = Q_NULLPTR,
                             double * = Q_NULLPTR,
                             int      = 1) const;

  virtual bool
  accountMasses(double * = Q_NULLPTR, double * = Q_NULLPTR, int = 1) const;

  int symbolIndex(const QList<Atom *> &refList) const;
  static int symbolIndex(const QString &str,
                         const QList<Atom *> &refList,
                         Atom * = Q_NULLPTR);

  int nameIndex(const QList<Atom *> &refList) const;
  static int nameIndex(const QString &str,
                       const QList<Atom *> &refList,
                       Atom *other = Q_NULLPTR);

  bool validate();

  bool renderXmlAtomElement(const QDomElement &element, int version);

  QString *formatXmlAtomElement(int offset,
                                const QString &index = QString(" "));

  QString asText();
};

} // namespace minexpert

} // namespace msxps

