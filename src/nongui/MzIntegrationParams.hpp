/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


#include <unordered_map>

#include <pappsomspp/precision.h>
#include <pappsomspp/processing/filters/savgolfilter.h>
#include <pappsomspp/massspectrum/massspectrum.h>

namespace msxps
{
namespace minexpert
{

//! Type of binning when performing integrations to a mass spectrum
enum class BinningType
{
  //! < no binning
  NONE = 0,

  //! binning based on mass spectral data
  DATA_BASED,

  //! binning based on arbitrary bin size value
  ARBITRARY,

  LAST,
};

extern std::unordered_map<BinningType, QString> binningTypeMap;


//! The MzIntegrationParams class provides the parameters definining how m/z !
// integrations must be performed.
/*!
 * Depending on the various mass spectrometer vendors, the mass spectrometry
 * data files are structured in different ways and the software for mass data
 * format conversion from raw files to mzML or mzXML produce mass data
 * characterized by different behaviours.
 *
 * The different characteristics of mass spectrometry data set are:
 *
 * The size of the various mass spectra in the file is constant or variable;
 *
 * The first m/z value of the various spectra is identical or not (that is,
 * the spectra are root in a constant or variable root m/z value);
 *
 * The m/z delta between two consecutive m/z values of a given spectrum are
 * constant or variable;
 *
 * The spectra contain or not 0-value m/z data points;

*/
class MzIntegrationParams
{
  friend class MzIntegrationParamsDlg;


  public:
  MzIntegrationParams();
  MzIntegrationParams(pappso::pappso_double minMz,
                      pappso::pappso_double maxMz,
                      BinningType binningType,
                      int decimalPlaces,
                      pappso::PrecisionPtr precisionPtr,
                      bool applyMzShift,
                      pappso::pappso_double mzShift,
                      bool removeZeroValDataPoints);

  MzIntegrationParams(const MzIntegrationParams &other);
  MzIntegrationParams(const pappso::SavGolParams &savGolParams);

  virtual ~MzIntegrationParams();

  MzIntegrationParams &operator=(const MzIntegrationParams &other);

  void setSmallestMz(pappso::pappso_double value);
  void updateSmallestMz(pappso::pappso_double value);
  pappso::pappso_double getSmallestMz() const;

  void setGreatestMz(pappso::pappso_double value);
  void updateGreatestMz(pappso::pappso_double value);
  pappso::pappso_double getGreatestMz() const;

  void setBinningType(BinningType binningType);
  BinningType getBinningType() const;

  void setDecimalPlaces(int decimal_places);
  int getDecimalPlaces() const;

  void setPrecision(pappso::PrecisionPtr precisionPtr);
  pappso::PrecisionPtr getPrecision() const;

  void setApplyMzShift(bool applyMzShift);
  bool isApplyMzShift() const;

  void setRemoveZeroValDataPoints(bool removeOrNot = true);
  bool isRemoveZeroValDataPoints() const;

  void setApplySavGolFilter(bool applySavGolFilter);
  bool isApplySavGolFilter() const;

  void setSavGolParams(int nL              = 15,
                       int nR              = 15,
                       int m               = 4,
                       int lD              = 0,
                       bool convolveWithNr = false);
  void setSavGolParams(const pappso::SavGolParams &params);
  pappso::SavGolParams getSavGolParams() const;

  void reset();

  bool isValid() const;

  bool hasValidMzBinRange() const;

  std::vector<pappso::pappso_double> createBins();
  std::vector<pappso::pappso_double>
  createBins(pappso::MassSpectrumCstSPtr mass_spectrum_csp);

  QString toString(int offset = 0, const QString &spacer = QString()) const;

  private:
  // That smallest value needs to be set to max, because it will be necessary
  // compare any new m/z valut to it.
  pappso::pappso_double m_smallestMzBin = std::numeric_limits<double>::max();

  // That greatest value needs to be set to min, because it will be necessary
  // compare any new m/z valut to it.
  pappso::pappso_double m_greatestMzBin = std::numeric_limits<double>::min();

  BinningType m_binningType = BinningType::NONE;

  int m_decimalPlaces = -1;

  // This should actually be called "bin size" as it describes the width of
  // the bins.
  pappso::PrecisionPtr mp_precision =
    pappso::PrecisionFactory::getDaltonInstance(0.05);
  bool m_applyMzShift             = false;
  pappso::pappso_double m_mzShift = 0;
  bool m_removeZeroValDataPoints  = true;

  bool m_applySavGolFilter = false;
  pappso::SavGolParams m_savGolParams;

  std::vector<double> createArbitraryBins();
  std::vector<double>
  createDataBasedBins(pappso::MassSpectrumCstSPtr massSpectrum);
};


} // namespace minexpert
} // namespace msxps
