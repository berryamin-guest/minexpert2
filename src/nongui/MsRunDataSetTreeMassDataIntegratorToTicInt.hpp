/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes
#include <vector>


/////////////////////// Qt includes


/////////////////////// pappsomspp includes
#include <pappsomspp/trace/maptrace.h>


/////////////////////// Local includes
#include "globals.hpp"
#include "MsRunDataSet.hpp"
#include "ProcessingStep.hpp"
#include "ProcessingFlow.hpp"
#include "MassDataIntegrator.hpp"


namespace msxps
{
namespace minexpert
{

class MsRunDataSetTreeMassDataIntegratorToTicInt;

typedef std::shared_ptr<MsRunDataSetTreeMassDataIntegratorToTicInt> MsRunDataSetTreeMassDataIntegratorToTicIntSPtr;

class MsRunDataSetTreeMassDataIntegratorToTicInt : public MassDataIntegrator
{
  Q_OBJECT;

  public:
  MsRunDataSetTreeMassDataIntegratorToTicInt();

  MsRunDataSetTreeMassDataIntegratorToTicInt(MsRunDataSetCstSPtr ms_run_data_set_csp);

  MsRunDataSetTreeMassDataIntegratorToTicInt(MsRunDataSetCstSPtr ms_run_data_set_csp,
                     const ProcessingFlow &processing_flow);

  MsRunDataSetTreeMassDataIntegratorToTicInt(const MsRunDataSetTreeMassDataIntegratorToTicInt &other);

  virtual ~MsRunDataSetTreeMassDataIntegratorToTicInt();

  const ProcessingFlow &getProcessingFlow() const;
  void appendProcessingStep(ProcessingStep *processing_step_p);

  double getTicIntensity() const;

  public slots:

  bool integrateToTicIntensity();

  signals:

  protected:
  ProcessingFlow m_processingFlow;

  // Initialize the tic intensity to max() so that we know if a computation was
  // carried-over or not.
  double m_ticIntensity = std::numeric_limits<double>::max();
};

} // namespace minexpert

} // namespace msxps

Q_DECLARE_METATYPE(msxps::minexpert::MsRunDataSetTreeMassDataIntegratorToTicInt);
extern int msRunDataSetTreeMassDataIntegratorToTicIntMetaTypeId;

Q_DECLARE_METATYPE(msxps::minexpert::MsRunDataSetTreeMassDataIntegratorToTicIntSPtr);
extern int msRunDataSetTreeMassDataIntegratorToTicIntSPtrMetaTypeId;
