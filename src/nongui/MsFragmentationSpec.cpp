/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QDebug>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "MsFragmentationSpec.hpp"


namespace msxps
{
namespace minexpert
{


MsFragmentationSpec::MsFragmentationSpec()
{
}


MsFragmentationSpec::MsFragmentationSpec(
  std::size_t ms_level,
  const std::vector<double> &precursors,
  std::vector<std::size_t> precursor_spectrum_indices)
  : m_msLevel(ms_level),
    m_precursors(precursors),
    m_precursorSpectrumIndices(precursor_spectrum_indices)
{
}


MsFragmentationSpec::MsFragmentationSpec(const MsFragmentationSpec &other)
{
  m_msLevel                  = other.m_msLevel;
  mp_precision               = other.mp_precision;
  m_precursors               = other.m_precursors;
  m_precursorSpectrumIndices = other.m_precursorSpectrumIndices;
}


MsFragmentationSpec::~MsFragmentationSpec()
{
}


MsFragmentationSpec &
MsFragmentationSpec::operator=(const MsFragmentationSpec &other)
{
  if(this == &other)
    return *this;

  m_msLevel                  = other.m_msLevel;
  mp_precision               = other.mp_precision;
  m_precursors               = other.m_precursors;
  m_precursorSpectrumIndices = other.m_precursorSpectrumIndices;

  return *this;
}


void
MsFragmentationSpec::setMsLevel(std::size_t ms_level)
{
  m_msLevel = ms_level;
}


std::size_t
MsFragmentationSpec::getMsLevel() const
{
  return m_msLevel;
}


void
MsFragmentationSpec::setPrecisionPtr(pappso::PrecisionPtr precision_p)
{
  mp_precision = precision_p;
}


pappso::PrecisionPtr
MsFragmentationSpec::getPrecisionPtr() const
{
  return mp_precision;
}


bool
MsFragmentationSpec::containsMzPrecursor(double searched_mz_value) const
{
  // We cannot hope to have the user enter the m/z value with all the decimals
  // right (because they do not even know them by looking a the data !) So we
  // let the user set a precsion with which the match needs to be done.

  // qDebug() << "Searching for mz value:" << searched_mz_value;

  auto result =
    std::find_if(m_precursors.begin(),
                 m_precursors.end(),
                 [this, searched_mz_value](double value) {
                   double tolerance = mp_precision->delta(searched_mz_value);
                   bool result = (value >= searched_mz_value - tolerance) &&
                                 (value <= searched_mz_value + tolerance);

                   // qDebug() << "Match with value:" << value;

                   return result;
                 });

  if(result != m_precursors.end())
    return true;

  return false;
}


bool
MsFragmentationSpec::containsSpectrumPrecursorIndex(
  std::size_t searched_precursor_index) const
{
  auto result = std::find_if(m_precursorSpectrumIndices.begin(),
                             m_precursorSpectrumIndices.end(),
                             [searched_precursor_index](double value) {
                               return value == searched_precursor_index;
                             });

  if(result != m_precursorSpectrumIndices.end())
    return true;

  return false;
}


std::size_t
MsFragmentationSpec::precursorsCount() const
{
  return m_precursors.size();
}


std::size_t
MsFragmentationSpec::precursorSpectrumIndicesCount() const
{
  return m_precursorSpectrumIndices.size();
}


bool
MsFragmentationSpec::isValid() const
{
  // The fragmentation spec is valid if there is either one of the following:

  return m_msLevel || m_precursors.size() || m_precursorSpectrumIndices.size();
}


QString
MsFragmentationSpec::msLevelToString() const
{
  return QString("%1 ").arg(m_msLevel);
}


QString
MsFragmentationSpec::mzPrecursorsToString() const
{
  QString text;

  for(auto &&precursor_mz : m_precursors)
    text += QString("%1\n").arg(precursor_mz, 0, 'f', 4);

  return text;
}


QString
MsFragmentationSpec::precursorSpectrumIndicesToString() const
{
  QString text;

  for(auto &&precursor_spectrum_index : m_precursorSpectrumIndices)
    text += QString("%1\n").arg(precursor_spectrum_index);

  return text;
}


QString
MsFragmentationSpec::toString(int offset, const QString &spacer) const
{
  QString lead;

  for(int iter = 0; iter < offset; ++iter)
    lead += spacer;

  QString text = lead;

  text += "MS fragmentation spec:\n";

  text += lead;
  text += spacer;
  text += "MS level: ";
  text += msLevelToString();
  text += "\n";

  if(m_precursors.size())
    {
      text += lead;
      text += spacer;
      text += "Precursors' m/z: ";
      text += mzPrecursorsToString();
      text += "\n";

      if(mp_precision == nullptr)
        {
          qFatal("Cannot be that precision ptr is nullptr");
        }

      // Indent one space more for the precision
      text += lead;
      text += spacer;
      text += spacer;
      text += QString("m/z value precision: %1").arg(mp_precision->toString());
      text += "\n";
    }

  if(m_precursorSpectrumIndices.size())
    {
      text += lead;
      text += spacer;
      text += "Precursor spectrum indices: ";
      text += precursorSpectrumIndicesToString();
      text += "\n";
    }

  return text;
}


} // namespace minexpert

} // namespace msxps
