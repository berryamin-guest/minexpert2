/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// Qt includes
///////////////////////
#include <QtGlobal>
#include <QtXml>

#include "globals.hpp"


namespace msxps
{
namespace minexpert
{


//! The Isotope class provides an isotope entity.
/*!

  Isotopes are the constitutive elements of atoms (the materializing entities
  of chemical elements). An isotope is characterized by two data:

  - the isotope mass, initialized to 0 upon default-construction;

  - the isotope abundance, initialized to 0 upon default-construction;.

  The carbon atoms (symbol 'C'), for example, are mainly found in nature in
  the form of two isotopes: \f$\mathrm{^{12}C}\f$ and \f$\mathrm{^{13}C}\f$ .
  The \f$\mathrm{^{12}C}\f$ has an abundance of 0.99 (that is, 99 percent)
  while the heavier isotope has an abundance of 0.01 (that is, 1 percent).

  Consequently, the main member data of this class are the mass value
  and the abundance value.

*/
class Isotope
{
  protected:
  //! Mass.
  double m_mass = 0;

  //! Abundance.
  double m_abundance = 0;

  public:
  Isotope();
  Isotope(double mass, double abundance);
  Isotope(const Isotope &other);
  virtual ~Isotope();

  virtual Isotope *clone() const;
  virtual void clone(Isotope *other) const;
  virtual void mold(const Isotope &other);
  virtual Isotope &operator=(const Isotope &other);

  void setMass(double);
  double mass() const;
  QString massString(int decimalPlaces = ATOM_DEC_PLACES) const;

  void setAbundance(double abundance);
  void incrementAbundance(double abundance);
  void decrementAbundance(double abundance);
  double abundance() const;
  QString abundanceString(int decimalPlaces = ATOM_DEC_PLACES) const;

  virtual bool operator==(const Isotope &) const;
  virtual bool operator!=(const Isotope &) const;
  virtual bool operator<(const Isotope &) const;
  static bool lessThan(const Isotope *first, const Isotope *second);

  bool renderXmlIsotopeElement(const QDomElement &element, int version);
  QString *formatXmlIsotopeElement(int, const QString & = QString(" "));

  QString asText() const;
};

} // namespace minexpert

} // namespace msxps

