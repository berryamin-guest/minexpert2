/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QObject>
#include <QMainWindow>
#include <QStringList>
#include <QHash>
#include <QMap>


/////////////////////// pappsomspp includes
#include <pappsomspp/msrun/msrunreader.h>
#include <pappsomspp/processing/combiners/massspectrumpluscombiner.h>


/////////////////////// Local includes
#include "MsRunDataSet.hpp"
#include "MsRunDataSetStats.hpp"


namespace msxps
{
namespace minexpert
{

class MassSpecDataFileLoader;

typedef std::shared_ptr<MassSpecDataFileLoader> MassSpecDataFileLoaderSPtr;
typedef std::shared_ptr<const MassSpecDataFileLoader>
  MassSpecDataFileLoaderCstSPtr;


//! The MassSpecDataFileLoader provides a mass spectrometry data file loader.
/*!

  The MassSpecDataFileLoader implements a base class for specialized mass
  spectrometry data file loaders.

*/
class MassSpecDataFileLoader : public QObject,
                               public pappso::SpectrumCollectionHandlerInterface

{

  Q_OBJECT

  public:
  MassSpecDataFileLoader(const QString &fileName = QString(), bool load_fully_in_memory = false);
  virtual ~MassSpecDataFileLoader();

  void setMsRunDataSet(MsRunDataSetSPtr &ms_run_data_set_sp);
  MsRunDataSetSPtr getMsRunDataSet();

  void setFileName(const QString &fileName);

  virtual bool shouldStop() override;

  virtual void spectrumListHasSize(std::size_t size) override;

  // This is the call back function that provides spectrum after spectrum the
  // full spectrum collection from the file (pappso::MsRunReader).
  virtual void setQualifiedMassSpectrum(
    const pappso::QualifiedMassSpectrum &qualified_mass_spectrum) override;

  // Special function to a mass spectrum from a string (typically the
  // clipboard).
  int loadDataFromString(const QString &msDataString,
                         MsRunDataSet *msRunDataSet);

  // Returns always true in our implementation because we always need the data
  // even in streamed mode (we free the data once the TIC and COLORMAP stuff
  // have been computed).
  virtual bool needPeakList() const override;

  // This function call back is kind of a signal that the spectrum collection
  // reading process has ended.
  virtual void loadingEnded() override;


  public slots:
  void cancelOperation();

  signals:

  void setupProgressBarSignal(std::size_t min_value, std::size_t max_value);
  void setProgressBarMinValueSignal(std::size_t value);
  // connected to setProgressBarMaxValue()
  void spectrumListSizeSignal(std::size_t size);
  void setProgressBarMaxValueSignal(std::size_t value);
  void setProgressBarCurrentValueSignal(std::size_t size);

  void setStatusTextSignal(QString text);
  void appendStatusTextSignal(QString text);
  void setStatusTextAndCurrentValueSignal(QString text, std::size_t value);

  void lockTaskMonitorCompositeWidgetSignal();
  void unlockTaskMonitorCompositeWidgetSignal();

  protected:
  //! Name of the mass data file from which to load mass spectrometry data.
  QString m_fileName = "NOT_SET";

	bool m_isLoadFullInMemory = false;

  //! Pointer to a MsRunDataSet instance.
  /*!

    When loading mass data for later analysis in mineXpert, the loaded data
    are typically stored in this MsRunDataSet.

    Note that the data in \c mp_msRunDataSet are \e not owned by \c this
    MassSpecDataFileLoader instance.

*/
  MsRunDataSetSPtr msp_msRunDataSet = nullptr;

  // This switch is useful for the owner of an instance of this class to
  // stop the current operation.
  bool m_isOperationCancelled = false;

  // Number of spectra in the spectrum list size (the pwiz msrun reader will
  // call the pappso::SpectrumCollectionHandlerInterface::spectrumListHasSize()
  // function to inform us on that size.).
  std::size_t m_spectrumListSize = 0;

  // Value that counts the number of times a new mass spectrum has been set.
  std::size_t m_setQualifiedMassSpectrumCount = 0;
};

} // namespace minexpert
} // namespace msxps


Q_DECLARE_METATYPE(msxps::minexpert::MassSpecDataFileLoaderSPtr);
extern int massSpecDataFileLoaderSPtrMetaTypeId;
