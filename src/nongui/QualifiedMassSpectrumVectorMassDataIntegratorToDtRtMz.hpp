/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes
#include <vector>


/////////////////////// Qt includes


/////////////////////// pappsomspp includes
#include <pappsomspp/trace/maptrace.h>


/////////////////////// Local includes
#include "ProcessingStep.hpp"
#include "ProcessingFlow.hpp"
#include "QualifiedMassSpectrumVectorMassDataIntegrator.hpp"


namespace msxps
{
namespace minexpert
{

class QualifiedMassSpectrumVectorMassDataIntegratorToDtRtMz;

typedef std::shared_ptr<QualifiedMassSpectrumVectorMassDataIntegratorToDtRtMz>
  QualifiedMassSpectrumVectorMassDataIntegratorToDtRtMzSPtr;

typedef std::shared_ptr<
  const QualifiedMassSpectrumVectorMassDataIntegratorToDtRtMz>
  QualifiedMassSpectrumVectorMassDataIntegratorToDtRtMzCstSPtr;

class QualifiedMassSpectrumVectorMassDataIntegratorToDtRtMz
  : public QualifiedMassSpectrumVectorMassDataIntegrator
{
  Q_OBJECT;

  public:
  QualifiedMassSpectrumVectorMassDataIntegratorToDtRtMz();

  QualifiedMassSpectrumVectorMassDataIntegratorToDtRtMz(
    MsRunDataSetCstSPtr ms_run_data_set_csp,
    const ProcessingFlow &processing_flow,
    QualifiedMassSpectraVectorSPtr &qualified_mass_spectra_to_integrate_sp);

  QualifiedMassSpectrumVectorMassDataIntegratorToDtRtMz(
    const QualifiedMassSpectrumVectorMassDataIntegratorToDtRtMz &other);

  virtual ~QualifiedMassSpectrumVectorMassDataIntegratorToDtRtMz();

  pappso::DataKind getLastIntegrationDataKind() const;

  std::size_t getColorMapKeyCellCount() const;
  std::size_t getColorMapMzCellCount() const;

  double getColorMapMinKey() const;
  double getColorMapMaxKey() const;

  double getColorMapMinMz() const;
  double getColorMapMaxMz() const;

  using Map = std::map<double, pappso::MapTrace>;
  const std::shared_ptr<Map> & getDoubleMapTraceMapSPtr() const;


  public slots:

  void integrateToDtRtMz(pappso::DataKind data_kind);

  signals:

  protected:
  std::size_t m_colorMapKeyCellCount = 0;
  std::size_t m_colorMapMzCellCount  = 0;

  double m_colorMapMinKey = std::numeric_limits<double>::max();
  double m_colorMapMaxKey = std::numeric_limits<double>::min();

  double m_colorMapMinMz = std::numeric_limits<double>::max();
  double m_colorMapMaxMz = std::numeric_limits<double>::min();

  std::shared_ptr<Map> m_doubleMapTraceMapSPtr = nullptr;

  pappso::DataKind m_lastIntegratinDataKind;
};

} // namespace minexpert

} // namespace msxps


Q_DECLARE_METATYPE(
  msxps::minexpert::QualifiedMassSpectrumVectorMassDataIntegratorToDtRtMz);
extern int qualifiedMassSpectrumVectorMassDataIntegratorToDtRtMzMetaTypeId;

Q_DECLARE_METATYPE(
  msxps::minexpert::QualifiedMassSpectrumVectorMassDataIntegratorToDtRtMzSPtr);
extern int qualifiedMassSpectrumVectorMassDataIntegratorToDtRtMzSPtrMetaTypeId;

