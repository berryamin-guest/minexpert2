/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes
#include <map>
#include <vector>


/////////////////////// Qt includes
#include <QString>
#include <QDateTime>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "ProcessingType.hpp"
#include "ProcessingSpec.hpp"


namespace msxps
{
namespace minexpert
{

struct ProcessingTypeCompare
{
  bool
  operator()(const ProcessingType &a, const ProcessingType &b) const
  {
    return a.bitSet().to_string() < b.bitSet().to_string();
  }
};

typedef std::pair<ProcessingType, ProcessingSpec *> ProcessingTypeSpecPtrPair;


class ProcessingStep
{

  friend class ProcessingFlow;

  public:
  ProcessingStep();
  ProcessingStep(const ProcessingStep &other);

  virtual ~ProcessingStep();

  ProcessingStep &operator=(const ProcessingStep &other);

  void setMzIntegrationParams(const MzIntegrationParams &mz_integration_params);
  const MzIntegrationParams *getMzIntegrationParamsPtr() const;

  void newSpec(ProcessingType processing_type,
               ProcessingSpec *processing_spec_p);

  ProcessingSpec *newSpec(ProcessingType processing_type,
                          const ProcessingSpec &processing_spec);

  void newSpec(std::bitset<64> bit_set, ProcessingSpec *processing_spec_p);

  void newSpec(const QString &brief_desc, ProcessingSpec *processing_spec_p);

  std::vector<ProcessingType> processingTypes() const;

  const std::multimap<ProcessingType, ProcessingSpec *, ProcessingTypeCompare> &
  getProcessingTypeSpecMap() const;

  bool matches(std::bitset<64> bit_set) const;
  bool matches(ProcessingType type) const;
  bool matches(const QString &brief_desc) const;
  bool contains(const QString &brief_desc) const;

  std::vector<ProcessingSpec *> allProcessingSpecsMatching(
    const ProcessingType &processingType,
    bool only_with_valid_ms_fragmentation_spec = false) const;

	size_t greatestMsLevel() const;

  QString toString(int offset = 0, const QString &spacer = QString()) const;

  protected:
  std::multimap<ProcessingType, ProcessingSpec *, ProcessingTypeCompare>
    m_processingTypeSpecMultiMap;

  QDateTime m_dateAndTime;

  // When integrating to m/z spectra we need to configure the integration. This
  // class instance allows to do so. Because not all the specs need such
  // parameters, we do not make a member variable of that type, but we provide a
  // means to store a pointer.
  MzIntegrationParams *mpa_mzIntegrationParams = nullptr;
};


} // namespace minexpert

} // namespace msxps
