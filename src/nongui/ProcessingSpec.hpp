/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes
#include <map>
#include <vector>
#include <memory>


/////////////////////// Qt includes
#include <QString>
#include <QDateTime>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "MsFragmentationSpec.hpp"
#include "MzIntegrationParams.hpp"
#include "ProcessingType.hpp"


namespace msxps
{
namespace minexpert
{


class ProcessingSpec
{
  friend class ProcessingStep;
  friend class ProcessingFlow;

  public:
  ProcessingSpec();
  ProcessingSpec(const ProcessingSpec &other);
  ProcessingSpec(double start_value, double end_value);
  ProcessingSpec(const MsFragmentationSpec &ms_fragmentation_spec);
  ProcessingSpec(const MsFragmentationSpec &ms_fragmentation_spec,
                 double start_value,
                 double end_value);
  virtual ~ProcessingSpec();

  ProcessingSpec &operator=(const ProcessingSpec &other);

  void setMsFragmentationSpec(const MsFragmentationSpec &fragmentation_spec);
  MsFragmentationSpec getMsFragmentationSpec() const;
  MsFragmentationSpec *getMsFragmentationSpecPtr();

  double getStart() const;
  double getEnd() const;
  void setRange(double start, double end);
  void resetRange();
  bool hasValidRange() const;

  bool hasValidFragmentationSpec() const;

  QString toString(
    int offset                            = 0,
    const QString &spacer                 = QString(),
    const ProcessingType &processing_type = ProcessingType("NOT_SET")) const;

  protected:
  // The ms fragmentation pattern is optional here. If non-nullptr it is
  // considered to be enforceable.
  MsFragmentationSpec *mpa_msFragmentationSpec = nullptr;

  double m_start = std::numeric_limits<double>::max();
  double m_end   = std::numeric_limits<double>::min();
};


} // namespace minexpert

} // namespace msxps
