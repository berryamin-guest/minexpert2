/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// Qt includes
#include <QString>
#include <QHash>


/////////////////////// Local includes


namespace msxps
{
namespace minexpert
{


//! Enum to specify the way to record the analysis stanzas.
/*!

  When the user performs data scrutiny of a mass spectrum, she can
  decide to record the various data analysis steps performed. The recording of
  these steps might be directed to various targets.

*/
enum RecordTarget
{
  // the left hand value of the << : the bit value (either 0 or 1)
  // the right hand value of the << : the bit index (from right to left)
  // at which the bit value needs to be shifted to.
  RECORD_NOTHING = 0 << 0,
  /*!< Do not record anything */
  RECORD_TO_CONSOLE = 1 << 1,
  /*!< Record to the console window */
  RECORD_TO_FILE = 1 << 2,
  /*!< Record to file */
  RECORD_TO_CLIPBOARD = 1 << 3,
  /*!< Record to clipboard */
};

//! Enum to characterize the mass spec data.
/*!

  When dealing with mass spectrometry data, a number of different kinds of
  data might be dealt with. This enum serves the purpose to characterize the
  kind of mass spectrometry data at hand.

*/
enum FormatType
{
  TIC_CHROM = 0,
  /*!< Data are of total ion current chromatogram kind*/
  MASS_SPEC,
  /*!< Data are of mass spectrum kind*/
  DRIFT_SPEC,
  /*!< Data are of drift spectrum kind*/
  LAST,
};


//! Structure describing a data file format
/*!

*/
struct DataFormatStringSpecif
{
  FormatType m_formatType;
  /*!< Format type (enum)*/
  QString m_label;
  /*!< Label string for short format display*/
  QString m_format;
  /*!< String for long format description*/
};


//! The AnalysisPreferences class provides a means to configure the data
//! analysis recording
/*!

  When performing data analysis by scrutinizing chromatograms or spectra (mass
  or drift), the user might want to record the analysis steps. This class aims
  at allowing the configuration of the user's wishes as to what and how
  record.

*/
class AnalysisPreferences
{
  public:
  //! Name of the file in which to record data analysis steps.
  QString m_fileName;

	QString m_sampleName;

  //! Hash relating a FormatType with a pointer to a DataFormatStringSpecif
  /*!

    When recording data analysis steps, it necessary to define a string
    format for the text to be recorded. That string format is going to be
    different if the data are mass spectral data or drift spectral data or
    total ion chromatogram data. This hash relates a given FormatType to its
    corresponding format string.

    The key if the FormatType enum value that describes the type of data
    that is being handled. the value is the pointer to the corresponding
    DataFormatStringSpecif structure.

*/
  QHash<int, DataFormatStringSpecif *> m_dataFormatStringSpecifHash;


  //! Describes the kind of data analysis recording
  int m_recordTarget = RecordTarget::RECORD_NOTHING;

  //! Describes the file opening mode (overwrite or append) when recording to
  //! file
  int m_fileOpenMode;


  AnalysisPreferences();

  AnalysisPreferences(const AnalysisPreferences &other);

  virtual ~AnalysisPreferences();

  AnalysisPreferences &operator=(const AnalysisPreferences &other);

  QString toString() const;
};


} // namespace minexpert

} // namespace msxps

