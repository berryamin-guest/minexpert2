/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes
#include <vector>


/////////////////////// Qt includes


/////////////////////// pappsomspp includes
#include <pappsomspp/trace/maptrace.h>


/////////////////////// Local includes
#include "ProcessingStep.hpp"
#include "ProcessingFlow.hpp"
#include "MassDataIntegrator.hpp"


namespace msxps
{
namespace minexpert
{

class MsRunDataSetTreeMassDataIntegratorToRt;

typedef std::shared_ptr<MsRunDataSetTreeMassDataIntegratorToRt>
  MsRunDataSetTreeMassDataIntegratorToRtSPtr;

class MsRunDataSetTreeMassDataIntegratorToRt : public MassDataIntegrator
{
  Q_OBJECT;

  public:
  MsRunDataSetTreeMassDataIntegratorToRt();

  MsRunDataSetTreeMassDataIntegratorToRt(MsRunDataSetCstSPtr ms_run_data_set_csp);

  MsRunDataSetTreeMassDataIntegratorToRt(MsRunDataSetCstSPtr ms_run_data_set_csp,
                                     const ProcessingFlow &processing_flow);

  MsRunDataSetTreeMassDataIntegratorToRt(
    const MsRunDataSetTreeMassDataIntegratorToRt &other);

  virtual ~MsRunDataSetTreeMassDataIntegratorToRt();

  const ProcessingFlow &getProcessingFlow() const;
  void appendProcessingStep(ProcessingStep *processing_step_p);

  const MsRunDataSetStats &getMsRunDataSetStats() const;

  public slots:

  void seedInitialTicChromatogramAndMsRunDataSetStatistics();

  bool integrateToRt();

  signals:

  void finishedIntegratingToInitialTicChromatogramSignal(
    MsRunDataSetTreeMassDataIntegratorToRt *mass_data_integrator_p);

  void finishedCalculatingInitialMsRunDataSetStatisticsSignal(
    MsRunDataSetTreeMassDataIntegratorToRt *mass_data_integrator_p);

  protected:
  MsRunDataSetStats m_msRunDataSetStats;
};

} // namespace minexpert

} // namespace msxps

Q_DECLARE_METATYPE(msxps::minexpert::MsRunDataSetTreeMassDataIntegratorToRt);
extern int msRunDataSetTreeMassDataIntegratorMetaTypeId;

Q_DECLARE_METATYPE(msxps::minexpert::MsRunDataSetTreeMassDataIntegratorToRtSPtr);
extern int msRunDataSetTreeMassDataIntegratorSPtrMetaTypeId;
