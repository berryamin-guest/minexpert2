/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes
#include <vector>


/////////////////////// Qt includes


/////////////////////// pappsomspp includes
#include <pappsomspp/trace/maptrace.h>


/////////////////////// Local includes
#include "globals.hpp"
#include "MsRunDataSet.hpp"
#include "ProcessingFlow.hpp"


namespace msxps
{
namespace minexpert
{

class MassDataIntegrator;

typedef std::shared_ptr<MassDataIntegrator> MassDataIntegratorSPtr;

class MassDataIntegrator : public QObject
{
  Q_OBJECT;

  public:
  MassDataIntegrator();

  MassDataIntegrator(MsRunDataSetCstSPtr ms_run_data_set_csp);
  MassDataIntegrator(MsRunDataSetCstSPtr ms_run_data_set_csp,
                     const ProcessingFlow &processing_flow);

  MassDataIntegrator(const MassDataIntegrator &other);

  virtual ~MassDataIntegrator();

  void setMsRunDataSet(MsRunDataSetCstSPtr ms_run_data_set_csp);
  MsRunDataSetCstSPtr getMsRunDataSet() const;

	const ProcessingFlow &getProcessingFlow() const;

  const pappso::MapTrace &getMapTrace() const;
  const std::shared_ptr<std::map<double, pappso::MapTrace>>
  getDoubleMapTraceMapSPtr() const;

  public slots:

  void cancelOperation();


  signals:
  void cancelOperationSignal();

  void setTaskDescriptionTextSignal(QString text);

  void setupProgressBarSignal(std::size_t min_value, std::size_t max_value);
  void setProgressBarMinValueSignal(std::size_t value);
  void setProgressBarMaxValueSignal(std::size_t value);
  void incrementProgressBarMaxValueSignal(std::size_t increment);
  void setProgressBarCurrentValueSignal(std::size_t number_of_processed_nodes);
  void incrementProgressBarCurrentValueSignal(std::size_t increment);

  void setStatusTextSignal(QString text);
  void appendStatusTextSignal(QString text);
  void setStatusTextAndCurrentValueSignal(QString text, int value);

  void
  incrementProgressBarCurrentValueAndSetStatusTextSignal(std::size_t increment,
                                                         const QString &text);

  void logTextToConsoleSignal(QString text);
  void logColoredTextToConsoleSignal(QString text, QColor color);

  void lockTaskMonitorCompositeWidgetSignal();
  void unlockTaskMonitorCompositeWidgetSignal();

  protected:
  MsRunDataSetCstSPtr mcsp_msRunDataSet = nullptr;

  ProcessingFlow m_processingFlow;

  // These are the data structure used to store the result of the computation.

  // map <double, double> with types <double, double> representing (mz, i) or
  // (rt, tic), for example.
  pappso::MapTrace m_mapTrace;

  using Map = std::map<double, pappso::MapTrace>;
  std::shared_ptr<Map> m_doubleMapTraceMapSPtr = nullptr;

  bool m_isOperationCancelled = false;

  std::size_t
  bestThreadCountForMsRunDataSetTreeNodeCount(std::size_t node_count);
  // In the pair below, first is the ideal number of threads and second is the
  // number of nodes per thread.
  std::pair<std::size_t, std::size_t>
  bestParallelIntegrationParams(std::size_t node_count);

  using Iterator = std::vector<pappso::MsRunDataSetTreeNode *>::const_iterator;

  std::vector<std::pair<Iterator, Iterator>>
  calculateMsRunDataSetTreeNodeIteratorPairs(std::size_t node_count,
                                             std::size_t thread_count,
                                             int nodes_per_thread);
  std::vector<std::pair<Iterator, Iterator>>
  calculateMsRunDataSetTreeNodeIteratorPairs(Iterator begin_iterator, Iterator end_iterator,
                                             std::size_t thread_count, int nodes_per_thread);

};

} // namespace minexpert

} // namespace msxps

Q_DECLARE_METATYPE(msxps::minexpert::MassDataIntegrator);
extern int massDataIntegratorMetaTypeId;

Q_DECLARE_METATYPE(msxps::minexpert::MassDataIntegratorSPtr);
extern int massDataIntegratorSPtrMetaTypeId;
