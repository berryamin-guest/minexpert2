/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes
#include <map>


/////////////////////// Qt includes
#include <QString>
#include <QDateTime>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "ProcessingStep.hpp"
#include "MsRunDataSet.hpp"


namespace msxps
{
namespace minexpert
{


class ProcessingFlow : public std::vector<ProcessingStep *>
{

  public:
  ProcessingFlow();
  ProcessingFlow(MsRunDataSetCstSPtr ms_run_data_set_csp);
  ProcessingFlow(const ProcessingFlow &other);

  virtual ~ProcessingFlow();

  ProcessingFlow &operator=(const ProcessingFlow &other);

  std::vector<ProcessingStep *> steps();

  bool innermostRange(ProcessingType type_mask,
                      std::pair<ProcessingType, ProcessingSpec *> &pair) const;
  bool innermostRange(ProcessingType type_mask, double &start, double &end) const;

  bool innermostRtRange(std::pair<ProcessingType, ProcessingSpec *> &pair) const;
	bool innermostRtRange(double &start, double &end) const;

  bool innermostDtRange(std::pair<ProcessingType, ProcessingSpec *> &pair) const;
	bool innermostDtRange(double &start, double &end) const;

  bool innermostMzRange(std::pair<ProcessingType, ProcessingSpec *> &pair) const;
	bool innermostMzRange(double &start, double &end) const;

  size_t greatestMsLevel() const;

  const ProcessingStep *oldestStep() const;
  const ProcessingStep *closestOlderStep(const QDateTime &date_time) const;
  const ProcessingStep *mostRecentStep() const;

  void setDefaultMzIntegrationParams(
    const MzIntegrationParams &mz_integration_params);
  const MzIntegrationParams &getDefaultMzIntegrationParams() const;
  const MzIntegrationParams *mostRecentMzIntegrationParams() const;

  void setDefaultMsFragmentationSpec(
    const MsFragmentationSpec &ms_fragmentation_spec);
  const MsFragmentationSpec &getDefaultMsFragmentationSpec() const;

  void setMsRunDataSetCstSPtr(MsRunDataSetCstSPtr ms_run_data_set_csp);
  MsRunDataSetCstSPtr getMsRunDataSetCstSPtr() const;

  QString toString(int offset = 0, const QString &spacer = QString()) const;

  protected:
  MsRunDataSetCstSPtr mcsp_msRunDataSet = nullptr;

  // Note that each step has an optional MzIntegrationParams *. This one is the
  // default one when not a single step has mz integration params. This one is
  // set automatically in the first graph obtained when displaying the TIC
  // chromatogram right after having loaded the ms run from file.
  MzIntegrationParams m_defaultMzIntegrationParams;

  // Same for MsFragmentationSpec
  MsFragmentationSpec m_defaultMsFragmentationSpec;
};


} // namespace minexpert

} // namespace msxps
