# ${TARGET}-objlib will be the name of the object files collection (in CMake
# parlance that is an "OBJECT" library type. In that library, that is simply a
# list of compiled object files, I put all the *.o files that make the ${TARGET}
# (minexpert or massxpert) executable image file, but not main.cpp. This is done
# because when building the tests/tests binary for tests relating to ${TARGET},
# I'll reuse these compiled object files.

set(CMAKE_COLOR_MAKEFILE ON)
set(CMAKE_VERBOSE_MAKEFILE ON)

if(APPLE)
	set(TARGET mineXpert2)
	message(STATUS "APPLE target name: ${TARGET}")
elseif(UNIX AND NOT APPLE)
	set(TARGET minexpert2)
	message(STATUS "Non-APPLE target name: ${TARGET}")
elseif(WIN64)
	set(TARGET mineXpert2.exe)
	message(STATUS "WIN64 target name: ${TARGET}")
endif()

set(TITLE_TARGET "mineXpert2")
set(DOCTARGET "minexpert2")
set(MINEXPERT_TARGET_NAME ${TARGET})

message("")
message(STATUS "${BoldGreen}Starting configuring of ${TARGET}${ColourReset}")
message("")

# Prepare various variables for parent dir consumption later.
set(MINEXPERT2_TARGET_NAME ${TARGET} PARENT_SCOPE)
set(TARGET ${TARGET} PARENT_SCOPE)

#############################################################
# Software configuration
message(STATUS "CMAKE_CURRENT_BINARY_DIR: " ${CMAKE_CURRENT_BINARY_DIR})

########################################################
# Files
set(${TARGET}-objlib_nongui_SRCS
	#
	# Some globals.
	nongui/globals.cpp
	#
	# The classes for the IsoSpec calculations.
	nongui/Atom.cpp
	nongui/AtomCount.cpp
	nongui/Formula.cpp
	nongui/IsoSpecEntity.cpp
	nongui/Isotope.cpp
	nongui/Ponderable.cpp
	#
	# The main class that hold the mass data.
	nongui/MsRunDataSet.cpp
	#
	# The class to handle the statistics computation about a given MsRunDataSet.
	nongui/MsRunDataSetStats.cpp

	nongui/MassSpecDataFileLoader.cpp
	#
	# The classe that monitors integrators.
	nongui/MassDataIntegratorTask.cpp
	#
	# The base class of the integrators.
	nongui/MassDataIntegrator.cpp
	#
	# The various integrators that use BaseMsRunDataSetTreeNodeVisitor.
	nongui/MsRunDataSetTreeMassDataIntegratorToRt.cpp
	nongui/MsRunDataSetTreeMassDataIntegratorToMz.cpp
	nongui/MsRunDataSetTreeMassDataIntegratorToDt.cpp
	nongui/MsRunDataSetTreeMassDataIntegratorToDtRtMz.cpp
	nongui/MsRunDataSetTreeMassDataIntegratorToTicInt.cpp
	#
	# The various MsRunDataSetTreeNodeVisitor classes.
	nongui/BaseMsRunDataSetTreeNodeVisitor.cpp
	nongui/MsRunStatisticsTreeNodeVisitor.cpp
	nongui/TicChromTreeNodeCombinerVisitor.cpp
	nongui/IntensityTreeNodeCombinerVisitor.cpp
	nongui/RtDtMzColorMapsTreeNodeCombinerVisitor.cpp
	nongui/TraceTreeNodeCombinerVisitor.cpp
	nongui/MassSpectrumTreeNodeCombinerVisitor.cpp
	nongui/DriftSpectrumTreeNodeCombinerVisitor.cpp
	nongui/MultiTreeNodeCombinerVisitor.cpp
	#
	# The various integrators that use QualifiedMassSpectrumVector.
	nongui/QualifiedMassSpectrumVectorMassDataIntegrator.cpp
	nongui/QualifiedMassSpectrumVectorMassDataIntegratorToRt.cpp
	nongui/QualifiedMassSpectrumVectorMassDataIntegratorToMz.cpp
	nongui/QualifiedMassSpectrumVectorMassDataIntegratorToDt.cpp
	nongui/QualifiedMassSpectrumVectorMassDataIntegratorToTicInt.cpp
	nongui/QualifiedMassSpectrumVectorMassDataIntegratorToDtRtMz.cpp
	#
	# The classes that handle the processing flow
	nongui/ProcessingFlow.cpp
	nongui/ProcessingStep.cpp
	nongui/ProcessingSpec.cpp
	nongui/ProcessingType.cpp
	#
	# The classes that handle the parameters for the various integrations and the
	# display of traces.
	nongui/MzIntegrationParams.cpp
	nongui/MsFragmentationSpec.cpp
	#
	# Peak-shaping classes (Gaussian Lorentzian peak shapes).
	nongui/MassPeakShaperConfig.cpp
	nongui/MassPeakShaper.cpp
	#
	# Analysis preferences
	nongui/AnalysisPreferences.cpp
	)

set(${TARGET}-objlib_gui_SRCS
	gui/Application.cpp
	gui/ProgramWindow.cpp
	gui/MsRunSelectorDlg.cpp
	gui/AboutDlg.cpp
	#
	gui/ConsoleWnd.cpp
	#
	gui/MsRunReadTask.cpp
	#
	gui/OpenMsRunDataSetsDlg.cpp
	gui/ColorSelector.cpp
	#
	gui/TaskMonitorCompositeWidget.cpp
	gui/TaskMonitorWnd.cpp
	#
	# The ms run data set tree view classes
	gui/MsRunDataSetTableViewWnd.cpp
	gui/MsRunDataSetTableViewItem.cpp
	gui/MsRunDataSetTableView.cpp
	gui/MsRunDataSetTableViewModel.cpp
	gui/MsRunDataSetTableViewProxyModel.cpp
	#	
	# The MS fragmentation specification dialog
	gui/MsFragmentationSpecDlg.cpp
	gui/MzIntegrationParamsDlg.cpp
	#
	# The data plot graph/widget tree classes
	gui/DataPlottableNode.cpp
	gui/DataPlottableTree.cpp
	#
	# The trace plot widgets derived from pappso::BaseTracePlotWidget
	gui/TicXicChromTracePlotWidget.cpp
	gui/MassSpecTracePlotWidget.cpp
	gui/DriftSpecTracePlotWidget.cpp
	#
	# The trace plot composite widgets
	gui/BasePlotCompositeWidget.cpp
	gui/BaseTracePlotCompositeWidget.cpp
	gui/TicXicChromTracePlotCompositeWidget.cpp
	gui/MassSpecTracePlotCompositeWidget.cpp
	gui/DriftSpecTracePlotCompositeWidget.cpp
	#
	# The color map plot composite widgets
	gui/BaseColorMapPlotCompositeWidget.cpp
	gui/TicXicChromMassSpecColorMapPlotCompositeWidget.cpp
	gui/DriftSpecMassSpecColorMapPlotCompositeWidget.cpp
	#
	# The plot windows
	gui/BasePlotWnd.cpp
	#	
	# Trace plot windows
	gui/BaseTracePlotWnd.cpp
	gui/BaseColorMapPlotWnd.cpp
	gui/TicXicChromTracePlotWnd.cpp
	gui/MassSpecTracePlotWnd.cpp
	gui/DriftSpecTracePlotWnd.cpp
	#
	# Color map plot windows
	gui/TicXicChromMassSpecColorMapWnd.cpp
	gui/DriftSpecMassSpecColorMapWnd.cpp
	#
	# The save plot to graphics file
	gui/SaveToGraphicsFileDlg.cpp
	#
	# The IsoSpec computations-related widgets
	gui/IsoSpecDlg.cpp
	gui/IsoSpecTableView.cpp
	gui/IsoSpecTableViewModel.cpp
	gui/MassPeakShaperDlg.cpp
	#
	# The ProcessingFlow viewer dialog window
	gui/ProcessingFlowViewerDlg.cpp
	#
	# The XIC chromatogram feature
	gui/XicExtractionWnd.cpp
	#
	# Analysis preferences
	gui/AnalysisPreferencesDlg.cpp
	)

set(${TARGET}-objlib_UIS
	gui/ui/AboutDlg.ui
	gui/ui/MsRunSelectorDlg.ui
	gui/ui/OpenMsRunDataSetsDlg.ui
	gui/ui/MsRunDataSetTableViewWnd.ui
	gui/ui/BasePlotWnd.ui
	gui/ui/BasePlotCompositeWidget.ui
	gui/ui/TaskMonitorCompositeWidget.ui
	gui/ui/TaskMonitorWnd.ui
	gui/ui/ConsoleWnd.ui
	gui/ui/MsFragmentationSpecDlg.ui
	gui/ui/MzIntegrationParamsDlg.ui
	gui/ui/SaveToGraphicsFileDlg.ui
	gui/ui/IsoSpecDlg.ui
	gui/ui/MassPeakShaperDlg.ui	
	gui/ui/ElementGroupBoxWidget.ui
	gui/ui/ProcessingFlowViewerDlg.ui
	gui/ui/XicExtractionWnd.ui
	gui/ui/AnalysisPreferencesDlg.ui)

# The only file that does not enter in the object library of
# object files (see notice above).
set(${TARGET}-exec_SRCS main.cpp)


# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)

find_package(Qt5 COMPONENTS Widgets Xml Svg PrintSupport)

qt5_wrap_ui(${TARGET}-objlib_UIS_H ${${TARGET}-objlib_UIS})

qt5_add_resources(${TARGET}_QRC_CPP gui/application.qrc)

### These two lines required because now CMake only automocs
### the generated files and not the source files.
set_source_files_properties(${${TARGET}-objlib_UIS_H} PROPERTIES SKIP_AUTOMOC ON)
set_source_files_properties(${${TARGET}_QRC_CPP} PROPERTIES SKIP_AUTOMOC ON)


add_library(${TARGET}-objlib OBJECT
	${${TARGET}-objlib_nongui_SRCS}
	${${TARGET}-objlib_gui_SRCS}
	${${TARGET}-objlib_UIS_H})


###############################################################
# Configuration of the binary to be built
###############################################################

# Only now can we add the executable, because we have defined the sources of the
# object library above.

if(NOT APPLE)

	add_executable(${TARGET}
		${${TARGET}-exec_SRCS}
		$<TARGET_OBJECTS:${TARGET}-objlib>
		${${TARGET}_QRC_CPP})

else()

	# Copy the icon file to the Contents/Resources directory of the bundle at
	# location ${MACOSX_PACKAGE_LOCATION}.
	set_source_files_properties(../images/icons/mineXpert2.icns PROPERTIES MACOSX_PACKAGE_LOCATION Resources)

	# Identify MacOS bundle

	set(MACOSX_BUNDLE_BUNDLE_NAME ${TARGET})
	message(STATUS "MACOSX_BUNDLE_BUNDLE_NAME: ${MACOSX_BUNDLE_BUNDLE_NAME}")

	set(MACOSX_BUNDLE_BUNDLE_VERSION ${PROJECT_VERSION})
	set(MACOSX_BUNDLE_LONG_VERSION_STRING ${PROJECT_VERSION})
	set(MACOSX_BUNDLE_SHORT_VERSION_STRING ${PROJECT_VERSION})
	set(MACOSX_BUNDLE_COPYRIGHT ${COPYRIGHT})
	set(MACOSX_BUNDLE_GUI_IDENTIFIER ${IDENTIFIER})
	set(MACOSX_BUNDLE_ICON_FILE ../images/icons/mineXpert2.icns)

	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mmacosx-version-min=10.9"
		CACHE STRING "Flags used by the compiler during all build types." FORCE)

	#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fvisibility-inlines-hidden"
	#CACHE STRING "Flags used by the compiler during all build types." FORCE)

	set(CMAKE_OSX_SYSROOT "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.12.sdk/"
		CACHE STRING "MacOSX10.12.sdk." FORCE)

	add_executable(${TARGET} MACOSX_BUNDLE
		${${TARGET}-exec_SRCS}
		$<TARGET_OBJECTS:${TARGET}-objlib>
		${${TARGET}_QRC_CPP}
		../images/icons/mineXpert2.icns
		)

	set_target_properties(${TARGET} PROPERTIES MACOSX_BUNDLE_ICON_FILE mineXpert2.icns)

endif()

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17 -pthread")

qt5_use_modules(${TARGET} Widgets)
qt5_use_modules(${TARGET} Xml)
qt5_use_modules(${TARGET} Svg)
qt5_use_modules(${TARGET} Sql)
qt5_use_modules(${TARGET} PrintSupport)

include_directories(

	# The config file is binary directory-specific !!!
	# For the config.h file generated from config.h.in
	# For the ui_Xyyyyy.h files
	${CMAKE_CURRENT_BINARY_DIR}

	# For all the source subdirectories
	${CMAKE_SOURCE_DIR}

	${Qt5Widgets_INCLUDE_DIRS}
	${Qt5Xml_INCLUDE_DIRS}
	${Qt5Svg_INCLUDE_DIRS}
	${Qt5Sql_INCLUDE_DIRS}
	${Qt5PrintSupport_INCLUDE_DIRS})

# On GNU/Linux, make sure we have the QCustomPlot library
# installed on the system (on MINGW, we have the
# source files (see above).
if(NOT WIN64 AND NOT APPLE)

	find_package(QCustomPlot)

	if(NOT QCustomPlot_FOUND)
		message(FATAL_ERROR "QCustomPlot not found!")
	else()
		message(STATUS "QCustomPlot found!")
	endif()

endif()


message(STATUS "Using parallelizing library ${OPENMP_LIBRARY}")

set(required_target_link_libraries
	${OPENMP_LIBRARY}
	Qt5::Widgets
	Qt5::Xml
	Qt5::Svg
	Qt5::Sql
	Qt5::PrintSupport)


# On WIN64 we provide a number of libraries by our own, that we
# put in /usr/local/lib under /mingw64/.
if(WIN64)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -LC:/msys64/mingw64/usr/local/lib")
endif()


##############################################################
# libpappsomspp
message(STATUS "${BoldYellow}Link against the libpappsomspp library.${ColourReset}")
if(WIN64)
	include_directories("/mingw64/include/pwiz")
elseif(APPLE)
	include_directories("/opt/local/include/pwiz")
	link_directories("/opt/local/lib")
else()

	message(STATUS "Building privately")

	if(${CMAKE_BINARY_DIR} STREQUAL
			"/home/rusconi/devel/msxpertsuite/minexpert2/build-area")

		include_directories("/home/rusconi/devel/libpappsomspp/development/src")
		set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -L/home/rusconi/devel/libpappsomspp/build-area/src")
		set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -L/home/rusconi/devel/libpappsomspp/build-area/src/pappsomspp/widget")

		# Run the program like this:
		message(STATUS "Run the program like this:
		LD_LIBRARY_PATH=/home/rusconi/devel/libpappsomspp/build-area/src/pappsomspp/widget:/home/rusconi/devel/libpappsomspp/build-area/src:\${LD_LIBRARY_PATH} src/minexpert2")

	elseif() # Making a system-wide build for packaging
		message(STATUS "Building system-wide")
		include_directories("/usr/include/pappsomspp")
	endif()

endif()

set(required_target_link_libraries
	${required_target_link_libraries}
	pappsomspp-qt5
	pappsomspp-widget-qt5)


##############################################################
# libpwiz for reading mass spec data files,
message(STATUS "${BoldYellow}Link against the libpwiz library.${ColourReset}")
if(WIN64)
	include_directories("/mingw64/include/pwiz")
elseif(APPLE)
	include_directories("/opt/local/include/pwiz")
	link_directories("/opt/local/lib")
else()
	include_directories("/usr/include/proteowizard")
endif()

set(required_target_link_libraries
	${required_target_link_libraries}
	pwiz)


##############################################################
# libIsoSpec++ for calculating isotopic clusters
message(STATUS "${BoldYellow}Link against the libIsoSpec++ library.${ColourReset}")
if(WIN64)
	include_directories("C:/msys64/mingw64/usr/local/include")
elseif(APPLE)
	include_directories("/opt/local/include")
	link_directories("/opt/local/lib")
else()
	include_directories("/usr/include")
endif()

set(required_target_link_libraries
	${required_target_link_libraries}
	IsoSpec++)


##############################################################
# libboost_* on mingw
# On MINGW.* we are our own provider of the boost libraries.
if(WIN64)
	message(STATUS "${BoldYellow}Link against the libboost libraries.${ColourReset}")

	include_directories("${CMAKE_SOURCE_DIR}/../../boost/boost_1_56_0")

	set(required_target_link_libraries
		${required_target_link_libraries}
		boost_chrono
		boost_filesystem
		boost_iostreams
		boost_program_options
		boost_serialization
		boost_system
		boost_thread
		z)
elseif(APPLE)
	# With macport
	include_directories("/opt/local/include")
	# Attention #####
	# below for
	# /opt/local/include/pwiz/libraries/boost_aux/boost/utility/singleton.hpp
	include_directories("/opt/local/include/pwiz/libraries/boost_aux")
	link_directories("/opt/local/lib")

	set(required_target_link_libraries
		${required_target_link_libraries}
		boost_chrono-mt
		boost_filesystem-mt
		boost_iostreams-mt
		boost_program_options-mt
		boost_serialization-mt
		boost_system-mt
		boost_thread-mt
		z)
endif()

# On MINGW.*, we'll need to add the -lstdc++ flag. Don't ask why.
if(WIN64)

	set(required_target_link_libraries
		${required_target_link_libraries}
		"stdc++")

endif()

# When on UNIX/NOT APPLE we can make use the system-wide qcustomplot library.
if(UNIX AND NOT APPLE)

	set(required_target_link_libraries
		${required_target_link_libraries}
		"qcustomplot")

endif()


#### Debugging stuff
message(STATUS required_target_link_libraries: ${required_target_link_libraries})

message(STATUS "Now printing all the include directories -I...")

get_property(dirs DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY INCLUDE_DIRECTORIES)
foreach(dir ${dirs})
	message(STATUS "included dir='${dir}'")
endforeach()

message(STATUS "Now printing all the linking directories -L...")

get_property(dirs DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY LINK_DIRECTORIES)
foreach(dir ${dirs})
	message(STATUS "link directory dir='${dir}'")
endforeach()

# Finally actually set the linking dependencies to the executable.
target_link_libraries(${TARGET}
	${required_target_link_libraries})

if(PROFILE)
	message(STATUS "Profiling is requested, adding linker -pg flag.")
	set (CMAKE_EXE_LINKER_FLAGS "-pg")
endif()

if(NOT APPLE)
	# We want to install the binary arch-dependent target in
	# specific situations. To have proper control, we define the arch
	# component.
	install(TARGETS ${TARGET}
		RUNTIME
		COMPONENT arch
		DESTINATION ${MINEXPERT2_BIN_DIR})
endif()


message("")
message(STATUS "${BoldGreen}Finished configuring of ${TARGET}.${ColourReset}")
message("")
