/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Std includes
#include <iostream>
#include <iomanip>
#include <string>
#include <algorithm>
#include <limits>
#include <ctime>
#include <chrono>


/////////////////////// Qt includes
#include <QApplication>
#include <QtGlobal>
#include <QStringList>
#include <QVector>
#include <QDebug>
#include <QCommandLineParser>
#include <QTableView>
#include <QClipboard>


/////////////////////// pappso includes

/////////////////////// Local includes
#include "config.h"
#include "gui/Application.hpp"
#include "gui/ProgramWindow.hpp"


namespace msxps
{
namespace minexpert
{

void printGreetings();
void printVersion();
void printConfig(const QString & = QString());

void
printGreetings()
{
  QString greetings = QObject::tr("mineXpert, version %1\n\n").arg(VERSION);

  greetings += "Type 'minexpert --help' for help\n\n";

  greetings +=
    "mineXpert is Copyright 2016-2019 \n"
    "by Filippo Rusconi.\n\n"
    "mineXpert comes with ABSOLUTELY NO WARRANTY.\n"
    "mineXpert is free software, "
    "covered by the GNU General\n"
    "Public License Version 3 or later, "
    "and you are welcome to change it\n"
    "and/or distribute copies of it under "
    "certain conditions.\n"
    "Check the file COPYING in the distribution "
    "and/or the\n"
    "'Help/About(Ctrl+H)' menu item of the program.\n"
    "\nHappy mineXpert'ing!\n\n";

  std::cout << greetings.toStdString();
}


void
printVersion()
{
  QString version = QObject::tr(
                      "mineXpert, version %1 -- "
                      "Compiled against Qt, version %2\n")
                      .arg(VERSION)
                      .arg(QT_VERSION_STR);

  std::cout << version.toStdString();
}


void
printConfig(const QString &execName)
{
  QString config = QObject::tr(
                     "mineXpert: "
                     "Compiled with the following configuration:\n"
                     "EXECUTABLE BINARY FILE: = %1\n"
                     "MSXPERTSUITE_BIN_DIR = %2\n"
                     "MSXPERTSUITE_DATA_DIR = %3\n"
                     "MSXPERTSUITE_DOC_DIR = %5\n")
                     .arg(execName)
                     .arg(MINEXPERT2_BIN_DIR)
                     .arg(MINEXPERT2_DOC_DIR);

  std::cout << config.toStdString();
}

} // namespace minexpert
} // namespace msxps


int
main(int argc, char **argv)
{

  Q_INIT_RESOURCE(application);

  // Qt stuff starts here.
  msxps::minexpert::Application application(argc, argv, "minexpert2");

  QCommandLineParser parser;
  parser.setApplicationDescription("mineXpert2");
  parser.addHelpOption();
  parser.addVersionOption();

  // Process the actual command line arguments given by the user
  parser.process(application);
  QString file_name;

  msxps::minexpert::ProgramWindow *main_window_p =
    new msxps::minexpert::ProgramWindow(nullptr, "mineXpert2");
  main_window_p->show();

  const QStringList args = parser.positionalArguments();

  if(!args.isEmpty())
    {
      for(auto &&file_name : args)
        {
          // Check if the argument is a directory or a file.

          QFileInfo file_info(file_name);

          if(!file_info.exists() || file_info.isDir())
            main_window_p->openMassSpectrometryFileDlg(file_name, false);
          else
            main_window_p->openMassSpectrometryFile(file_name, false);
        }
    }
  else
    {
      main_window_p->openMassSpectrometryFileDlg(file_name, false);
    }

  return application.exec();
}
// End of
// main(int argc, char **argv)


#if 0
  // The MsFileAccessor will allow to get a list of MS run ids that can then be
  // used to request a MS run reader appropriate for the MS data file format.
  pappso::MsFileAccessor fileAccessor(fileName, 0);

  std::vector<pappso::MsRunIdCstSPtr> ms_run_ids = fileAccessor.getMsRunIds();

  std::size_t run_count = ms_run_ids.size();

  qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "()"
           << "The number of ms runs in the file is:" << run_count;

  if(!run_count)
    exit(1);


  // Now actually load the data!

  // Get the first run id of the vector.
  pappso::MsRunIdCstSPtr ms_run_id_csp = ms_run_ids.front();

  qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "()"
           << "The run id as text:" << ms_run_id_csp->asText();

  // Instantiate our private mass spec data loader for pwiz since we know we
  // test that kind of loader.
  msxps::minexpert::MassSpecDataFileLoaderPwiz loader(fileName);

  // Allocate a new data set that will be reparented when we provide it to the
  // handling widget (window or plot widget or whatever QObject).

  bool is_streamed_mode = false;
  // bool is_streamed_mode = true;

  msxps::minexpert::MsRunDataSet *mass_spec_data_set_p =
    new msxps::minexpert::MsRunDataSet(
      *ms_run_id_csp, nullptr, "NOT_SET", fileName, is_streamed_mode);

  // Inform the loader that the mass spec data is there.
  loader.setMsRunDataSet(mass_spec_data_set_p);
  loader.setStreamed(is_streamed_mode);

  // From the file accessor get a shared pointer to the MS run reader for the ms
  // run id of interest.
  pappso::MsRunReaderSp reader = fileAccessor.msRunReaderSp(ms_run_id_csp);

  if(reader == nullptr)
    exit(1);

  qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "()"
           << "Obtained ms run reader's ms run id:"
           << reader->getMsRunId()->asText();

  qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "()"
           << "spectrum list size:" << reader->spectrumListSize();

  auto read_start = std::chrono::system_clock::now();

  // Actually start reading the spectra from the spectrum list corresponding to
  // the ms run id.
  reader->readSpectrumCollection(loader);

  // Report on the duration of the data loading process.
  auto read_end = std::chrono::system_clock::now();
  auto diff     = read_end - read_start;
  auto seconds  = std::chrono::duration_cast<std::chrono::seconds>(diff);
  auto milliseconds =
    std::chrono::duration_cast<std::chrono::milliseconds>(diff);

  pappso::MsRunDataSetTree *mass_spec_data_tree_p =
    mass_spec_data_set_p->getMsRunDataSetTree();

  std::cout << __FILE__ << " @ " << __LINE__ << " " << __FUNCTION__ << " () "
            << std::setprecision(15) << "Loaded data file in "
            << seconds.count() << "seconds with "
            << mass_spec_data_tree_p->getSpectrumCount()
            << " spectra. Depth of the data tree: "
            << mass_spec_data_tree_p->depth() << std::endl;

  const std::map<std::size_t, pappso::MsRunDataSetNode *> &index_node_map =
    mass_spec_data_tree_p->getIndexNodeMap();

  std::cout << __FILE__ << " @ " << __LINE__ << " " << __FUNCTION__ << " () "
            << std::setprecision(15) << "The index/node map contains "
            << index_node_map.size() << " items." << std::endl;

  msxps::minexpert::MsRunDataSetTableViewModel model(mass_spec_data_tree_p);

  msxps::minexpert::MsRunDataSetTableView tree_view;

  msxps::minexpert::MsRunDataSetTableViewProxyModel *proxyModel =
    new msxps::minexpert::MsRunDataSetTableViewProxyModel(&application);

  proxyModel->setSourceModel(&model);
  // tree_view.setModel(&model);
  tree_view.setModel(proxyModel);

  tree_view.setSortingEnabled(true);
  tree_view.sortByColumn(0, Qt::AscendingOrder);

  tree_view.setWindowTitle(ms_run_id_csp->getRunId());

  tree_view.setExpandsOnDoubleClick(true);
  tree_view.setRootIsDecorated(true);
  tree_view.setIndentation(10);
  tree_view.setAlternatingRowColors(true);
  tree_view.setSelectionBehavior(QAbstractItemView::SelectRows);
  tree_view.setSelectionMode(QAbstractItemView::ExtendedSelection);

  tree_view.show();

  qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << " ()"
           << "Done filling in the tree view.";

  // Try to perform an integration over all the mass spectra of the file.

  // Store the decimal places we want to work with (-1 : no rounding)
  // int decimal_places = -1;

  // Instantiate a mz integration params object

#if 0

  pappso::PrecisionPtr precision_p =
    pappso::PrecisionFactory::getDaltonInstance(0.019);

  // pappso::BinningType binng_type = pappso::BinningType::ARBITRARY
  msxps::minexpert::BinningType binning_type =
    // msxps::minexpert::BinningType::NONE;
    msxps::minexpert::BinningType::ARBITRARY;

  msxps::minexpert::MzIntegrationParams mz_integ_params(
    mass_spec_data_set_p->mzRangeStart(),
    mass_spec_data_set_p->mzRangeEnd(),
    binning_type,
    decimal_places,
    precision_p,
    false /* applyMzShift */,
    0,
    true /* removeZeroValDataPoints */);

  qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "()"
           << "MzIntegrationParams:" << mz_integ_params.asText()
           << "mz min:" << mass_spec_data_set_p->mzRangeStart()
           << "mz max:" << mass_spec_data_set_p->mzRangeEnd();

  //////////////////////// combinations ///////////////////////////

  // Record the start time.
  // std::chrono::system_clock::time_point start =
  // std::chrono::system_clock::now();

  // If we are combining with no bins:
  pappso::TracePlusCombiner trace_combiner(decimal_places);

  // if we are combining with bins:
  pappso::MassSpectrumPlusCombiner mass_spectrum_combiner(decimal_places);

  // Instantiate the receptable MapTrace object
  pappso::MapTrace map_trace;

  if(binning_type != msxps::minexpert::BinningType::NONE)
    {
      // Create the bins using the mz integration params
      std::vector<pappso::pappso_double> bins = mz_integ_params.createBins();

      mass_spectrum_combiner.setBins(bins);

      qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "()"
               << "combiner bins" << bins.size();
    }

  // Now get a pointer to the first mass spectrum that is pointed to by the
  // first map item.

  const std::map<std::size_t, pappso::MsRunDataSetNode *> &index_node_map =
    mass_spec_data_tree_p->getIndexNodeMap();

  // Now iterate in the map and for each map, do the combination.

  using Map         = std::map<std::size_t, pappso::MsRunDataSetNode *>;
  using MapIterator = Map::const_iterator;

  std::size_t spec_index = 0;

  for(MapIterator iterator = index_node_map.begin();
      iterator != index_node_map.end();
      ++iterator, ++spec_index)
    {
      pappso::MassSpectrumCstSPtr entering_spectrum =
        iterator->second->getQualifiedMassSpectrum()->getMassSpectrumSPtr();

      if(entering_spectrum == nullptr)
        {
          // qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << " ()"
          //<< "Combining spectrum in streamed mode.";

          // We have not loaded the data file with need peak list set to true.
          // We need to get the data from the file. We still have the reader at
          // hand. Use it.

          pappso::QualifiedMassSpectrum qualified_mass_spectrum =
            reader->qualifiedMassSpectrum(iterator->first);

          entering_spectrum = qualified_mass_spectrum.getMassSpectrumSPtr();
        }

      // qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << " ()"
      //<< "Will combine spectrum pos:" << spec_index + 1;

      if(binning_type == msxps::minexpert::BinningType::NONE)
        trace_combiner.combine(map_trace, *entering_spectrum);
      else
        mass_spectrum_combiner.combine(map_trace, *entering_spectrum);

      // qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << " ()"
      //<< "map trace size:" << map_trace.size();
    }
#endif

#if 0
  // Now try to get an integration of only the ms1 spectra.

  std::vector<pappso::MsRunDataSetNode *> nodes =
    mass_spec_data_tree_p->getRootNodes();
#endif

#if 0
  // Now try to get an integration of only the ms2 spectra.

  std::vector<pappso::MsRunDataSetNode *> nodes =
		mass_spec_data_tree_p->flattenedView();
		//mass_spec_data_tree_p->flattenedViewMsLevel(1, true);
		//mass_spec_data_tree_p->flattenedViewMsLevel(1, false);
		//mass_spec_data_tree_p->flattenedViewMsLevel(2, true);
		//mass_spec_data_tree_p->flattenedViewMsLevel(2, false);

  std::cout << __FILE__ << " @ " << __LINE__ << " " << __FUNCTION__ << " () "
            << std::setprecision(15)
            << "The returned node vector has size: " << nodes.size()
            << std::endl;

  // Iterate in the nodes and perform the combination of these spectra.

  std::size_t combined_spectra_count = 0;

	pappso::pappso_double combination_single_tic_intensity = 0;

  for(auto &&node : nodes)
    {
      pappso::MassSpectrumCstSPtr entering_spectrum =
        node->getQualifiedMassSpectrum()->getMassSpectrumSPtr();

      if(entering_spectrum == nullptr)
        {
          // qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << " ()"
          //<< "Combining spectrum in streamed mode.";

          // We have not loaded the data file with need peak list set to true.
          // We need to get the data from the file. We still have the reader at
          // hand. Use it.

          pappso::QualifiedMassSpectrum qualified_mass_spectrum =
            reader->qualifiedMassSpectrum(node->getQualifiedMassSpectrum()
                                            ->getMassSpectrumId()
                                            .getSpectrumIndex());

          entering_spectrum = qualified_mass_spectrum.getMassSpectrumSPtr();
        }

      qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << " ()"
               << "Will combine spectrum with ms level: "
               << node->getQualifiedMassSpectrum()->getMsLevel();

      if(binning_type == msxps::minexpert::BinningType::NONE)
        trace_combiner.combine(map_trace, *entering_spectrum);
      else
        mass_spectrum_combiner.combine(map_trace, *entering_spectrum);

			combination_single_tic_intensity += entering_spectrum->sumY();

      ++combined_spectra_count;

      // qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << " ()"
      //<< "map trace size:" << map_trace.size();
    }

  std::chrono::system_clock::time_point finish =
    std::chrono::system_clock::now();

  std::cout
    << __FILE__ << " @ " << __LINE__ << " " << __FUNCTION__ << " () "
    << "\n"
    << "The combination of " << combined_spectra_count << " spectra took: "
    << std::chrono::duration_cast<std::chrono::seconds>(finish - start).count()
    << " seconds, that is: "
    << std::chrono::duration_cast<std::chrono::minutes>(finish - start).count()
    << " minutes." 
		<< "The single intensity TIC of the obtained mass spectrum: "
		<< map_trace.asTrace().sumY() << " to compare to :" << combination_single_tic_intensity
		<< std::endl;

  pappso::Utils::writeToFile(map_trace.asText(),
                             "/home/rusconi/combination-mass-spectrum.txt");

#endif

  // Now try to get the precursor spectrum node for a given product spectrum
  // index.

#if 0

  pappso::MsRunDataSetNode *node =
    mass_spec_data_tree_p->precursorNodeByProductSpectrumIndex(9047);

  if(node == nullptr)
    qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << " ()";
  else
    std::cout << __FILE__ << " @ " << __LINE__ << " " << __FUNCTION__ << " () "
              << std::setprecision(15)
              << "For product ion spectrum index 9047, the found precuror "
                 "spectrum index: "
              << node->getQualifiedMassSpectrum()
                   ->getMassSpectrumId()
                   .getSpectrumIndex()
              << std::endl;

#endif

    // Now try to get the list of product ion scans for a given precursor
    // spectrum index.

#if 0
  std::vector<pappso::MsRunDataSetNode *> nodes = mass_spec_data_tree_p->productIonNodesBySpectrumIndex(9046);

	for(auto &&node : nodes)
	{
		std::cout << __FILE__ << " @ " << __LINE__ << " " << __FUNCTION__ << " () "
		          << std::setprecision(15)
		          << "4096 --> " << node->getQualifiedMassSpectrum()->getMassSpectrumId().getSpectrumIndex() << std::endl;
	}

#endif

  QApplication::processEvents();

  // Now try to get all the precursor nodes that hold a mass spectrum in which
  // an ion of the given mz value was fragmented.

#if 0
  read_start = std::chrono::system_clock::now();

  std::vector<pappso::MsRunDataSetNode *> nodes =
    mass_spec_data_tree_p->precursorNodesByPrecursorMz(712.3287, precision_p);

	std::cout << __FILE__ << " @ " << __LINE__ << " " << __FUNCTION__ << " () "
	          << std::setprecision(15)
	          << "Done making the research." << std::endl;

	std::cout << __FILE__ << " @ " << __LINE__ << " " << __FUNCTION__ << " () "
	          << std::setprecision(15)
	          << "nodes size: " << nodes.size() << std::endl;

  for(auto &&node : nodes)
    std::cout << __FILE__ << " @ " << __LINE__ << " " << __FUNCTION__ << " () "
              << std::setprecision(15) << "Actual precursor mz: "
              << node->getQualifiedMassSpectrum()->getMassSpectrumId().getSpectrumIndex()
              << std::endl;

  // Report on the duration of the process.
  read_end = std::chrono::system_clock::now();
  diff     = read_end - read_start;
  seconds  = std::chrono::duration_cast<std::chrono::seconds>(diff);
  milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(diff);

  std::cout << __FILE__ << " @ " << __LINE__ << " " << __FUNCTION__ << " () "
            << std::setprecision(15) << "Precursor ion nodes search performed in "
						<< milliseconds.count() << " milliseconds, that is: "
            << seconds.count() << " seconds. And " 
						<< nodes.size() << " nodes were found.\n" << std::endl;
#endif

#if 0
  // Now compute the TIC of the MS1 data and the TIC of the MS2 data.
  read_start = std::chrono::system_clock::now();

  std::vector<pappso::MsRunDataSetNode *> ms1_nodes =
    mass_spec_data_tree_p->flattenedViewMsLevel(1, false);

  std::vector<pappso::MsRunDataSetNode *> ms2_nodes =
    mass_spec_data_tree_p->flattenedViewMsLevel(2, false);

  using Pair    = std::pair<double, double>;
  using Map     = std::map<double, double>;
  using MapIter = Map::iterator;

  Map ms1_tic_map;
  Map ms2_tic_map;

  for(auto &&node : ms1_nodes)
    {
      pappso::QualifiedMassSpectrumCstSPtr mass_spectrum_csp =
        node->getQualifiedMassSpectrum();

      std::pair<MapIter, bool> ret = ms1_tic_map.insert(
        Pair(mass_spectrum_csp->getRtInMinutes(),
             mass_spectrum_csp->getMassSpectrumSPtr()->sumY()));

      if(!ret.second)
        {
          // There was no insertion. That means that there was already a pair by
          // the same key.

          ret.first->second += mass_spectrum_csp->getMassSpectrumSPtr()->sumY();
        }
    }

  QString ms1_tic_string;

  for(auto &&pair : ms1_tic_map)
    {
      ms1_tic_string += QString("%1 %2\n")
                          .arg(pair.first, 0, 'f', 6)
                          .arg(pair.second, 0, 'f', 6);
    }

  pappso::Utils::writeToFile(ms1_tic_string, "/home/rusconi/ms1-tic.txt");

  for(auto &&node : ms2_nodes)
    {
      pappso::QualifiedMassSpectrumCstSPtr mass_spectrum_csp =
        node->getQualifiedMassSpectrum();

      std::pair<MapIter, bool> ret = ms2_tic_map.insert(
        Pair(mass_spectrum_csp->getRtInMinutes(),
             mass_spectrum_csp->getMassSpectrumSPtr()->sumY()));

      if(!ret.second)
        {
          // There was no insertion. That means that there was already a pair by
          // the same key.

          ret.first->second += mass_spectrum_csp->getMassSpectrumSPtr()->sumY();
        }
    }

  QString ms2_tic_string;

  for(auto &&pair : ms2_tic_map)
    {
      ms2_tic_string += QString("%1 %2\n")
                          .arg(pair.first, 0, 'f', 6)
                          .arg(pair.second, 0, 'f', 6);
    }

  pappso::Utils::writeToFile(ms2_tic_string, "/home/rusconi/ms2-tic.txt");

#endif

  // Report on the duration of the process.
  read_end     = std::chrono::system_clock::now();
  diff         = read_end - read_start;
  seconds      = std::chrono::duration_cast<std::chrono::seconds>(diff);
  milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(diff);

  std::cout << __FILE__ << " @ " << __LINE__ << " " << __FUNCTION__ << " () "
            << std::setprecision(15) << "Process performed in "
            << milliseconds.count()
            << " milliseconds, that is: " << seconds.count() << " seconds."
            << std::endl;
#endif
