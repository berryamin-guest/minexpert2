mineXpert2
==========

mineXpert2 (of the http://www.msxpertsuite.org software collection) is a program
that allows one to perform the following tasks:

 - Open mass spectrometry data files (mzML, mzXML, asc, xy, ...);
 - Full-depth MS^n data exploration;
 - Calculate and display the TIC chromatogram and various color maps;
 - For mobility data, calculate and display a mz=f(dt) color map;
 - Gather all mass data into a single table view for easy filtering of the data
   according to a number of criteria;
 - Integrate the table view filtered data to any kind of visual representation:
   TIC chromatogram, mass|drift spectrum, color maps;
 - Integrate the data from the TIC chromatogram or color map
   - to mass spectrum;
   - to drift spectrum;
   - to mz vs drift time color map or m/z vs retention time color map;
 - Integrate back to any;
 - Perform single-point integrations for detailed mass spectral data scrutiny;
 - Export the data to text files;
 - Save plot to graphics files (pdf, png, jpg);

