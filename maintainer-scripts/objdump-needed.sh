#!/bin/sh


if [ ! -f "$1" ]
    then
    echo "Provide full path to binary file"
    exit 1
fi

for i in $(objdump -p $1 | grep NEEDED | sed 's/ *NEEDED *l/l/')
  do 
  dpkg -S $i
done

