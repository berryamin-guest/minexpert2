[Setup]
AppName=mineXpert2

; Set version number below
#define public version "6.0.0"
AppVersion={#version}

#define public arch "mingw64"
#define public platform "win7+"
#define sourceDir "C:\msys64\home\polipo\devel\msxpertsuite\development"

; Set version number below
AppVerName=mineXpert2 version {#version}
DefaultDirName={pf}\mineXpert2
DefaultGroupName=mineXpert2
OutputDir="C:\msys64\home\polipo\devel\msxpertsuite\minexpert2\development\winInstaller"

; Set version number below
OutputBaseFilename=mineXpert2-{#arch}-{#platform}-v{#version}-setup

; Set version number below
OutputManifestFile=mineXpert2-{#arch}-{#platform}-v{#version}-setup-manifest.txt
ArchitecturesAllowed=x64
ArchitecturesInstallIn64BitMode=x64

LicenseFile="{#sourceDir}\doc\COPYING"
AppCopyright="Copyright (C) 2016-2020 Filippo Rusconi"

AllowNoIcons=yes
AlwaysShowComponentsList=yes
AllowRootDirectory=no
AllowCancelDuringInstall=yes
AppComments="mineXpert2, by Filippo Rusconi"
AppContact="Filippo Rusconi, PhD, Research scientist at CNRS, France"
CloseApplications=yes
CreateUninstallRegKey=yes
DirExistsWarning=yes
WindowResizable=yes
WizardImageFile="{#sourceDir}\images\splashscreen-minexpert2-innosetup.bmp"
WizardImageStretch=yes

[Dirs]
Name: "{app}\doc"

[Files]
Source: "C:\msXpertSuite-libDeps\*"; DestDir: {app}; Flags: ignoreversion recursesubdirs;

Source: "{#sourceDir}\doc\history.html"; DestDir: {app}\doc;
Source: "{#sourceDir}\doc\history.odt"; DestDir: {app}\doc;

Source: "{#sourceDir}\..\build-area\minexpert\minexpert2.exe"; DestDir: {app};
Source: "{#sourceDir}\minexpert\user-manual\minexpert-doc.pdf"; DestDir: {app}\doc\minexpert2;

[Icons]
Name: "{group}\mineXpert2"; Filename: "{app}\minexpert2.exe"; WorkingDir: "{app}"
Name: "{group}\Uninstall mineXpert2"; Filename: "{uninstallexe}"

[Run]
Filename: "{app}\minexpert.exe"; Description: "Launch mineXpert"; Flags: postinstall nowait unchecked
